package settings;

import io.appium.java_client.android.AndroidDriver;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.remote.DesiredCapabilities;

import java.net.MalformedURLException;
import java.net.URL;

/**
 * Created by imran on 05/08/2018.
 */
public class WebSetup {
    protected static WebDriver driver;
    protected static String chromeDriverPath = "C:\\chromedriver\\chromedriver.exe"; //Download the driver and keep any drive in pc then put the folder path here (https://chromedriver.storage.googleapis.com/index.html?path=2.35/)

    public static void prepareWeb() throws MalformedURLException {
        System.setProperty("webdriver.chrome.driver", chromeDriverPath);
        driver = new ChromeDriver();
        driver.manage().window().maximize();
        driver.get("https://selenium2.coinspectapp.com/");
        System.out.println("Browser successfully opened the website https://selenium2.coinspectapp.com/");
    }
}
