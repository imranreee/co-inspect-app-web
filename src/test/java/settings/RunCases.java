
package settings;
import cases.*;
import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.Status;
import com.aventstack.extentreports.markuputils.ExtentColor;
import com.aventstack.extentreports.markuputils.MarkupHelper;
import com.aventstack.extentreports.reporter.ExtentHtmlReporter;
import com.aventstack.extentreports.reporter.configuration.ChartLocation;
import com.aventstack.extentreports.reporter.configuration.Theme;
import org.testng.ITestResult;
import org.testng.annotations.*;

import java.io.File;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by imran on 05/08/2018.
 */
public class RunCases extends WebSetup {
    public static ExtentHtmlReporter htmlReporter;
    public static ExtentReports extent;
    public static ExtentTest test;
    String versionNumber = new String("0.0.1");  //Put the app versing here

    DateFormat dateFormat = new SimpleDateFormat("dd-MMM-yyyy__hh-mm-ssaa");
    Date date = new Date();
    String currentDate = dateFormat.format(date);

    DateFormat dateFormatNew = new SimpleDateFormat("dd-MMM-yyyy");
    Date dateNew = new Date();
    String currentDateNew = dateFormatNew.format(dateNew);

    @BeforeClass
    public void setUp() throws Exception {
        prepareWeb();
    }

    @BeforeTest
    public void config() throws Exception {
        File file = new File(HelperClass.resultFolderPath + currentDateNew);
        if (!file.exists()) {
            file.mkdir();
        } else {

        }
        htmlReporter = new ExtentHtmlReporter(file + "\\CoInspectAppAdminAutomationTestResult_" + versionNumber + "_" + currentDate + ".html");
        extent = new ExtentReports();
        extent.attachReporter(htmlReporter);

        extent.setSystemInfo("Machine", "Windows");
        extent.setSystemInfo("Platform Name", "Web");
        extent.setSystemInfo("Application Name", "CoInspectApp_Admin");
        extent.setSystemInfo("Application Version", versionNumber);
        extent.setSystemInfo("Script written by", "Al Imran(imranreee@gmail.com)");

        htmlReporter.config().setChartVisibilityOnOpen(true);
        htmlReporter.config().setDocumentTitle("Automation Test Results");
        htmlReporter.config().setReportName("Automation Test Results of CoInspectAppAdmin_"+versionNumber+" Web App");
        htmlReporter.config().setTestViewChartLocation(ChartLocation.TOP);
        htmlReporter.config().setTheme(Theme.DARK);
    }

    @Test(priority = 0) //This case common for running individual case
    public void logInTest() throws Exception {
        new LogIn(driver).logIn();
    }

    @Test(priority = 1)
    public void dashboardFilterTest() throws Exception {
        new Dashboard(driver).dashboard();
    }

    @Test(priority = 2)
    public void propertyTest() throws Exception {
        new Property(driver).property();
    }

    @Test(priority = 3)
    public void checkListCRUDTest() throws Exception {
        new CheckListCRUD(driver).checkListCRUD();
    }

    @Test(priority = 4)
    public void checkListExportImportTest() throws Exception {
        new CheckListExportImport(driver).checkListExportImport();
    }

    @Test(priority = 5)
    public void checkListAddGroupCRUDTest() throws Exception {
        new CheckListAddGroupCRUD(driver).checkListAddGroupCRUD();
    }

    @Test(priority = 6)
    public void checkListAddGroupMultiFieldTest() throws Exception {
        new CheckListAddGroupMultiField(driver).checkListAddGroupMultiField();
    }

    @Test(priority = 7)
    public void checkListSingleMultiSelectTest() throws Exception {
        new CheckListSingleMultiSelect(driver).checkListSingleMultiSelect();
    }

    @Test(priority = 8)
    public void checkListAnswerGroupTest() throws Exception {
        new CheckListAnswerGroup(driver).checkListAnswerGroup();
    }

    @Test(priority = 9)
    public void checkListScoringTest() throws Exception {
        new CheckListScoring(driver).checkListScoring();
    }

    @Test(priority = 10)
    public void checkListAdvancedTest() throws Exception {
        new CheckListAdvanced(driver).checkListAdvanced();
    }

    @Test(priority = 11)
    public void checkListWcMsgTest() throws Exception {
        new CheckListWcMsg(driver).checkListWcMsg();
    }

    @Test(priority = 12)
    public void checkListNotificationTest() throws Exception {
        new CheckListNotification(driver).checkListNotification();
    }

    @Test(priority = 13)
    public void checkListReportConfigureTest() throws Exception {
        new CheckListReportConfigure(driver).checkListReportConfigure();
    }

    @Test(priority = 14)
    public void usersCRUDTest() throws Exception {
        new UsersCRUD(driver).usersCRUD();
    }

    @Test(priority = 15)
    public void usersInvalidEntriesTest() throws Exception {
        new UsersInvalidEntries(driver).usersInvalidEntries();
    }

    @Test(priority = 16)
    public void usersGroupTest() throws Exception {
        new UsersGroup(driver).usersGroup();
    }

    @Test(priority = 17)
    public void assignmentsTest() throws Exception {
        new Assignments(driver).assignments();
    }

    @Test(priority = 18)
    public void settingsPresetsTest() throws Exception {
        new SettingsPresets(driver).settingsPresets();
    }

    @Test(priority = 19)
    public void settingsNotificationsTest() throws Exception {
        new SettingsNotifications(driver).settingsNotifications();
    }

    @Test(priority = 20)
    public void settingsLogosTest() throws Exception {
        new SettingsLogos(driver).settingsLogos();
    }

    @Test(priority = 21)
    public void settingsMobileTest() throws Exception {
        new SettingsMobile(driver).settingsMobile();
    }

    @Test(priority = 22)
    public void settingsAuthTest() throws Exception {
        new SettingsAuth(driver).settingsAuth();
    }

    @Test(priority = 23)
    public void settingsGeneralTest() throws Exception {
        new SettingsGeneral(driver).settingsGeneral();
    }

    @Test(priority = 24)
    public void myAccountTest() throws Exception {
        new MyAccount(driver).myAccount();
    }

    @Test(priority = 25)
    public void logoutAndInvalidLoginTest() throws Exception {
        new LogoutAndInvalidLogin(driver).logoutAndInvalidLogin();
    }

    @AfterMethod
    public void getResult(ITestResult result) throws IOException {
        if (result.getStatus() == ITestResult.FAILURE) {
            String screenShotPath = CaptureScreenShot.capture(driver, result.getName() + "_" + currentDate);
            test.log(Status.FAIL, MarkupHelper.createLabel(result.getName() + " Test case FAILED due to below issues:", ExtentColor.RED));
            test.fail(result.getThrowable());
            test.addScreenCaptureFromPath(screenShotPath);
            driver.navigate().refresh();

        } else if (result.getStatus() == ITestResult.SUCCESS) {
            test.log(Status.PASS, MarkupHelper.createLabel(result.getName() + " Test Case PASSED", ExtentColor.GREEN));
            driver.navigate().refresh();
        } else {
            test.log(Status.SKIP, MarkupHelper.createLabel(result.getName() + " Test Case SKIPPED", ExtentColor.ORANGE));
            test.skip(result.getThrowable());
            driver.navigate().refresh();
        }
    }

    @AfterTest
    public void tearDown() {
        extent.flush();
    }
}
