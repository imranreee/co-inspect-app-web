package settings;

import org.openqa.selenium.*;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

/**
 * Created by imran on 05/08/2018.
 */
public class HelperClass {

    protected WebDriver driver;
    protected By makeFail = By.xpath("");
    /*protected String csvFilePath = "D:\\GitLab\\CoInspectAppAdmin\\files\\FL_insurance_sample.csv";
    protected String propertyPhotoPath = "D:\\GitLab\\CoInspectAppAdmin\\files\\property.jpg";
    protected String logoPath = "D:\\GitLab\\CoInspectAppAdmin\\files\\coinspect_logo_black.png";
    protected String zipFilePath = "D:\\GitLab\\CoInspectAppAdmin\\files\\TestName.zip";
    public static String resultFolderPath = "D:\\GitLab\\CoInspectAppAdmin\\testResult\\";*/

    protected String csvFilePath = System.getProperty("user.dir") +"\\files\\FL_insurance_sample.csv";
    protected String propertyPhotoPath = System.getProperty("user.dir") +"\\files\\property.jpg";
    protected String logoPath = System.getProperty("user.dir") +"\\files\\coinspect_logo_black.png";
    protected String zipFilePath = System.getProperty("user.dir") +"\\files\\TestName.zip";
    public static String resultFolderPath = System.getProperty("user.dir") +"\\testResult\\";


    public HelperClass(WebDriver driver) {
        this.driver = driver;
    }

    protected void waitForVisibilityOf(By locator) {
        WebDriverWait wait = new WebDriverWait(driver, 30);
        wait.until(ExpectedConditions.visibilityOfElementLocated(locator));
    }
    protected void waitForClickabilityOf(By locator) {
        WebDriverWait wait = new WebDriverWait(driver, 30);
        wait.until(ExpectedConditions.elementToBeClickable(locator));
    }
}
