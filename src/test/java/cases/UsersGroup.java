package cases;

import com.aventstack.extentreports.Status;
import com.aventstack.extentreports.markuputils.ExtentColor;
import com.aventstack.extentreports.markuputils.MarkupHelper;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import settings.HelperClass;
import settings.RunCases;

public class UsersGroup extends HelperClass {
    By usersGroupTab = By.xpath("/html/body/div[1]/aside/menu/ul/li[5]");

    By createUserGroupBtn = By.xpath("/html/body/div[1]/section/div/div/div/header/div/div[1]/a");
    By groupNameField = By.xpath("//*[@id=\"chk_name\"]");
    By descriptionField = By.xpath("//*[@id=\"chk_description\"]");
    By cancelBtn = By.xpath("/html/body/div[1]/section/div/div/div/section/form/div/div[2]/div/div/div[2]/a/div");
    By saveBtn = By.xpath("/html/body/div[1]/section/div/div/div/section/form/div/div[2]/div/div/div[3]/div");
    By deleteUserGroup = By.xpath("/html/body/div[1]/section/div/div/div/ul/li[1]/div/div[2]/div");
    By yesConfirmationDialog = By.xpath("/html/body/div[1]/section/menu/button[2]");

    By checkCreatedUserGroup = By.xpath("//*[text()[contains(.,'TestUserGroup')]]");
    By checkModdedUserGroup = By.xpath("//*[text()[contains(.,'AModTestUserGroup')]]");

    By usersTab = By.xpath("/html/body/div[1]/aside/menu/ul/li[4]");

    public UsersGroup(WebDriver driver) {
        super(driver);
    }

    public UsersGroup usersGroup() throws Exception {
        RunCases.test = RunCases.extent.createTest("Users Group Test");

        waitForVisibilityOf(usersGroupTab);
        driver.findElement(usersGroupTab).click();
        waitForVisibilityOf(createUserGroupBtn);
        System.out.println("App on the user group page");
        RunCases.test.log(Status.PASS, MarkupHelper.createLabel("App on the user group page", ExtentColor.GREEN));

        driver.findElement(createUserGroupBtn).click();
        Thread.sleep(1000);
        driver.findElement(groupNameField).sendKeys("TestUserGroup");
        Thread.sleep(1000);
        driver.findElement(descriptionField).sendKeys("This is test user group's description from automation");
        Thread.sleep(1000);
        driver.findElement(saveBtn).click();

        driver.findElement(usersTab).click();
        Thread.sleep(1000);
        driver.findElement(usersGroupTab).click();
        waitForVisibilityOf(checkCreatedUserGroup);
        System.out.println("User group created successfully");
        RunCases.test.log(Status.PASS, MarkupHelper.createLabel("User group created successfully", ExtentColor.GREEN));

        driver.findElement(checkCreatedUserGroup).click();
        Thread.sleep(1000);
        driver.findElement(groupNameField).clear();
        driver.findElement(groupNameField).sendKeys("AModTestUserGroup");
        driver.findElement(saveBtn).click();

        driver.findElement(usersGroupTab).click();
        waitForVisibilityOf(checkModdedUserGroup);
        System.out.println("User group modified successfully");
        RunCases.test.log(Status.PASS, MarkupHelper.createLabel("User group modified successfully", ExtentColor.GREEN));

        driver.findElement(deleteUserGroup).click();
        waitForVisibilityOf(yesConfirmationDialog);
        driver.findElement(yesConfirmationDialog).click();

        driver.findElement(usersTab).click();
        Thread.sleep(1000);
        driver.findElement(usersGroupTab).click();
        waitForVisibilityOf(createUserGroupBtn);
        Thread.sleep(3000);

        if (driver.findElements(checkModdedUserGroup).size() > 0){
            System.out.println("User group delete not working");
            RunCases.test.log(Status.FAIL, MarkupHelper.createLabel("User group delete not working", ExtentColor.RED));
            driver.findElements(makeFail);
        }else {
            System.out.println("User group deleted successfully");
            RunCases.test.log(Status.PASS, MarkupHelper.createLabel("User deleted successfully", ExtentColor.GREEN));
        }
        return new UsersGroup(driver);
    }
}
