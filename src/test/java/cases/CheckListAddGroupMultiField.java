package cases;

import com.aventstack.extentreports.Status;
import com.aventstack.extentreports.markuputils.ExtentColor;
import com.aventstack.extentreports.markuputils.MarkupHelper;
import org.omg.PortableServer.THREAD_POLICY_ID;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import settings.HelperClass;
import settings.RunCases;

public class CheckListAddGroupMultiField extends HelperClass {
    By createCategoryBtn =By.xpath("//*[@id=\"sub-body\"]/section/div/div/div/header/div[1]/button");
    By checkListTab = By.xpath("//*[@id=\"sub-body\"]/aside/menu/ul/li[3]");
    By checkLogo = By.xpath("//*[@id=\"checklist-editor\"]/div[2]/div[1]/div/section/form/div/div[1]/div/div/a/img");

    By enterCategory = By.xpath("//*[@id=\"sub-body\"]/section/div/div/div/ul/li[1]/div/div/div[1]/a/h3");
    By enterChecklist = By.xpath("/html/body/div[1]/section/div/div/div/ul/li/div/div/a");
    By addGroupBtn = By.xpath("/html/body/div[1]/section/div/div/div[2]/div[3]/div[2]/div[1]/div[1]");

    By questionGroupField = By.xpath("/html/body/div[1]/section/div/div/div[2]/div[3]/section/div/div/div[2]/header/section/div/div[2]/input");
    By questionGroupField2 = By.xpath("//*[@id=\"checklist-editor\"]/div[2]/div[3]/section/div/div/div[3]/header/section/div/div[2]/input");
    By questionGroupField3 = By.xpath("/html/body/div[1]/section/div/div/div[2]/div[3]/section/div/div/div[4]/header/section/div/div[2]/input");
    By plusBtn2 = By.xpath("/html/body/div[1]/section/div/div/div[2]/div[3]/section/div/div/div[3]/header/aside/div[2]");

    By detailsTab = By.xpath("/html/body/div[1]/section/div/div/div[2]/div[3]/section/div/div/div[3]/ul/li/section[2]/div/header/div[1]");
    By answerTab = By.xpath("/html/body/div[1]/section/div/div/div[2]/div[3]/section/div/div/div[3]/ul/li/section[2]/div/header/div[4]");
    By addAnswerOptions = By.xpath("/html/body/div[1]/section/div/div/div[2]/div[3]/section/div/div/div[3]/ul/li/section[2]/div/div/div/div/div[3]/button");
    By secondAnswerOption = By.xpath("//*[@id=\"criteria\"]/div/div[4]/div[2]");
    By addAnotherAnswerBtn = By.xpath("//*[@id=\"criteria\"]/div/div[4]/div[2]/div/div[5]/span/div[1]");

    By newAnswerField = By.xpath("/html/body/div[1]/section/div/div/div[2]/div[3]/section/div/div/div[3]/ul/li/section[2]/div/div/div/div/div[4]/div/div/table/tbody/tr/th[1]/input");
    By thirdNewAnswerField = By.xpath("//*[@id=\"criteria\"]/div/div[4]/div[2]/div/table/tbody/tr[4]/th[1]");

    By addNewResponse = By.xpath("/html/body/div[1]/section/div/div/div[2]/div[3]/section/div/div/div[3]/ul/li/section[2]/div/div/div/div/div[6]/button");
    By labelFieldNewResponseSecond = By.xpath("//*[@id=\"criteria\"]/div/div[7]/ul/li[2]/div/div[1]/div/div/div[2]/input");
    By deleteQuestionGroup = By.xpath("//*[@id=\"checklist-editor\"]/div[2]/div[3]/section/div/div/div[2]/header/aside/div[1]");
    By yesConfirmationDialog = By.xpath("//*[@id=\"confirm-modal\"]/section/menu/button[2]");


    public CheckListAddGroupMultiField(WebDriver driver) {
        super(driver);
    }

    public CheckListAddGroupMultiField checkListAddGroupMultiField() throws Exception {
        RunCases.test = RunCases.extent.createTest("Checklist Add group multi field Test");
        waitForVisibilityOf(checkListTab);

        driver.findElement(checkListTab).click();
        waitForVisibilityOf(createCategoryBtn);
        System.out.println("On the checklist page");
        RunCases.test.log(Status.PASS, MarkupHelper.createLabel("On the checklist page", ExtentColor.GREEN));

        waitForVisibilityOf(enterCategory);
        driver.findElement(enterCategory).click();
        waitForVisibilityOf(enterChecklist);
        driver.findElement(enterChecklist).click();
        waitForVisibilityOf(checkLogo);
        Thread.sleep(2000);

        //for scrolling the page, can move up or down by changing the element as pre your needs
        WebElement element = driver.findElement(addGroupBtn);
        ((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView();", element);

        boolean check = true;
        while( check == true){
            Thread.sleep(3000);
            if (driver.findElements(deleteQuestionGroup).size() > 0){
                driver.findElement(deleteQuestionGroup).click();
                waitForVisibilityOf(yesConfirmationDialog);
                driver.findElement(yesConfirmationDialog).click();
                System.out.println("Existing question group deleted");
                RunCases.test.log(Status.PASS, MarkupHelper.createLabel("Existing question group deleted", ExtentColor.GREEN));

            }else {
                check = false;
            }
        }

        driver.findElement(addGroupBtn).click();
        waitForVisibilityOf(questionGroupField);
        System.out.println("App on the add group name page");
        RunCases.test.log(Status.PASS, MarkupHelper.createLabel("App on the add group name page", ExtentColor.GREEN));


        driver.findElement(questionGroupField).sendKeys("FirstGroup");
        Thread.sleep(2000);
        driver.findElement(addGroupBtn).click();
        waitForVisibilityOf(questionGroupField2);
        driver.findElement(questionGroupField2).sendKeys("SecondGroup");
        Thread.sleep(2000);
        driver.findElement(addGroupBtn).click();
        waitForClickabilityOf(questionGroupField3);
        driver.findElement(questionGroupField3).sendKeys("ThirdGroup");
        System.out.println("Multiple question group created");
        RunCases.test.log(Status.PASS, MarkupHelper.createLabel("Multiple question group created", ExtentColor.GREEN));

        driver.findElement(deleteQuestionGroup).click();
        waitForVisibilityOf(yesConfirmationDialog);
        driver.findElement(yesConfirmationDialog).click();
        Thread.sleep(2000);
        System.out.println("Existing question group deleted");
        RunCases.test.log(Status.PASS, MarkupHelper.createLabel("Existing question group deleted", ExtentColor.GREEN));

        driver.navigate().refresh();
        waitForVisibilityOf(checkLogo);

        WebElement element4 = driver.findElement(plusBtn2);
        ((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView();", element4);
        Thread.sleep(2000);

        driver.findElement(plusBtn2).click();
        waitForVisibilityOf(detailsTab);
        System.out.println("NewQuestion group name created");
        RunCases.test.log(Status.PASS, MarkupHelper.createLabel("NewQuestion group name created", ExtentColor.GREEN));

        driver.findElement(answerTab).click();
        waitForVisibilityOf(addAnswerOptions);
        Thread.sleep(1000);
        driver.findElement(addAnswerOptions).click();
        waitForVisibilityOf(newAnswerField);
        Thread.sleep(2000);
        driver.findElement(addAnswerOptions).click();
        waitForVisibilityOf(newAnswerField);
        Thread.sleep(3000);

        WebElement element2 = driver.findElement(secondAnswerOption);
        ((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView();", element2);
        System.out.println("Multiple answer option type field created");
        RunCases.test.log(Status.PASS, MarkupHelper.createLabel("Multiple answer option type field created", ExtentColor.GREEN));

        Thread.sleep(2000);
        driver.findElement(addAnotherAnswerBtn).click();
        Thread.sleep(2000);
        driver.findElement(addAnotherAnswerBtn).click();
        Thread.sleep(2000);
        driver.findElement(thirdNewAnswerField);
        System.out.println("Multiple answer field created");
        RunCases.test.log(Status.PASS, MarkupHelper.createLabel("Multiple answer field created", ExtentColor.GREEN));
        Thread.sleep(2000);

        WebElement element3 = driver.findElement(addNewResponse);
        ((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView();", element3);
        Thread.sleep(2000);

        driver.findElement(addNewResponse).click();
        Thread.sleep(1000);
        driver.findElement(addNewResponse).click();
        Thread.sleep(1000);
        driver.findElement(labelFieldNewResponseSecond);
        System.out.println("Multiple response field created");
        RunCases.test.log(Status.PASS, MarkupHelper.createLabel("Multiple answer field created", ExtentColor.GREEN));

        return new CheckListAddGroupMultiField(driver);
    }
}
