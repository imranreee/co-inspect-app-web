package cases;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import settings.HelperClass;
import settings.RunCases;

public class SettingsGeneral extends HelperClass {
    By settingsTab = By.xpath("//*[@id=\"sub-body\"]/aside/menu/ul/li[7]");
    By generalTab = By.xpath("//*[@id=\"sub-body\"]/section/div/div/div[1]/header/div[6]");

    By allowAdminOff = By.xpath("//*[@id=\"sub-body\"]/section/div/div/div[1]/section[6]/div/div/div[1]/div/div/div[2]/label[2]");
    By allowAdminOn = By.xpath("//*[@id=\"sub-body\"]/section/div/div/div[1]/section[6]/div/div/div[1]/div/div/div[2]/label[1]");

    By defaultTimeZone = By.xpath("//*[@id=\"sub-body\"]/section/div/div/div[1]/section[6]/div/div/div[2]/div[2]/div/div[1]/div/div/span/button");
    By defaultTimeZoneSearch = By.xpath("//*[@id=\"-overlay\"]/div[1]/div[2]/input");
    By searchResult = By.xpath("//*[@id=\"-overlay\"]/div[2]/div");

    By currentDateTime = By.xpath("//*[@id=\"sub-body\"]/section/div/div/div[1]/section[6]/div/div/div[2]/div[2]/div/div[2]");

    public SettingsGeneral(WebDriver driver) {
        super(driver);
    }

    public SettingsGeneral settingsGeneral() throws InterruptedException {
        RunCases.test = RunCases.extent.createTest("Settings Auth Test");
        waitForVisibilityOf(settingsTab);
        driver.findElement(settingsTab).click();

        waitForVisibilityOf(generalTab);
        driver.findElement(generalTab).click();
        Thread.sleep(2000);

        driver.findElement(allowAdminOff).click();
        System.out.println("Allow admin to login to CoInspect turned off");
        Thread.sleep(1000);
        driver.findElement(allowAdminOn).click();
        System.out.println("Allow admin to login to CoInspect turned on");
        Thread.sleep(1000);

        driver.findElement(defaultTimeZone).click();
        Thread.sleep(1000);
        driver.findElement(defaultTimeZoneSearch).sendKeys("America");
        Thread.sleep(2000);
        driver.findElement(searchResult).click();
        System.out.println("Default time selected");

        System.out.println("Current date and time as per the time zone: "+driver.findElement(currentDateTime).getText());
        return new SettingsGeneral(driver);
    }
}
