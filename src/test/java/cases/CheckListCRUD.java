package cases;

import com.aventstack.extentreports.Status;
import com.aventstack.extentreports.markuputils.ExtentColor;
import com.aventstack.extentreports.markuputils.MarkupHelper;
import org.openqa.selenium.*;
import settings.HelperClass;
import settings.RunCases;

public class CheckListCRUD extends HelperClass {
    By createCategoryBtn =By.xpath("//*[@id=\"sub-body\"]/section/div/div/div/header/div[1]/button");
    By checkListTab = By.xpath("//*[@id=\"sub-body\"]/aside/menu/ul/li[3]");

    By saveBtn = By.xpath("//*[@id=\"sub-body\"]/section/div/div/div/ul[1]/li/div/div/div[2]/div[3]");
    By editBtn = By.xpath("//*[@id=\"sub-body\"]/section/div/div/div/ul/li/div/div/div[2]/div[2]");
    By deleteBtn = By.xpath("//*[@id=\"sub-body\"]/section/div/div/div/ul/li[1]/div/div/div[2]/div[1]");

    By nameInputFiled = By.xpath("//*[@id=\"sub-body\"]/section/div/div/div/ul/li/div/div/div[1]/input");

    By sampleCategory = By.xpath("//*[@id=\"sub-body\"]/section/div/div/div/ul/li/div/div");
    By newCategory = By.xpath("//*[contains(text(), 'new category')]");
    By modifiedCategory = By.xpath("//*[contains(text(), 'aaaModified')]");
    By protectedDialog = By.xpath("//*[contains(text(), 'This category is currently linked to Open and/or Done inspections. Deleting this category is not permitted')]");

    By yesConfirmation = By.xpath("//*[@id=\"confirm-modal\"]/section/menu/button[2]");

    By createCheckListBtn = By.xpath("//*[@id=\"sub-body\"]/section/div/div/div/header/div/div/a/span");
    By crossBtn = By.xpath("//*[@id=\"confirm-modal\"]/section/div");


    By imageUpload = By.xpath("//*[@id=\"checklist-editor\"]/div[2]/div[1]/div/section/form/div/div[1]/div/div/a/img");
    By selectImage = By.xpath("//*[@id=\"checklist-editor\"]/div[1]/section/div/div/div[2]/div/img[14]");
    By selectBtn = By.xpath("//*[@id=\"checklist-editor\"]/div[1]/section/menu/button[2]");
    By addCheckListNameField = By.xpath("//*[@id=\"chk_name\"]");
    //By addADescriptionField = By.xpath("//*[@id=\"checklist-editor\"]/div[2]/div[1]/div/section/form/div/div[2]/div[2]/div/div[1]/div");
    By addADescriptionField = By.id("chk_description");
    By passingScroeField = By.xpath("//*[@id=\"passing_pecentage\"]");
    By enableRandomizationBtn = By.xpath("//*[@id=\"checkbox-checklist-randomize\"]");

    By saveBtnChecklist = By.xpath("//*[@id=\"checklist-editor\"]/div[2]/section[1]/div/div/div/div/button[3]");
    By checker = By.xpath("//*[@id=\"checklist-editor\"]/div[2]/div[3]/div[1]/div/div[2]");


    public CheckListCRUD(WebDriver driver) {
        super(driver);
    }

    public CheckListCRUD checkListCRUD() throws InterruptedException {
        RunCases.test = RunCases.extent.createTest("Checklist CRUD Test");
        waitForVisibilityOf(checkListTab);

        driver.findElement(checkListTab).click();

        waitForVisibilityOf(createCategoryBtn);
        System.out.println("On the checklist page");
        RunCases.test.log(Status.PASS, MarkupHelper.createLabel("On the checklist page", ExtentColor.GREEN));

        Thread.sleep(2000);
        boolean check = true;
        while( check == true){
            if (driver.findElements(deleteBtn).size() > 0){
                driver.findElement(deleteBtn).click();
                Thread.sleep(3000);
                if (driver.findElements(protectedDialog).size() > 0){
                    driver.findElement(crossBtn).click();
                    System.out.println("Unable to delete as Protected category");
                    RunCases.test.log(Status.PASS, MarkupHelper.createLabel("Unable to delete as Protected category", ExtentColor.GREEN));
                    check = false;
                }else {
                    driver.findElement(yesConfirmation).click();
                    driver.navigate().refresh();
                    waitForVisibilityOf(createCategoryBtn);
                    System.out.println("Category deleted");
                    RunCases.test.log(Status.PASS, MarkupHelper.createLabel("Category deleted", ExtentColor.GREEN));
                }
            }else {
                check = false;
            }
        }

        driver.findElement(createCategoryBtn).click();
        Thread.sleep(1000);
        driver.findElement(saveBtn).click();
        Thread.sleep(2000);
        driver.findElement(newCategory);
        System.out.println("Category created");
        RunCases.test.log(Status.PASS, MarkupHelper.createLabel("Category created", ExtentColor.GREEN));

        driver.findElement(editBtn).click();
        Thread.sleep(1000);
        driver.findElement(nameInputFiled).clear();
        driver.findElement(nameInputFiled).sendKeys("aaaModified");
        driver.findElement(saveBtn).click();
        Thread.sleep(2000);
        driver.findElement(modifiedCategory);
        System.out.println("Category modified");
        RunCases.test.log(Status.PASS, MarkupHelper.createLabel("Category modified", ExtentColor.GREEN));

        driver.findElement(deleteBtn).click();
        waitForVisibilityOf(yesConfirmation);
        driver.findElement(yesConfirmation).click();
        driver.navigate().refresh();
        Thread.sleep(3000);
        if (driver.findElements(modifiedCategory).size() <= 0){
            System.out.println("Category deleted");
            RunCases.test.log(Status.PASS, MarkupHelper.createLabel("Category deleted", ExtentColor.GREEN));
        }else {
            System.out.println("Category delete not working");
            RunCases.test.log(Status.PASS, MarkupHelper.createLabel("Category delete not working", ExtentColor.GREEN));
        }


        driver.findElement(createCategoryBtn).click();
        Thread.sleep(1000);
        driver.findElement(saveBtn).click();
        Thread.sleep(2000);

        driver.findElement(sampleCategory).click();
        Thread.sleep(500);
        driver.findElement(createCheckListBtn).click();
        waitForVisibilityOf(imageUpload);
        System.out.println("On the checklist details page");
        RunCases.test.log(Status.PASS, MarkupHelper.createLabel("On the checklist details page", ExtentColor.GREEN));

        driver.findElement(imageUpload).click();
        Thread.sleep(1000);
        driver.findElement(selectImage).click();
        Thread.sleep(1000);
        driver.findElement(selectBtn).click();
        Thread.sleep(1000);
        driver.findElement(imageUpload);
        System.out.println("Image uploaded");
        RunCases.test.log(Status.PASS, MarkupHelper.createLabel("Image uploaded", ExtentColor.GREEN));

        driver.findElement(addCheckListNameField).sendKeys("TestName");
        Thread.sleep(1000);
        driver.findElement(addADescriptionField).click();
        driver.findElement(addADescriptionField).sendKeys("Description from automation test");
        driver.findElement(passingScroeField).clear();
        driver.findElement(passingScroeField).sendKeys("75");
        driver.findElement(enableRandomizationBtn).click();
        Thread.sleep(1000);
        driver.findElement(saveBtnChecklist).click();
        driver.findElement(checker);
        System.out.println("Check list has been created");
        RunCases.test.log(Status.PASS, MarkupHelper.createLabel("Check list has been created", ExtentColor.GREEN));

        Thread.sleep(3000);

        return new CheckListCRUD(driver);
    }
}
