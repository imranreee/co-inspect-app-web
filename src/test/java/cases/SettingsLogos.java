package cases;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import settings.HelperClass;
import settings.RunCases;

import java.awt.*;
import java.awt.datatransfer.StringSelection;
import java.awt.event.KeyEvent;

public class SettingsLogos extends HelperClass {
    By settingsTab = By.xpath("//*[@id=\"sub-body\"]/aside/menu/ul/li[7]");
    By logosTab = By.xpath("//*[@id=\"sub-body\"]/section/div/div/div[1]/header/div[3]");
    By loginScreen = By.xpath("//*[@id=\"sub-body\"]/section/div/div/div[1]/section[3]/div/div[1]/section/ul/li[1]/div/div/h3");

    By navigationBar = By.xpath("//*[@id=\"sub-body\"]/section/div/div/div[1]/section[3]/div/div[1]/section/ul/li[2]/div/div/h3");
    By reportDefault = By.xpath("//*[@id=\"sub-body\"]/section/div/div/div[1]/section[3]/div/div[1]/section/ul/li[3]/div/div/h3");
    By uploadPhotoBtn = By.xpath("//*[@id=\"sub-body\"]/section/div/div/div[1]/section[3]/div/div[2]/div/div/div[1]/div[3]/div/div");
    By replaceBtn = By.xpath("//*[@id=\"sub-body\"]/section/div/div/div[1]/section[3]/div/div[2]/div/div/div[1]/div[2]/div");

    By uploadPhotoNavigation = By.xpath("//*[@id=\"sub-body\"]/section/div/div/div[1]/section[3]/div/div[2]/div/div/div[1]/div[3]/div/div/input");
    By replaceNavigation = By.xpath("//*[@id=\"sub-body\"]/section/div/div/div[1]/section[3]/div/div[2]/div/div/div[1]/div[2]/div");

    By uploadBtnReport = By.xpath("//*[@id=\"sub-body\"]/section/div/div/div[1]/section[3]/div/div[2]/div/div/div[1]/div[3]/div/div/input");
    By replaceReport = By.xpath("//*[@id=\"sub-body\"]/section/div/div/div[1]/section[3]/div/div[2]/div/div/div[1]/div[2]/div");

    public SettingsLogos(WebDriver driver) {
        super(driver);
    }

    public SettingsLogos settingsLogos() throws Exception {
        RunCases.test = RunCases.extent.createTest("Settings Logos Test");
        waitForVisibilityOf(settingsTab);
        driver.findElement(settingsTab).click();

        waitForVisibilityOf(logosTab);
        driver.findElement(logosTab).click();
        Thread.sleep(2000);

        driver.findElement(loginScreen).click();
        Thread.sleep(1000);
        if (driver.findElements(replaceBtn).size() > 0){
            driver.findElement(replaceBtn).click();
            Thread.sleep(2000);
            System.out.println("Logo already exist, deleted the existing logo");
        }

        //Special case for uploading file/image

        driver.findElement(uploadPhotoBtn).click();
        Thread.sleep(2000);
        StringSelection ss = new StringSelection(logoPath);
        Toolkit.getDefaultToolkit().getSystemClipboard().setContents(ss, null);
        Robot robot = new Robot();
        robot.keyPress(KeyEvent.VK_CONTROL);
        robot.keyPress(KeyEvent.VK_V);
        robot.keyRelease(KeyEvent.VK_V);
        robot.keyRelease(KeyEvent.VK_CONTROL);
        robot.keyPress(KeyEvent.VK_ENTER);
        robot.keyRelease(KeyEvent.VK_ENTER);

        waitForVisibilityOf(replaceBtn);
        System.out.println("Login screen image uploaded");
        Thread.sleep(2000);

        driver.findElement(navigationBar).click();
        Thread.sleep(1000);
        if (driver.findElements(replaceNavigation).size() > 0){
            driver.findElement(replaceNavigation).click();
            Thread.sleep(2000);
            System.out.println("Logo already exist, deleted the existing logo");
        }

        driver.findElement(uploadPhotoNavigation).sendKeys(logoPath);
        waitForVisibilityOf(replaceNavigation);
        System.out.println("Navigation bar image uploaded");
        Thread.sleep(2000);

        driver.findElement(reportDefault).click();
        Thread.sleep(1000);
        if (driver.findElements(replaceNavigation).size() > 0){
            driver.findElement(replaceNavigation).click();
            Thread.sleep(2000);
            System.out.println("Logo already exist, deleted the existing logo");
        }

        driver.findElement(uploadBtnReport).sendKeys(logoPath);
        waitForVisibilityOf(replaceReport);
        System.out.println("Report image uploaded");
        Thread.sleep(2000);

        return new SettingsLogos(driver);
    }
}
