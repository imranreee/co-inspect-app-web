package cases;

import com.aventstack.extentreports.Status;
import com.aventstack.extentreports.markuputils.ExtentColor;
import com.aventstack.extentreports.markuputils.MarkupHelper;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import settings.HelperClass;
import settings.RunCases;

import java.util.List;

public class Dashboard extends HelperClass{
    By sevenDaysFilter = By.xpath("//*[@id=\"dashboard\"]/div[1]/div[1]/div[1]/div/div[1]");
    By thirtyDaysFilter = By.xpath("//*[@id=\"dashboard\"]/div[1]/div[1]/div[1]/div/div[2]");
    By ninetyDaysFilter = By.xpath("//*[@id=\"dashboard\"]/div[1]/div[1]/div[1]/div/div[3]");

    By datePickerFirst = By.xpath("//*[@id=\"dashboard\"]/div[1]/div[1]/div[2]/div/header/div/div[1]/input");
    By datePickerMonthView = By.xpath("/html/body/div[4]");
    By previousBtn = By.xpath("/html/body/div[4]/div[3]/table/thead/tr[1]/th[1]");
    By selectFromDate = By.xpath("/html/body/div[4]/div[3]/table/tbody/tr[2]/td[1]");

    By timePickerFirst = By.xpath("//*[@id=\"dashboard\"]/div[1]/div[1]/div[2]/div/header/div/div[2]/div/div/span/button");
    By searchFieldFirstDate = By.xpath("//*[@id=\"-overlay\"]/div[1]/div[2]/input");
    By selectFromTime = By.xpath("//*[@id=\"-overlay\"]/div[2]/div[1]");

    By datePickerSecond = By.xpath("//*[@id=\"dashboard\"]/div[1]/div[1]/div[2]/div/header/div/div[3]/input");
    By selectToDate = By.xpath("/html/body/div[5]/div[3]/table/tbody/tr[5]/td[3]");

    By selectTime = By.xpath("//*[@id=\"-overlay\"]/div[2]/div");
    By timePickerSecondSearchFiled = By.xpath("//*[@id=\"-overlay\"]/div[1]/div[2]/input");
    By timePickerSecond = By.xpath("//*[@id=\"dashboard\"]/div[1]/div[1]/div[2]/div/header/div/div[4]");

    By selectProperty = By.id("dashboard-search-property");
    By propertySearchField = By.xpath("//*[@id=\"-overlay\"]/div[1]/div[2]");
    By propertySearchResult = By.xpath("//*[@id=\"-overlay\"]/div[2]");
    By selectAllBtn = By.xpath("//*[@id=\"-overlay\"]/div[1]/div[1]/button[1]");

    By selectCheckList = By.xpath("//*[@id=\"dashboard-search-checklist\"]/span/button");
    //By selectCheckListSearchField = By.xpath("//*[@id=\"-overlay\"]/div[1]/div[2]/input");
    By selectCheckListSearchField = By.xpath("By.xpath(\"//input[contains(@placeholder,'Search...']\")");
    By selectAll = By.id("-overlay");

    By selectUser = By.xpath("//*[@id=\"dashboard-search-user\"]/span/button");
    By searchFiledSelectUser = By.xpath("//*[@id=\"-overlay\"]/div[1]/div[2]/input");
    By userSearchResult = By.xpath("//*[@id=\"-overlay\"]/div[2]/div[1]/div");

    By updateDashboardBtn = By.xpath("//*[@id=\"configFilter\"]/div[2]/div/div[2]/div/button");

    By resultTab = By.xpath("//*[@id=\"dashboard\"]/div[2]/div/div/header/div[1]");
    By inspectionGraph = By.xpath("//*[@id=\"dashboard\"]/div[2]/div/div/section/div[1]/div/div/div/div/div/div[1]/div[1]/ul[1]/li[1]/span");
    By inspectionGraphView = By.xpath("//*[@id=\"dashboard\"]/div[2]/div/div/section/div[1]/div/div/div/div/div/div[1]/div[2]/div[1]/div");

    By inpectionTable = By.xpath("//*[@id=\"dashboard\"]/div[2]/div/div/section/div[1]/div/div/div/div/div/div[1]/div[1]/ul[1]/li[2]/span");
    By inpectionTableView = By.xpath("//*[@id=\"dashboard\"]/div[2]/div/div/section/div[1]/div/div/div/div/div/div[1]/div[2]/div[2]/div/div/div/img");

    By questionResutlTable = By.xpath("//*[@id=\"dashboard\"]/div[2]/div/div/section/div[1]/div/div/div/div/div/div[1]/div[1]/ul[1]/li[3]/span");
    By questionResutlTableView = By.xpath("//*[@id=\"dashboard\"]/div[2]/div/div/section/div[1]/div/div/div/div/div/div[1]/div[2]/div[3]/div/div/div/img");
    By exportBtn = By.xpath("//*[@id=\"export-button\"]/div[1]");

    By selectAllText = By.xpath("//*[contains(text(), 'Select All')]");
    By selectAllText2 = By.xpath("//*[contains(text(), 'Sample Checklist')]");
    By selectAllText3 = By.xpath("//*[contains(text(), 'agent')]");
    By inspectionGraphText = By.xpath("//*[contains(text(), 'Inspection Graph')]");
    By inspectionTableText = By.xpath("//*[contains(text(), 'Inspection Table')]");
    By questionResultText = By.xpath("//*[contains(text(), 'Question Results Table')]");




    public Dashboard(WebDriver driver) {
        super(driver);
    }

    public Dashboard dashboard() throws InterruptedException {
        RunCases.test = RunCases.extent.createTest("Dashboard Test");

        waitForVisibilityOf(sevenDaysFilter);
        RunCases.test.log(Status.PASS, MarkupHelper.createLabel("7 days filter found", ExtentColor.GREEN));
        System.out.println("7 days filter found");

        waitForVisibilityOf(thirtyDaysFilter);
        RunCases.test.log(Status.PASS, MarkupHelper.createLabel("30 days filter found", ExtentColor.GREEN));
        System.out.println("30 days filter found");

        waitForVisibilityOf(ninetyDaysFilter);
        RunCases.test.log(Status.PASS, MarkupHelper.createLabel("90 days filter found", ExtentColor.GREEN));
        System.out.println("90 days filter found");

        driver.findElement(datePickerFirst).click();
        Thread.sleep(500);
        driver.findElement(datePickerMonthView);
        System.out.println("First date picker appear");
        RunCases.test.log(Status.PASS, MarkupHelper.createLabel("First date picker appear", ExtentColor.GREEN));

        driver.findElement(previousBtn).click();
        driver.findElement(selectFromDate).click();
        String selectedDate = driver.findElement(datePickerFirst).getText();
        System.out.println("Selected date is "+selectedDate);
        RunCases.test.log(Status.INFO, MarkupHelper.createLabel("Selected date is "+selectedDate, ExtentColor.CYAN));
        System.out.println("From date picked");
        RunCases.test.log(Status.PASS, MarkupHelper.createLabel("From date picked", ExtentColor.GREEN));

        driver.findElement(timePickerFirst).click();
        driver.findElement(searchFieldFirstDate).click();
        driver.findElement(searchFieldFirstDate).sendKeys("1:00");
        Thread.sleep(1000);
        driver.findElement(selectFromTime).click();
        String selectedTime = driver.findElement(selectFromTime).getText();
        System.out.println("Selected time is "+selectedTime);
        RunCases.test.log(Status.INFO, MarkupHelper.createLabel("Selected time is "+selectedTime, ExtentColor.CYAN));
        System.out.println("From time picked");
        RunCases.test.log(Status.PASS, MarkupHelper.createLabel("From time picked", ExtentColor.GREEN));

        Thread.sleep(1000);

        driver.findElement(datePickerSecond).click();
        Thread.sleep(1000);
        driver.findElement(selectToDate).click();
        System.out.println("To date picked");
        RunCases.test.log(Status.PASS, MarkupHelper.createLabel("To date picked", ExtentColor.GREEN));


        Thread.sleep(1000);
        driver.findElement(timePickerSecond).click();
        Thread.sleep(1000);

        driver.findElement(selectProperty).click();
        waitForClickabilityOf(selectAllText);
        Thread.sleep(3000);
        driver.findElement(selectAllText).click();
        System.out.println("Property search field appeared");
        RunCases.test.log(Status.INFO, MarkupHelper.createLabel("Property search field appeared", ExtentColor.CYAN));

        Thread.sleep(2000);
        driver.findElement(selectCheckList).click();
        waitForClickabilityOf(selectAllText2);
        Thread.sleep(3000);
        driver.findElement(selectAllText2).click();
        System.out.println("All check list selected");
        RunCases.test.log(Status.PASS, MarkupHelper.createLabel("All check list selected", ExtentColor.GREEN));

        Thread.sleep(2000);
        driver.findElement(selectUser).click();
        waitForClickabilityOf(selectAllText3);
        Thread.sleep(3000);
        driver.findElement(selectAllText3).click();
        RunCases.test.log(Status.PASS, MarkupHelper.createLabel("User selected", ExtentColor.GREEN));

        driver.findElement(updateDashboardBtn).click();
        Thread.sleep(1000);


        WebElement element = driver.findElement(resultTab);
        ((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView();", element);


        driver.findElement(resultTab).click();
        Thread.sleep(1000);
        driver.findElement(inspectionGraphText).click();
        waitForVisibilityOf(inspectionGraphView);
        Thread.sleep(1000);

        driver.findElement(inspectionTableText).click();
        waitForVisibilityOf(inpectionTableView);
        Thread.sleep(1000);

        driver.findElement(questionResultText).click();
        waitForVisibilityOf(questionResutlTableView);
        Thread.sleep(1000);

        driver.findElement(exportBtn).click();
        waitForVisibilityOf(exportBtn);
        System.out.println("Exported");


        return new Dashboard(driver);
    }

}
