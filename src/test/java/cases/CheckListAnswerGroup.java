package cases;

import com.aventstack.extentreports.Status;
import com.aventstack.extentreports.markuputils.ExtentColor;
import com.aventstack.extentreports.markuputils.MarkupHelper;
import org.omg.IOP.TAG_JAVA_CODEBASE;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;
import org.testng.Assert;
import settings.HelperClass;
import settings.RunCases;

public class CheckListAnswerGroup extends HelperClass {
    By createCategoryBtn =By.xpath("//*[@id=\"sub-body\"]/section/div/div/div/header/div[1]/button");
    By checkListTab = By.xpath("//*[@id=\"sub-body\"]/aside/menu/ul/li[3]");
    By checkLogo = By.xpath("//*[@id=\"checklist-editor\"]/div[2]/div[1]/div/section/form/div/div[1]/div/div/a/img");

    By enterCategory = By.xpath("//*[@id=\"sub-body\"]/section/div/div/div/ul/li[1]/div/div/div[1]/a/h3");
    By enterChecklist = By.xpath("/html/body/div[1]/section/div/div/div/ul/li/div/div/a");
    By addGroupBtn = By.xpath("/html/body/div[1]/section/div/div/div[2]/div[3]/div[2]/div[1]/div[1]");

    By questionGroupField = By.xpath("/html/body/div[1]/section/div/div/div[2]/div[3]/section/div/div/div[2]/header/section/div/div[2]/input");
    By questionGroupField2 = By.xpath("//*[@id=\"checklist-editor\"]/div[2]/div[3]/section/div/div/div[3]/header/section/div/div[2]/input");
    By plusBtn2 = By.xpath("/html/body/div[1]/section/div/div/div[2]/div[3]/section/div/div/div[3]/header/aside/div[2]");

    By detailsTab = By.xpath("/html/body/div[1]/section/div/div/div[2]/div[3]/section/div/div/div[3]/ul/li/section[2]/div/header/div[1]");
    By answerTab = By.xpath("/html/body/div[1]/section/div/div/div[2]/div[3]/section/div/div/div[3]/ul/li/section[2]/div/header/div[4]");
    By addAnswerOptions = By.xpath("/html/body/div[1]/section/div/div/div[2]/div[3]/section/div/div/div[3]/ul/li/section[2]/div/div/div/div/div[3]/button");

    By deleteQuestionGroup = By.xpath("//*[@id=\"checklist-editor\"]/div[2]/div[3]/section/div/div/div[2]/header/aside/div[1]");
    By yesConfirmationDialog = By.xpath("//*[@id=\"confirm-modal\"]/section/menu/button[2]");

    By addNewResponse = By.xpath("//*[@id=\"criteria\"]/div/div[5]/button");
    By enableRequiredFieldToggleBtn = By.xpath("//*[@id=\"criteria\"]/div/div[6]/ul/li/div/div[1]/div/div/div[4]/label");
    By includeQuestionCheckbox = By.xpath("//*[@id=\"criteria\"]/div/div[7]/div[1]/div[1]/label");
    By requiredPhotoCheckbox = By.xpath("//*[@id=\"criteria\"]/div/div[7]/div[1]/div[2]/label");

    By mandatoryRadioBtn = By.xpath("//*[@id=\"criteria\"]/div/div[7]/div[2]/div/div/div[1]/label");
    By randomizeRadioBtn = By.xpath("//*[@id=\"criteria\"]/div/div[7]/div[2]/div/div/div[2]/label");

    By typeField = By.xpath("//*[@id=\"criteria\"]/div/div[6]/ul/li/div/div[1]/div/div/div[1]/select");
    By typeFieldAsSelected = By.xpath("//*[@id=\"criteria\"]/div/div[6]/ul/li/div/div[1]/div/div/div[1]/div/div[1]/select");

    By number = By.xpath("//*[contains(text(), 'Number')]");
    By texts = By.xpath("//*[contains(text(), 'Text')]");

    By unitsField = By.xpath("//*[@id=\"criteria\"]/div/div[6]/ul/li/div/div[1]/div/div/div[1]/div/div[3]/input");
    By descriptionField = By.xpath("//*[@id=\"criteria\"]/div/div[6]/ul/li/div/div[1]/div/div/div[3]/input");
    By labelField2 = By.xpath("//*[@id=\"criteria\"]/div/div[6]/ul/li/div/div[1]/div/div/div[2]/input");
    By deleteNewResponse = By.xpath("//*[@id=\"criteria\"]/div/div[6]/ul/li/div/div[2]/div");

    public CheckListAnswerGroup(WebDriver driver) {
        super(driver);
    }

    public CheckListAnswerGroup checkListAnswerGroup() throws Exception {
        RunCases.test = RunCases.extent.createTest("Checklist Answer group Test");
        waitForVisibilityOf(checkListTab);

        driver.findElement(checkListTab).click();
        waitForVisibilityOf(createCategoryBtn);
        System.out.println("On the checklist page");
        RunCases.test.log(Status.PASS, MarkupHelper.createLabel("On the checklist page", ExtentColor.GREEN));

        waitForVisibilityOf(enterCategory);
        driver.findElement(enterCategory).click();
        waitForVisibilityOf(enterChecklist);
        Thread.sleep(1000);
        driver.findElement(enterChecklist).click();
        waitForVisibilityOf(checkLogo);
        Thread.sleep(2000);

        //for scrolling the page, can move up or down by changing the element as per your needs
        WebElement element = driver.findElement(addGroupBtn);
        ((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView();", element);

        boolean check = true;
        while( check == true){
            Thread.sleep(3000);
            if (driver.findElements(deleteQuestionGroup).size() > 0){
                driver.findElement(deleteQuestionGroup).click();
                waitForVisibilityOf(yesConfirmationDialog);
                driver.findElement(yesConfirmationDialog).click();
                System.out.println("Existing question group deleted");
                RunCases.test.log(Status.PASS, MarkupHelper.createLabel("Existing question group deleted", ExtentColor.GREEN));

            }else {
                check = false;
            }
        }

        driver.findElement(addGroupBtn).click();
        waitForVisibilityOf(questionGroupField);
        System.out.println("App on the add group name page");
        RunCases.test.log(Status.PASS, MarkupHelper.createLabel("App on the add group name page", ExtentColor.GREEN));

        driver.findElement(questionGroupField).sendKeys("FirstGroup");
        Thread.sleep(2000);
        driver.navigate().refresh();
        waitForVisibilityOf(checkLogo);
        Thread.sleep(3000);

        WebElement element2 = driver.findElement(addGroupBtn);
        ((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView();", element2);

        Thread.sleep(1000);
        driver.findElement(addGroupBtn).click();
        waitForVisibilityOf(questionGroupField2);
        driver.findElement(questionGroupField2).sendKeys("secondGroup");
        Thread.sleep(2000);
        driver.navigate().refresh();
        waitForVisibilityOf(checkLogo);

        System.out.println("App on the add group name page");
        RunCases.test.log(Status.PASS, MarkupHelper.createLabel("App on the add group name page", ExtentColor.GREEN));

        WebElement element3 = driver.findElement(addGroupBtn);
        ((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView();", element3);

        driver.findElement(plusBtn2).click();
        waitForVisibilityOf(detailsTab);
        driver.findElement(detailsTab).click();
        Thread.sleep(1000);

        driver.findElement(answerTab).click();
        waitForVisibilityOf(addAnswerOptions);
        Thread.sleep(1000);

        WebElement element4 = driver.findElement(addNewResponse);
        ((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView();", element4);
        Thread.sleep(2000);

        driver.findElement(addNewResponse).click();
        Thread.sleep(1000);

        driver.findElement(typeField).click();
        Thread.sleep(1000);
        driver.findElement(number).click();
        waitForVisibilityOf(unitsField);
        driver.findElement(unitsField).sendKeys("5ft");
        Thread.sleep(1000);
        driver.findElement(typeFieldAsSelected).click();
        Thread.sleep(1000);
        driver.findElement(texts).click();
        driver.findElement(typeField).click();
        Thread.sleep(1000);
        driver.findElement(number).click();
        waitForVisibilityOf(unitsField);

        WebElement inputBox1 = driver.findElement(unitsField);
        String unitFieldText = inputBox1.getAttribute("value");
        if(unitFieldText.isEmpty()){
            System.out.println("Input field is empty");
            RunCases.test.log(Status.FAIL, MarkupHelper.createLabel("Input field is empty :(", ExtentColor.RED));
            driver.findElement(makeFail);
        }else {
            Assert.assertEquals("5ft", unitFieldText);
            System.out.println(unitFieldText);
        }

        System.out.println("Number field works fine");
        RunCases.test.log(Status.PASS, MarkupHelper.createLabel("Number field works fine", ExtentColor.GREEN));

        Thread.sleep(1000);
        driver.findElement(typeFieldAsSelected).click();
        Thread.sleep(1000);
        driver.findElement(texts).click();
        Thread.sleep(1000);
        driver.findElement(labelField2).sendKeys("testText");

        driver.findElement(typeField).click();
        Thread.sleep(1000);

        //For special case, selecting value from dropdown list
        Select type = new Select(driver.findElement(typeField));
        type.selectByVisibleText("Text");
        type.selectByIndex(2);
        Thread.sleep(1000);
        driver.findElement(labelField2);

        WebElement inputBox2 = driver.findElement(labelField2);
        String labelFieldText = inputBox2.getAttribute("value");
        if(labelFieldText.isEmpty()){
            System.out.println("Input field is empty");
            RunCases.test.log(Status.FAIL, MarkupHelper.createLabel("Input field is empty :(", ExtentColor.RED));
            driver.findElement(makeFail);
        }else {
            Assert.assertEquals("testText", labelFieldText);
            System.out.println(labelFieldText);
        }

        if (driver.findElements(unitsField).size() > 0){
            System.out.println("Text field not working");
            RunCases.test.log(Status.FAIL, MarkupHelper.createLabel("Text field not working", ExtentColor.RED));
            driver.findElements(makeFail);
        }else {
            System.out.println("Text field works fine");
            RunCases.test.log(Status.PASS, MarkupHelper.createLabel("Text field works fine", ExtentColor.GREEN));
        }

        Thread.sleep(2000);
        driver.findElement(labelField2).clear();
        Thread.sleep(1000);
        driver.findElement(labelField2).sendKeys("01/02/2018");
        Thread.sleep(1000);

        WebElement inputBox3 = driver.findElement(labelField2);
        String labelFieldText2 = inputBox3.getAttribute("value");
        if(labelFieldText2.isEmpty()){
            System.out.println("Input field is empty");
            RunCases.test.log(Status.FAIL, MarkupHelper.createLabel("Input field is empty :(", ExtentColor.RED));
            driver.findElement(makeFail);
        }else {
            Assert.assertEquals("01/02/2018", labelFieldText2);
            System.out.println(labelFieldText2);
        }


        if (driver.findElements(unitsField).size() > 0){
            System.out.println("Date field not working");
            RunCases.test.log(Status.FAIL, MarkupHelper.createLabel("Text field not working", ExtentColor.RED));
            driver.findElements(makeFail);
        }else {
            System.out.println("Date field works fine");
            RunCases.test.log(Status.PASS, MarkupHelper.createLabel("Text field works fine", ExtentColor.GREEN));
        }

        driver.findElement(enableRequiredFieldToggleBtn).click();
        Thread.sleep(1000);
        driver.findElement(includeQuestionCheckbox).click();
        Thread.sleep(1000);
        driver.findElement(requiredPhotoCheckbox).click();
        Thread.sleep(1000);
        driver.findElement(randomizeRadioBtn).click();
        Thread.sleep(1000);
        driver.findElement(mandatoryRadioBtn).click();

        System.out.println("All buttons are checked");
        RunCases.test.log(Status.PASS, MarkupHelper.createLabel("All buttons are checked", ExtentColor.GREEN));

        driver.findElement(descriptionField).sendKeys("TestDescription");
        Thread.sleep(1000);

        WebElement inputBox = driver.findElement(descriptionField);
        String descriptionFieldText = inputBox.getAttribute("value");
        if(descriptionFieldText.isEmpty()){
            System.out.println("Input field is empty");
            RunCases.test.log(Status.FAIL, MarkupHelper.createLabel("Input field is empty :(", ExtentColor.RED));
            driver.findElement(makeFail);
        }else {
            Assert.assertEquals("TestDescription", descriptionFieldText);
            System.out.println(descriptionFieldText);
        }

        System.out.println("Description field working fine");
        RunCases.test.log(Status.PASS, MarkupHelper.createLabel("Description field working fine", ExtentColor.GREEN));


        driver.findElement(deleteNewResponse).click();
        Thread.sleep(2000);
        if (driver.findElements(descriptionField).size() > 0){
            System.out.println("Delete response not working :(");
            RunCases.test.log(Status.FAIL, MarkupHelper.createLabel("Delete response not working :(", ExtentColor.RED));
            driver.findElement(makeFail);
        }else {
            System.out.println("Delete response working fine:)");
            RunCases.test.log(Status.PASS, MarkupHelper.createLabel("Delete response working fine:)", ExtentColor.GREEN));
        }


        return new CheckListAnswerGroup(driver);
    }
}
