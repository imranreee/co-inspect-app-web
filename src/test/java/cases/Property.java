package cases;

import com.aventstack.extentreports.Status;
import com.aventstack.extentreports.markuputils.ExtentColor;
import com.aventstack.extentreports.markuputils.MarkupHelper;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.testng.Assert;
import settings.HelperClass;
import settings.RunCases;
import sun.java2d.d3d.D3DRenderQueue;

public class Property extends HelperClass {
    By propertyTab =By.xpath("//*[@id=\"sub-body\"]/aside/menu/ul/li[2]");
    By createCategoryBtn = By.xpath("//*[@id=\"sub-body\"]/section/div/div/div/header/div[1]/button");
    By categoryNameField = By.xpath("//*[@id=\"sub-body\"]/section/div/div/div/ul[1]/li/div/div/div[1]/input");
    By saveBtn = By.xpath("//*[@id=\"sub-body\"]/section/div/div/div/ul[1]/li/div/div/div[2]/div[3]");

    By testCategory = By.xpath("//*[contains(text(), 'TestCategory')]");
    By newCategory = By.xpath("//*[contains(text(), 'new category')]");
    By editCategory  = By.xpath("//*[@id=\"sub-body\"]/section/div/div/div/ul/li[1]/div/div/div[2]/div[2]");
    By modifiedCategory = By.xpath("//*[contains(text(), 'ZModifiedCategory')]");

    By unableToDelete = By.xpath("//*[@id=\"confirm-modal\"]/section/article");

    By deleteBtn = By.xpath("//*[@id=\"sub-body\"]/section/div/div/div/ul/li[1]/div/div/div[2]/div[1]");
    By deleteBtn2 = By.xpath("//*[@id=\"sub-body\"]/section/div/div/div/ul/li[2]/div/div/div[2]/div[1]");
    By yesBtnConfirmationDialgo = By.xpath("//*[@id=\"confirm-modal\"]/section/menu/button[2]");
    By searchField = By.xpath("//*[@id=\"sub-body\"]/section/div/div/div/header/div[2]/form/input");
    By uncategorizedBtn = By.xpath("//*[@id=\"sub-body\"]/section/div/div/div/ul/li[2]/div/div");
    By crossBtn = By.xpath("//*[@id=\"confirm-modal\"]/section/div");

    By importBtn = By.xpath("//*[@id=\"sub-body\"]/section/div/div/div/header/div/div/a[2]/span");
    By chooseFileBtn = By.xpath("//*[@id=\"import-properties\"]");
    By saveBtnFleBrowse = By.xpath("//*[@id=\"import-buildings\"]/section/div[2]/div/div/div[1]/div");
    By cancelBtnFleBrowse = By.xpath("//*[@id=\"import-buildings\"]/section/div[2]/div/div/div[2]/div");
    By createBtn = By.xpath("//*[@id=\"sub-body\"]/section/div/div/div/header/div/div/a[1]/span");
    By uploadBtn = By.xpath("//*[@id=\"property-editor\"]/div[1]/div[2]/div/div/input");
    By editBtn = By.xpath("//*[@id=\"property-editor\"]/div[1]/div[1]/div/div[1]/div/div/section/figure");
    By searchAddress = By.xpath("//*[@id=\"property-address-search\"]");
    By searchAddressSearchField = By.xpath("//*[@id=\"property-address-search-overlay\"]/div[1]/div[2]/input");
    By searchAddressSearchResult = By.xpath("//*[@id=\"property-address-search-overlay\"]/div[2]/div[2]/div[3]/label");
    By propertyNameField = By.xpath("//*[@id=\"property-editor\"]/div[5]/div[1]/input");
    By addressLineTwo = By.xpath("//*[@id=\"property-editor\"]/div[6]/div[2]/input");

    By openCategory = By.xpath("//*[@id=\"sub-body\"]/section/div/div/div/ul/li[1]/div/div/div[1]/a/h3");
    By latitudeField = By.xpath("//*[@id=\"property-editor\"]/div[7]/div[1]/input");
    By longitudeField = By.xpath("//*[@id=\"property-editor\"]/div[7]/div[2]/input");
    By cityField = By.xpath("//*[@id=\"city\"]");
    By stateField = By.xpath("//*[@id=\"state\"]");
    By zipField = By.xpath("//*[@id=\"zip\"]");
    By countryField = By.xpath("//*[@id=\"country\"]");
    By timeZone = By.xpath("//*[@id=\"property-editor\"]/div[9]/div[2]/div/div[1]/div/div/span/button");
    By savePropertyInfo = By.xpath("//*[@id=\"property-editor\"]/div[10]/div[2]/div[1]");

    By historyTab = By.xpath("//*[@id=\"sub-body\"]/section/div/div/div/header/div[2]");
    //By ownersTab = By.xpath("//*[@id=\"sub-body\"]/section/div/div/div/header/div[3]");
    //By ownersTab = By.xpath("//*[contains(text(), ' Owners')]");
    By ownersTab = By.xpath("//*[@id=\"sub-body\"]/section/div/div/div/header/div[3]");
    //By ownersTab = By.className("{active:active.view == 'owners'}");

    By createNewOwnerBtn = By.xpath("//*[@id=\"sub-body\"]/section/div/div/div/section[4]/div[1]/div/header/div/div[1]/a");
    By firstName = By.xpath("//*[@id=\"sub-body\"]/section/div/div/div/section[4]/div[2]/div/ul/div/div/div/div[2]/div/div[1]/div[1]/input");
    By middleName = By.xpath("//*[@id=\"sub-body\"]/section/div/div/div/section[4]/div[2]/div/ul/div/div/div/div[2]/div/div[1]/div[2]/input");
    By lastName = By.xpath("//*[@id=\"sub-body\"]/section/div/div/div/section[4]/div[2]/div/ul/div/div/div/div[2]/div/div[1]/div[3]/input");
    By emailAddress = By.xpath("//*[@id=\"sub-body\"]/section/div/div/div/section[4]/div[2]/div/ul/div/div/div/div[2]/div/div[2]/div[1]/input");
    By passWordField = By.xpath("//*[@id=\"sub-body\"]/section/div/div/div/section[4]/div[2]/div/ul/div/div/div/div[2]/div/div[2]/div[2]/input[1]");
    By OwnerSaveButton = By.xpath("//*[@id=\"sub-body\"]/section/div/div/div/section[4]/div[2]/div/ul/div/div/div/div[2]/div/div[4]/button[1]");

    By createdOwner = By.xpath("//*[@id=\"sub-body\"]/section/div/div/div/section[4]/div[2]/div/ul/div/div/div/div[1]/div");
    By yesConfirmationDialog = By.xpath("//*[@id=\"confirm-modal\"]/section/menu/button[2]");

    public Property(WebDriver driver) {
        super(driver);
    }

    public Property property() throws InterruptedException {
        RunCases.test = RunCases.extent.createTest("Property tab test");

        waitForVisibilityOf(propertyTab);
        System.out.println("Property tab found");

        driver.findElement(propertyTab).click();
        /*boolean check = true;
        while( check == true){
            Thread.sleep(3000);
            if (driver.findElements(deleteBtn).size() > 0){
                driver.findElement(deleteBtn).click();
                waitForVisibilityOf(yesConfirmationDialog);
                driver.findElement(yesConfirmationDialog).click();
                System.out.println("Existing category deleted");
                RunCases.test.log(Status.PASS, MarkupHelper.createLabel("Existing category deleted", ExtentColor.GREEN));

            }else if (driver.findElements(deleteBtn2).size() > 0){
                driver.findElement(deleteBtn2).click();
                waitForVisibilityOf(yesConfirmationDialog);
                driver.findElement(yesConfirmationDialog).click();
                System.out.println("Existing category deleted");
                RunCases.test.log(Status.PASS, MarkupHelper.createLabel("Existing category deleted", ExtentColor.GREEN));

            } else {
                check = false;
            }
        }*/


        waitForVisibilityOf(createCategoryBtn);
        System.out.println("App on the Property category page");
        RunCases.test.log(Status.PASS, MarkupHelper.createLabel("App on the property category page", ExtentColor.GREEN));

        driver.findElement(createCategoryBtn).click();
        driver.findElement(categoryNameField).clear();
        driver.findElement(categoryNameField).sendKeys("TestCategory"); //Pur your desired string here
        driver.findElement(saveBtn).click();
        Thread.sleep(2000);
        driver.findElement(testCategory);
        System.out.println("Category created successfully");
        RunCases.test.log(Status.PASS, MarkupHelper.createLabel("Category created successfully", ExtentColor.GREEN));

        driver.findElement(editCategory).click();
        Thread.sleep(1000);
        driver.findElement(categoryNameField).clear();
        driver.findElement(categoryNameField).sendKeys("ZModifiedCategory"); //Pur your desired string here
        driver.findElement(saveBtn).click();
        Thread.sleep(2000);
        driver.findElement(modifiedCategory);

        System.out.println("Category name modify worked successfully");
        RunCases.test.log(Status.PASS, MarkupHelper.createLabel("Category name modify worked successfully", ExtentColor.GREEN));

        driver.findElement(deleteBtn2).click();

        Thread.sleep(2000);
        if (driver.findElements(unableToDelete).size() > 0){
            driver.findElement(crossBtn).click();
            System.out.println("Category already linked, unable to delete");
            RunCases.test.log(Status.PASS, MarkupHelper.createLabel("Category already linked, unable to delete", ExtentColor.GREEN));
        }else {
            waitForVisibilityOf(yesBtnConfirmationDialgo);
            driver.findElement(yesBtnConfirmationDialgo).click();
            Thread.sleep(1000);
            if (driver.findElements(newCategory).size() <= 0){
                System.out.println("Delete category worked successfully");
                RunCases.test.log(Status.PASS, MarkupHelper.createLabel("Delete category worked successfully", ExtentColor.GREEN));
            }else {
                System.out.println("Delete category not working");
                RunCases.test.log(Status.FAIL, MarkupHelper.createLabel("Delete category not working", ExtentColor.RED));
                driver.findElement(makeFail);
            }
        }


        driver.findElement(searchField).sendKeys("new category"); //Pur your desired string here, this function currently not working
        Thread.sleep(1000);


        driver.findElement(openCategory).click();
        Thread.sleep(1000);
        driver.findElement(importBtn).click();
        driver.findElement(cancelBtnFleBrowse).click();

        driver.findElement(importBtn).click();
        driver.findElement(chooseFileBtn).sendKeys(csvFilePath);
        Thread.sleep(2000);
        driver.findElement(saveBtnFleBrowse).click();
        System.out.println("Import button working fine");
        RunCases.test.log(Status.PASS, MarkupHelper.createLabel("Import button working fine", ExtentColor.GREEN));
        Thread.sleep(1000);

        driver.findElement(createBtn).click();

        driver.findElement(searchAddress).click();
        Thread.sleep(1000);
        driver.findElement(searchAddressSearchField).sendKeys("new"); //Pur your search string here
        waitForVisibilityOf(searchAddressSearchResult);
        driver.findElement(searchAddressSearchResult).click();
        System.out.println("Search result found and selected");
        RunCases.test.log(Status.PASS, MarkupHelper.createLabel("Search result found and selected", ExtentColor.GREEN));
        Thread.sleep(3000);

        driver.findElement(propertyNameField).sendKeys("TestName");

        driver.findElement(addressLineTwo).sendKeys("TestAddress");
        System.out.println("Address line 2 putted");
        RunCases.test.log(Status.PASS, MarkupHelper.createLabel("Address line 2 putted", ExtentColor.GREEN));

        System.out.println("Latitude "+driver.findElement(latitudeField).getText());
        RunCases.test.log(Status.PASS, MarkupHelper.createLabel("Latitude "+driver.findElement(latitudeField).getText(), ExtentColor.GREEN));

        System.out.println("Longitude "+driver.findElement(longitudeField).getText());
        RunCases.test.log(Status.PASS, MarkupHelper.createLabel("Longitude "+driver.findElement(longitudeField).getText(), ExtentColor.GREEN));


        System.out.println("State "+driver.findElement(stateField).getText());
        RunCases.test.log(Status.PASS, MarkupHelper.createLabel("State "+driver.findElement(stateField).getText(), ExtentColor.GREEN));

        System.out.println("Country "+driver.findElement(countryField).getText());
        RunCases.test.log(Status.PASS, MarkupHelper.createLabel("Country "+driver.findElement(countryField).getText(), ExtentColor.GREEN));

        System.out.println("Zip "+driver.findElement(zipField).getText());
        RunCases.test.log(Status.PASS, MarkupHelper.createLabel("Zip "+driver.findElement(zipField).getText(), ExtentColor.GREEN));

        System.out.println("City "+driver.findElement(cityField).getText());
        RunCases.test.log(Status.PASS, MarkupHelper.createLabel("City "+driver.findElement(cityField).getText(), ExtentColor.GREEN));

        System.out.println("Time zone "+driver.findElement(timeZone).getText());
        RunCases.test.log(Status.PASS, MarkupHelper.createLabel("Time zone "+driver.findElement(timeZone).getText(), ExtentColor.GREEN));

        driver.findElement(uploadBtn).sendKeys(propertyPhotoPath);
        waitForVisibilityOf(editBtn);
        Thread.sleep(3000);
        System.out.println("Photo uploaded successfully");
        RunCases.test.log(Status.PASS, MarkupHelper.createLabel("Photo uploaded successfully", ExtentColor.GREEN));

        driver.findElement(savePropertyInfo).click();
        System.out.println("Property saved successfully");
        RunCases.test.log(Status.PASS, MarkupHelper.createLabel("Property saved successfully", ExtentColor.GREEN));

        driver.findElement(historyTab).click();
        System.out.println("No history found");
        RunCases.test.log(Status.PASS, MarkupHelper.createLabel("No history found", ExtentColor.GREEN));
        Thread.sleep(1000);

        driver.navigate().refresh();
        waitForVisibilityOf(ownersTab);

        driver.findElement(ownersTab).click();
        driver.findElement(createNewOwnerBtn).click();
        Thread.sleep(1000);

        driver.findElement(firstName).sendKeys("TestName");
        Thread.sleep(1000);
        driver.findElement(middleName).sendKeys("TestName");
        Thread.sleep(1000);
        driver.findElement(lastName).sendKeys("TestName");
        Thread.sleep(1000);
        driver.findElement(emailAddress).sendKeys("test@appium.com ");
        Thread.sleep(1000);
        driver.findElement(passWordField).sendKeys("123456@$%^");
        Thread.sleep(1000);

        driver.findElement(OwnerSaveButton).click();

        return new Property(driver);
    }
}
