package cases;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import settings.HelperClass;
import settings.RunCases;

public class SettingsMobile extends HelperClass {
    By settingsTab = By.xpath("//*[@id=\"sub-body\"]/aside/menu/ul/li[7]");
    By mobileTab = By.xpath("//*[@id=\"sub-body\"]/section/div/div/div[1]/header/div[4]");

    By newPropertyOn = By.xpath("//*[@id=\"sub-body\"]/section/div/div/div[1]/section[4]/div/div/section/div/div/form/div[1]/div[2]/label[1]");
    By newPropertyOff = By.xpath("//*[@id=\"sub-body\"]/section/div/div/div[1]/section[4]/div/div/section/div/div/form/div[1]/div[2]/label[2]");

    By buildingHistoryOn = By.xpath("//*[@id=\"sub-body\"]/section/div/div/div[1]/section[4]/div/div/section/div/div/form/div[2]/div[2]/label[1]");
    By buildingHistoryOff = By.xpath("//*[@id=\"sub-body\"]/section/div/div/div[1]/section[4]/div/div/section/div/div/form/div[2]/div[2]/label[2]");

    By buildingHistoryLimitationSameUser = By.xpath("//*[@id=\"sub-body\"]/section/div/div/div[1]/section[4]/div/div/section/div/div/form/div[3]/div[2]/label[1]");
    By buildingHistoryLimitationAllUsers = By.xpath("//*[@id=\"sub-body\"]/section/div/div/div[1]/section[4]/div/div/section/div/div/form/div[3]/div[2]/label[2]");

    By inspectionSearchOn = By.xpath("//*[@id=\"sub-body\"]/section/div/div/div[1]/section[4]/div/div/section/div/div/form/div[4]/div[2]/label[1]");
    By inspectionSearchOff = By.xpath("//*[@id=\"sub-body\"]/section/div/div/div[1]/section[4]/div/div/section/div/div/form/div[4]/div[2]/label[2]");

    By inspectionSearchLimitationSameUser = By.xpath("//*[@id=\"sub-body\"]/section/div/div/div[1]/section[4]/div/div/section/div/div/form/div[5]/div[2]/label[1]");
    By inspectionSearchLimitationAllUsers = By.xpath("//*[@id=\"sub-body\"]/section/div/div/div[1]/section[4]/div/div/section/div/div/form/div[5]/div[2]/label[2]");

    By allowEmailChangeOn = By.xpath("//*[@id=\"sub-body\"]/section/div/div/div[1]/section[4]/div/div/section/div/div/form/div[6]/div[2]/label[1]");
    By allowEmailChangeOff = By.xpath("//*[@id=\"sub-body\"]/section/div/div/div[1]/section[4]/div/div/section/div/div/form/div[6]/div[2]/label[2]");

    By reviewHistoryOn = By.xpath("//*[@id=\"sub-body\"]/section/div/div/div[1]/section[4]/div/div/section/div/div/form/div[7]/div[2]/label[1]");
    By reviewHistoryOff = By.xpath("//*[@id=\"sub-body\"]/section/div/div/div[1]/section[4]/div/div/section/div/div/form/div[7]/div[2]/label[2]");

    By requireEmailWhenCreatingAUserOn = By.xpath("//*[@id=\"sub-body\"]/section/div/div/div[1]/section[4]/div/div/section/div/div/form/div[8]/div[2]/label[1]");
    By requireEmailWhenCreatingAUserOff = By.xpath("//*[@id=\"sub-body\"]/section/div/div/div[1]/section[4]/div/div/section/div/div/form/div[8]/div[2]/label[2]");

    By requireUsernameWhenCreatingAUserOn = By.xpath("//*[@id=\"sub-body\"]/section/div/div/div[1]/section[4]/div/div/section/div/div/form/div[9]/div[2]/label[1]");
    By requireUsernameWhenCreatingAUserOff = By.xpath("//*[@id=\"sub-body\"]/section/div/div/div[1]/section[4]/div/div/section/div/div/form/div[9]/div[2]/label[2]");

    By ForcePWChangeForNewUserOn = By.xpath("//*[@id=\"sub-body\"]/section/div/div/div[1]/section[4]/div/div/section/div/div/form/div[10]/div[2]/label[1]");
    By ForcePWChangeForNewUserOff = By.xpath("//*[@id=\"sub-body\"]/section/div/div/div[1]/section[4]/div/div/section/div/div/form/div[10]/div[2]/label[2]");

    public SettingsMobile(WebDriver driver) {
        super(driver);
    }

    public SettingsMobile settingsMobile() throws InterruptedException {
        RunCases.test = RunCases.extent.createTest("Settings Mobile Test");
        waitForVisibilityOf(settingsTab);
        driver.findElement(settingsTab).click();

        waitForVisibilityOf(mobileTab);
        driver.findElement(mobileTab).click();
        Thread.sleep(2000);

        driver.findElement(newPropertyOff).click();
        System.out.println("New property off");
        Thread.sleep(1000);
        driver.findElement(newPropertyOn).click();
        System.out.println("New property on");
        Thread.sleep(1000);

        driver.findElement(buildingHistoryOff).click();
        System.out.println("Building history setting off");
        Thread.sleep(1000);
        driver.findElement(buildingHistoryOn).click();
        System.out.println("Building history setting on");
        Thread.sleep(1000);

        driver.findElement(buildingHistoryLimitationSameUser).click();
        System.out.println("Building History Limitation setting changed to same user");
        Thread.sleep(1000);
        driver.findElement(buildingHistoryLimitationAllUsers).click();
        System.out.println("Building History Limitation setting changed to all users");
        Thread.sleep(1000);

        driver.findElement(inspectionSearchOff).click();
        System.out.println("Inspection Search setting off");
        Thread.sleep(1000);
        driver.findElement(inspectionSearchOn).click();
        System.out.println("Inspection Search setting on");
        Thread.sleep(1000);

        driver.findElement(inspectionSearchLimitationSameUser).click();
        System.out.println("Inspection Search Limitation setting changed to same user");
        Thread.sleep(1000);
        driver.findElement(inspectionSearchLimitationAllUsers).click();
        System.out.println("Inspection Search Limitation setting changed to all users");
        Thread.sleep(1000);

        driver.findElement(allowEmailChangeOff).click();
        System.out.println("Allow email change setting off");
        Thread.sleep(1000);
        driver.findElement(allowEmailChangeOn).click();
        System.out.println("Allow email change setting on");
        Thread.sleep(1000);

        driver.findElement(reviewHistoryOff).click();
        System.out.println("Review History setting off");
        Thread.sleep(1000);
        driver.findElement(reviewHistoryOn).click();
        System.out.println("Review History setting on");
        Thread.sleep(1000);

        driver.findElement(requireEmailWhenCreatingAUserOn).click();
        System.out.println("Require email when creating a user setting on");
        Thread.sleep(1000);
        driver.findElement(requireEmailWhenCreatingAUserOff).click();
        System.out.println("Require email when creating a user setting off");
        Thread.sleep(1000);

        driver.findElement(requireUsernameWhenCreatingAUserOff).click();
        System.out.println("Require username when creating a user setting off");
        Thread.sleep(1000);
        driver.findElement(requireUsernameWhenCreatingAUserOn).click();
        System.out.println("Require username when creating a user setting on");
        Thread.sleep(1000);

        driver.findElement(ForcePWChangeForNewUserOn).click();
        System.out.println("Force Password Change For New UsersCRUD setting on");
        Thread.sleep(1000);
        driver.findElement(ForcePWChangeForNewUserOff).click();
        System.out.println("Force Password Change For New UsersCRUD setting off");
        Thread.sleep(1000);

        return new SettingsMobile(driver);
    }
}
