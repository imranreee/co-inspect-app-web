package cases;

import com.aventstack.extentreports.Status;
import com.aventstack.extentreports.markuputils.ExtentColor;
import com.aventstack.extentreports.markuputils.MarkupHelper;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import settings.HelperClass;
import settings.RunCases;

import java.awt.*;
import java.awt.datatransfer.StringSelection;
import java.awt.event.KeyEvent;
import java.io.File;

public class CheckListAddGroupCRUD extends HelperClass {
    By createCategoryBtn =By.xpath("//*[@id=\"sub-body\"]/section/div/div/div/header/div[1]/button");
    By checkListTab = By.xpath("//*[@id=\"sub-body\"]/aside/menu/ul/li[3]");
    By checkLogo = By.xpath("//*[@id=\"checklist-editor\"]/div[2]/div[1]/div/section/form/div/div[1]/div/div/a/img");

    By enterCategory = By.xpath("//*[@id=\"sub-body\"]/section/div/div/div/ul/li[1]/div/div/div[1]/a/h3");
    By enterChecklist = By.xpath("/html/body/div[1]/section/div/div/div/ul/li/div/div/a");
    By addGroupBtn = By.xpath("/html/body/div[1]/section/div/div/div[2]/div[3]/div[2]/div[1]/div[1]");

    By questionGroupField = By.xpath("/html/body/div[1]/section/div/div/div[2]/div[3]/section/div/div/div[2]/header/section/div/div[2]/input");
    By scoreFiled = By.xpath("/html/body/div[1]/section/div/div/div[2]/div[3]/section/div/div/div[2]/header/section/div/div[1]/input");
    By plusBtn = By.xpath("/html/body/div[1]/section/div/div/div[2]/div[3]/section/div/div/div[3]/header/aside/div[2]");

    By questionGroupField2 = By.xpath("//*[@id=\"checklist-editor\"]/div[2]/div[3]/section/div/div/div[3]/header/section/div/div[2]/input");
    By scoreFiled2 = By.xpath("/html/body/div[1]/section/div/div/div[2]/div[3]/section/div/div/div[3]/header/section/div/div[1]/input");
    //By plusBtn2 = By.xpath("//*[@id=\"checklist-editor\"]/div[2]/div[3]/section/div/div/div[2]/header/aside/div[2]");
    By plusBtn2 = By.xpath("/html/body/div[1]/section/div/div/div[2]/div[3]/section/div/div/div[3]/header/aside/div[2]");

    By detailsTab = By.xpath("/html/body/div[1]/section/div/div/div[2]/div[3]/section/div/div/div[3]/ul/li/section[2]/div/header/div[1]");
    By questionField = By.xpath("/html/body/div[1]/section/div/div/div[2]/div[3]/section/div/div/div[3]/ul/li/section[2]/div/div/div/div[2]/div/input");
    By descriptionField = By.xpath("/html/body/div[1]/section/div/div/div[2]/div[3]/section/div/div/div[3]/ul/li/section[2]/div/div/div/div[3]/div/textarea");

    By websiteLinkField = By.xpath("/html/body/div[1]/section/div/div/div[2]/div[3]/section/div/div/div[3]/ul/li/section[2]/div/div/div/div[4]/div/input");
    By labelField = By.xpath("/html/body/div[1]/section/div/div/div[2]/div[3]/section/div/div/div[3]/ul/li/section[2]/div/div/div/div[5]/div[1]/input");
    By regulationField = By.xpath("/html/body/div[1]/section/div/div/div[2]/div[3]/section/div/div/div[3]/ul/li/section[2]/div/div/div/div[5]/div[2]/input");
    By entryCodeField = By.xpath("/html/body/div[1]/section/div/div/div[2]/div[3]/section/div/div/div[3]/ul/li/section[2]/div/div/div/div[5]/div[3]/input");

    By photosTab = By.xpath("/html/body/div[1]/section/div/div/div[2]/div[3]/section/div/div/div[3]/ul/li/section[2]/div/header/div[3]");
    By uploadPhotosBtn = By.xpath("/html/body/div[1]/section/div/div/div[2]/div[3]/section/div/div/div[3]/ul/li/section[2]/div/div/div/header/div/div[2]");
    By deletePhotoBtn = By.xpath("/html/body/div[1]/section/div/div/div[2]/div[3]/section/div/div/div[3]/ul/li/section[2]/div/div/div/div/div[1]/div/div/section/figure[1]");

    By answerTab = By.xpath("/html/body/div[1]/section/div/div/div[2]/div[3]/section/div/div/div[3]/ul/li/section[2]/div/header/div[4]");
    By addAnswerOptions = By.xpath("/html/body/div[1]/section/div/div/div[2]/div[3]/section/div/div/div[3]/ul/li/section[2]/div/div/div/div/div[3]/button");

    By newAnswerField = By.xpath("/html/body/div[1]/section/div/div/div[2]/div[3]/section/div/div/div[3]/ul/li/section[2]/div/div/div/div/div[4]/div/div/table/tbody/tr/th[1]/input");
    By addNewResponse = By.xpath("/html/body/div[1]/section/div/div/div[2]/div[3]/section/div/div/div[3]/ul/li/section[2]/div/div/div/div/div[6]/button");
    By labelFieldNewResponse = By.xpath("/html/body/div[1]/section/div/div/div[2]/div[3]/section/div/div/div[3]/ul/li/section[2]/div/div/div/div/div[7]/ul/li/div/div[1]/div/div/div[2]/input");
    By descriptionFieldNewResponse = By.xpath("/html/body/div[1]/section/div/div/div[2]/div[3]/section/div/div/div[3]/ul/li/section[2]/div/div/div/div/div[7]/ul/li/div/div[1]/div/div/div[3]/input");

    By scoringTab = By.xpath("/html/body/div[1]/section/div/div/div[2]/div[3]/section/div/div/div[3]/ul/li/section[2]/div/header/div[5]");
    By answerScoringField = By.xpath("/html/body/div[1]/section/div/div/div[2]/div[3]/section/div/div/div[3]/ul/li/section[2]/div/div/div/div[3]/div[2]/table/tbody/tr/td[2]/input");
    By autoFailCheckMark = By.xpath("/html/body/div[1]/section/div/div/div[2]/div[3]/section/div/div/div[3]/ul/li/section[2]/div/div/div/div[3]/div[2]/table/tbody/tr/td[4]/input");
    By excludeCheckBox = By.xpath("/html/body/div[1]/section/div/div/div[2]/div[3]/section/div/div/div[3]/ul/li/section[2]/div/div/div/div[3]/div[2]/table/tbody/tr/td[5]/input");
    By deleteQuestionGroup = By.xpath("//*[@id=\"checklist-editor\"]/div[2]/div[3]/section/div/div/div[2]/header/aside/div[1]");
    By yesConfirmationDialog = By.xpath("//*[@id=\"confirm-modal\"]/section/menu/button[2]");

    By editBtn = By.xpath("//*[@id=\"checklist-editor\"]/div[2]/div[3]/section/div/div/div[3]/header/aside/div[3]");
    By modeQuestionGroupName = By.xpath("//*[contains(text(), 'ModName')]");

    By modQ = By.xpath("//*[contains(text(), 'ModQ')]");
    By modD = By.xpath("//*[contains(text(), 'ModD')]");
    By modWl = By.xpath("//*[contains(text(), 'http://www.modwl.com')]");
    By modL = By.xpath("//*[contains(text(), 'ModL')]");
    By modR = By.xpath("//*[contains(text(), 'ModR')]");
    By modEC = By.xpath("//*[contains(text(), '456')]");


    By editDetails = By.xpath("/html/body/div[1]/section/div/div/div[2]/div[3]/section/div/div/div[3]/ul/li/section/header/aside/div[2]");
    By indexField = By.xpath("/html/body/div[1]/section/div/div/div[2]/div[3]/section/div/div/div[3]/ul/li/section[2]/div/div/div/div[1]/div[1]/input");
    By applyNewIndexBtn = By.xpath("/html/body/div[1]/section/div/div/div[2]/div[3]/section/div/div/div[3]/ul/li/section[2]/div/div/div/div[1]/div[2]/div");

    public CheckListAddGroupCRUD(WebDriver driver) {
        super(driver);
    }

    public CheckListAddGroupCRUD checkListAddGroupCRUD() throws Exception {
        RunCases.test = RunCases.extent.createTest("Checklist Add group CRUD Test");
        waitForVisibilityOf(checkListTab);

        driver.findElement(checkListTab).click();

        waitForVisibilityOf(createCategoryBtn);
        System.out.println("On the checklist page");
        RunCases.test.log(Status.PASS, MarkupHelper.createLabel("On the checklist page", ExtentColor.GREEN));

        waitForVisibilityOf(enterCategory);
        driver.findElement(enterCategory).click();
        waitForClickabilityOf(enterChecklist);
        driver.findElement(enterChecklist).click();
        waitForVisibilityOf(checkLogo);
        Thread.sleep(2000);

        //for scrolling the page, can move up or down by changing the element as pre your needs
        WebElement element = driver.findElement(addGroupBtn);
        ((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView();", element);

        boolean check = true;
        while( check == true){
            Thread.sleep(3000);
            if (driver.findElements(deleteQuestionGroup).size() > 0){
                driver.findElement(deleteQuestionGroup).click();
                waitForVisibilityOf(yesConfirmationDialog);
                driver.findElement(yesConfirmationDialog).click();
                System.out.println("Existing question group deleted");
                RunCases.test.log(Status.PASS, MarkupHelper.createLabel("Existing question group deleted", ExtentColor.GREEN));

            }else {
                check = false;
            }
        }

        driver.findElement(addGroupBtn).click();
        waitForVisibilityOf(questionGroupField);
        System.out.println("App on the add group name page");
        RunCases.test.log(Status.PASS, MarkupHelper.createLabel("App on the add group name page", ExtentColor.GREEN));


        driver.findElement(questionGroupField).sendKeys("TestQuestionGroup");
        Thread.sleep(1000);
        System.out.println("Question group name created");
        RunCases.test.log(Status.PASS, MarkupHelper.createLabel("Question group name created", ExtentColor.GREEN));

        driver.navigate().refresh();
        waitForVisibilityOf(addGroupBtn);
        Thread.sleep(1000);
        driver.findElement(addGroupBtn).click();
        waitForVisibilityOf(questionGroupField2);
        System.out.println("App on the add group name page");
        RunCases.test.log(Status.PASS, MarkupHelper.createLabel("App on the add group name page", ExtentColor.GREEN));


        driver.findElement(questionGroupField2).sendKeys("NewTestQuestionGroup");
        Thread.sleep(1000);
        driver.findElement(plusBtn2).click();
        waitForVisibilityOf(detailsTab);
        System.out.println("NewQuestion group name created");
        RunCases.test.log(Status.PASS, MarkupHelper.createLabel("NewQuestion group name created", ExtentColor.GREEN));

        driver.findElement(detailsTab).click();
        Thread.sleep(1000);
        driver.findElement(questionField).sendKeys("This is test question from test automation");
        Thread.sleep(1000);
        driver.findElement(descriptionField).sendKeys("This is test question from test automation");
        Thread.sleep(1000);
        driver.findElement(websiteLinkField).sendKeys("www.test.com");
        Thread.sleep(1000);
        driver.findElement(labelField).sendKeys("This is test label from test automation");
        Thread.sleep(1000);
        driver.findElement(regulationField).sendKeys("This is test regulation from test automation");
        Thread.sleep(1000);
        driver.findElement(entryCodeField).sendKeys("123");
        System.out.println("Question details inserted");
        RunCases.test.log(Status.PASS, MarkupHelper.createLabel("Question details inserted", ExtentColor.GREEN));

        driver.findElement(photosTab).click();
        waitForVisibilityOf(uploadPhotosBtn);

        Thread.sleep(2000);

        WebElement elem = driver.findElement(By.xpath("//input[@type='file']"));
        File file = new File(logoPath);
        elem.sendKeys(file.getAbsolutePath());

        waitForVisibilityOf(deletePhotoBtn);
        System.out.println("Photo updated successfully");
        RunCases.test.log(Status.PASS, MarkupHelper.createLabel("Photo updated successfully", ExtentColor.GREEN));

        driver.findElement(answerTab).click();
        waitForVisibilityOf(addAnswerOptions);
        driver.findElement(addAnswerOptions).click();
        waitForVisibilityOf(newAnswerField);
        driver.findElement(newAnswerField).sendKeys("This is test answer filed");
        Thread.sleep(1000);
        driver.findElement(addNewResponse).click();
        waitForVisibilityOf(labelFieldNewResponse);
        driver.findElement(labelFieldNewResponse).sendKeys("Test label");
        Thread.sleep(1000);
        driver.findElement(descriptionFieldNewResponse).sendKeys("Test description for new response");
        System.out.println("Answer details inserted successfully");
        RunCases.test.log(Status.PASS, MarkupHelper.createLabel("Answer details inserted successfully", ExtentColor.GREEN));

        driver.findElement(scoringTab).click();
        waitForVisibilityOf(answerScoringField);
        driver.findElement(answerScoringField).clear();
        driver.findElement(answerScoringField).sendKeys("2");
        Thread.sleep(1000);
        driver.findElement(autoFailCheckMark).click();
        Thread.sleep(1000);
        driver.findElement(excludeCheckBox).click();
        Thread.sleep(2000);
        System.out.println("Scoring filed filled up");
        RunCases.test.log(Status.PASS, MarkupHelper.createLabel("Scoring filed filled up", ExtentColor.GREEN));

        System.out.println("Question group created");
        RunCases.test.log(Status.PASS, MarkupHelper.createLabel("Question group created", ExtentColor.GREEN));

        driver.navigate().refresh();
        waitForVisibilityOf(editBtn);

        driver.findElement(editBtn).click();
        Thread.sleep(1000);
        driver.findElement(questionGroupField2).clear();
        Thread.sleep(1000);
        driver.findElement(questionGroupField2).sendKeys("ModName");
        Thread.sleep(3000);
        driver.navigate().refresh();
        waitForVisibilityOf(checkLogo);
        Thread.sleep(2000);

        WebElement element2 = driver.findElement(addGroupBtn);
        ((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView();", element2);
        driver.findElement(modeQuestionGroupName);

        System.out.println("Question group Modified");
        RunCases.test.log(Status.PASS, MarkupHelper.createLabel("Question group Modified", ExtentColor.GREEN));

        driver.findElement(editBtn).click();
        waitForVisibilityOf(editDetails);
        driver.findElement(editDetails).click();
        Thread.sleep(1000);

        driver.findElement(indexField).clear();
        Thread.sleep(1000);
        driver.findElement(indexField).sendKeys("2.2");
        Thread.sleep(1000);
        driver.findElement(applyNewIndexBtn).click();

        Thread.sleep(1000);
        driver.findElement(questionField).clear();
        Thread.sleep(1000);
        driver.findElement(questionField).sendKeys("ModQ");
        Thread.sleep(1000);

        driver.findElement(descriptionField).clear();
        Thread.sleep(1000);
        driver.findElement(descriptionField).sendKeys("ModD");
        Thread.sleep(1000);

        driver.findElement(websiteLinkField).clear();
        Thread.sleep(1000);
        driver.findElement(websiteLinkField).sendKeys("www.modwl.com");
        Thread.sleep(1000);

        driver.findElement(labelField).clear();
        Thread.sleep(1000);
        driver.findElement(labelField).sendKeys("ModL");
        Thread.sleep(1000);

        driver.findElement(regulationField).clear();
        Thread.sleep(1000);
        driver.findElement(regulationField).sendKeys("ModR");
        Thread.sleep(1000);

        driver.findElement(entryCodeField).clear();
        Thread.sleep(1000);
        driver.findElement(entryCodeField).sendKeys("456");
        Thread.sleep(3000);

        driver.navigate().refresh();
        waitForVisibilityOf(checkLogo);
        Thread.sleep(3000);

        WebElement element3 = driver.findElement(addGroupBtn);
        ((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView();", element3);

        driver.findElement(editBtn).click();
        waitForVisibilityOf(editDetails);
        driver.findElement(editDetails).click();
        Thread.sleep(1000);

        WebElement element4 = driver.findElement(entryCodeField);
        ((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView();", element4);

        driver.findElement(modQ);
        System.out.println("Description / Website Link / Label / Regulation / Entry Code fields edited");
        RunCases.test.log(Status.PASS, MarkupHelper.createLabel("Description / Website Link / Label / Regulation / Entry Code fields edited", ExtentColor.GREEN));

        return new CheckListAddGroupCRUD(driver);
    }
}
