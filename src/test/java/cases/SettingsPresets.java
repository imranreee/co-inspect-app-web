package cases;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import settings.HelperClass;
import settings.RunCases;

public class SettingsPresets extends HelperClass {
    By settingsTab = By.xpath("//*[@id=\"sub-body\"]/aside/menu/ul/li[7]");

    By presetsTab = By.xpath("//*[@id=\"sub-body\"]/section/div/div/div[1]/header/div[1]");
    By notificationTab = By.xpath("//*[@id=\"sub-body\"]/section/div/div/div[1]/header/div[2]");
    By logosTab = By.xpath("//*[@id=\"sub-body\"]/section/div/div/div[1]/header/div[3]");
    By mobileTab = By.xpath("//*[@id=\"sub-body\"]/section/div/div/div[1]/header/div[4]");
    By authTab = By.xpath("//*[@id=\"sub-body\"]/section/div/div/div[1]/header/div[5]");
    By generalTab = By.xpath("//*[@id=\"sub-body\"]/section/div/div/div[1]/header/div[6]");

    By createNew = By.xpath("//*[@id=\"sub-body\"]/section/div/div/div[1]/section[1]/div/div[1]/header/div/div/a/span");
    By nameField = By.xpath("//*[@id=\"preset-editor\"]/div[1]/div/input");
    By addOptionGroup = By.xpath("//*[@id=\"preset-editor\"]/div[2]/div/div/div/div");
    By addOption = By.xpath("//*[@id=\"preset-editor\"]/div[2]/div/div[1]/div/table/tbody/tr/th/div/div[1]/div");
    By dropDown = By.xpath("//*[@id=\"preset-editor\"]/div[2]/div/div[1]/div/table/tbody/tr/th/div/div[2]/div/div[1]/div/div[2]/label");
    By singleAnswer = By.xpath("//*[@id=\"preset-editor\"]/div[2]/div/div[1]/div/table/tbody/tr/th/div/div[2]/div/div[2]/div/div[1]/label");
    By index = By.xpath("//*[@id=\"preset-editor\"]/div[2]/div/div[1]/div/table/tbody/tr[1]/th[1]/input");
    By answer = By.xpath("//*[@id=\"preset-editor\"]/div[2]/div/div[1]/div/table/tbody/tr[1]/th[2]/input");
    By score = By.xpath("//*[@id=\"preset-editor\"]/div[2]/div/div[1]/div/table/tbody/tr[1]/th[3]/input");
    By exclude = By.xpath("//*[@id=\"preset-editor\"]/div[2]/div/div[1]/div/table/tbody/tr[1]/th[5]/input");
    By autoFail = By.xpath("//*[@id=\"preset-editor\"]/div[2]/div/div[1]/div/table/tbody/tr[1]/th[6]/input");

    By addFreeResponse = By.xpath("//*[@id=\"preset-editor\"]/div[3]/div/table/tbody/tr/th/div");
    By indexAfr = By.xpath("//*[@id=\"preset-editor\"]/div[3]/div/table/tbody/tr[1]/th[1]/input");
    By labelField = By.xpath("//*[@id=\"preset-editor\"]/div[3]/div/table/tbody/tr[1]/th[2]/input");
    By description = By.xpath("//*[@id=\"preset-editor\"]/div[3]/div/table/tbody/tr[1]/th[3]/input");

    By addMeasurmentBtn = By.xpath("//*[@id=\"preset-editor\"]/div[4]/div/table/tbody/tr/th/div");
    By indexAddM = By.xpath("//*[@id=\"preset-editor\"]/div[4]/div/table/tbody/tr[1]/th[1]/input");
    By answerM = By.xpath("//*[@id=\"preset-editor\"]/div[4]/div/table/tbody/tr[1]/th[2]/input");
    By requirement = By.xpath("//*[@id=\"preset-editor\"]/div[4]/div/table/tbody/tr[1]/th[3]/input");
    By scoreM = By.xpath("//*[@id=\"preset-editor\"]/div[4]/div/table/tbody/tr[1]/th[6]/input");
    By autoFailM = By.xpath("//*[@id=\"preset-editor\"]/div[4]/div/table/tbody/tr[1]/th[7]/input");

    By saveBtn = By.xpath("//*[@id=\"preset-editor\"]/div[5]/div[1]/button[1]");

    By presetsName = By.xpath("//*[contains(text(), 'TestName')]");
    By propertyTab =By.xpath("//*[@id=\"sub-body\"]/aside/menu/ul/li[2]");
    By deletePresetsList =By.xpath("//*[@id=\"sub-body\"]/section/div/div/div[1]/section[1]/div/div[1]/section/ul/li/div/div[2]/div");
    By yesBtnDialog =By.xpath("//*[@id=\"confirm-modal\"]/section/menu/button[2]");

    public SettingsPresets(WebDriver driver) {
        super(driver);
    }

    public SettingsPresets settingsPresets() throws InterruptedException {
        RunCases.test = RunCases.extent.createTest("Settings Presets Test");
        waitForVisibilityOf(settingsTab);
        driver.findElement(settingsTab).click();

        waitForVisibilityOf(createNew);
        driver.findElement(createNew).click();
        Thread.sleep(2000);
        driver.findElement(nameField).click();
        driver.findElement(nameField).sendKeys("TestName");
        driver.findElement(addOptionGroup).click();
        waitForVisibilityOf(addOption);
        driver.findElement(addOption).click();
        Thread.sleep(1000);

        driver.findElement(index).clear();
        driver.findElement(index).sendKeys("2");
        driver.findElement(answer).sendKeys("AnswerTest");
        driver.findElement(score).clear();
        driver.findElement(score).sendKeys("2");
        driver.findElement(exclude).click();
        driver.findElement(autoFail).click();
        driver.findElement(dropDown).click();
        driver.findElement(singleAnswer).click();
        System.out.println("Option created");

        WebElement element = driver.findElement(addFreeResponse); //for scrolling the page, can move up or down by changing the element as pre your needs
        ((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView();", element);

        driver.findElement(addFreeResponse).click();
        waitForVisibilityOf(indexAfr);
        driver.findElement(indexAfr).clear();
        driver.findElement(indexAfr).sendKeys("2");
        driver.findElement(labelField).sendKeys("TestAnswer");
        driver.findElement(description).sendKeys("This is test description from automation testing");

        driver.findElement(addMeasurmentBtn).click();
        Thread.sleep(2000);
        driver.findElement(indexAddM).clear();
        driver.findElement(indexAddM).sendKeys("2");
        driver.findElement(answerM).sendKeys("ft");
        driver.findElement(requirement).sendKeys("req");
        driver.findElement(scoreM).sendKeys("2");
        driver.findElement(autoFailM).click();

        driver.findElement(saveBtn).click();
        Thread.sleep(3000);

        driver.findElement(propertyTab).click();
        Thread.sleep(1000);
        driver.findElement(settingsTab).click();
        Thread.sleep(2000);
        driver.findElement(presetsName);
        System.out.println("Presets category created and list detected");

        driver.findElement(presetsName).click();
        driver.findElement(addMeasurmentBtn);
        System.out.println("Preset category working");

        driver.findElement(deletePresetsList).click();
        waitForVisibilityOf(yesBtnDialog);
        driver.findElement(yesBtnDialog).click();
        waitForVisibilityOf(createNew);

        driver.navigate().refresh();
        waitForVisibilityOf(createNew);

        if (driver.findElements(presetsName).size() > 0){
            System.out.println("Presets list Delete not working");
            driver.findElements(makeFail);
        }else {
            System.out.println("Delete presets list worked fine");
        }

        return new SettingsPresets(driver);
    }
}
