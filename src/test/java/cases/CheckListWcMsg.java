package cases;

import com.aventstack.extentreports.Status;
import com.aventstack.extentreports.markuputils.ExtentColor;
import com.aventstack.extentreports.markuputils.MarkupHelper;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import settings.HelperClass;
import settings.RunCases;

public class CheckListWcMsg extends HelperClass {
    By createCategoryBtn =By.xpath("//*[@id=\"sub-body\"]/section/div/div/div/header/div[1]/button");
    By checkListTab = By.xpath("//*[@id=\"sub-body\"]/aside/menu/ul/li[3]");
    By checkLogo = By.xpath("//*[@id=\"checklist-editor\"]/div[2]/div[1]/div/section/form/div/div[1]/div/div/a/img");

    By enterCategory = By.xpath("//*[@id=\"sub-body\"]/section/div/div/div/ul/li[1]/div/div/div[1]/a/h3");
    By enterChecklist = By.xpath("/html/body/div[1]/section/div/div/div/ul/li/div/div/a");
    By addGroupBtn = By.xpath("/html/body/div[1]/section/div/div/div[2]/div[3]/div[2]/div[1]/div[1]");

    By questionGroupField = By.xpath("/html/body/div[1]/section/div/div/div[2]/div[3]/section/div/div/div[2]/header/section/div/div[2]/input");
    By questionGroupField2 = By.xpath("//*[@id=\"checklist-editor\"]/div[2]/div[3]/section/div/div/div[3]/header/section/div/div[2]/input");
    By plusBtn2 = By.xpath("/html/body/div[1]/section/div/div/div[2]/div[3]/section/div/div/div[3]/header/aside/div[2]");
    By detailsTab = By.xpath("/html/body/div[1]/section/div/div/div[2]/div[3]/section/div/div/div[3]/ul/li/section[2]/div/header/div[1]");

    By deleteQuestionGroup = By.xpath("//*[@id=\"checklist-editor\"]/div[2]/div[3]/section/div/div/div[2]/header/aside/div[1]");
    By yesConfirmationDialog = By.xpath("//*[@id=\"confirm-modal\"]/section/menu/button[2]");

    By scoringTab = By.xpath("//*[@id=\"checklist-editor\"]/div[2]/div[3]/section/div/div/div[3]/ul/li/section[2]/div/header/div[5]");
    By showAdvanceOptionBtn = By.xpath("//*[@id=\"checklist-editor\"]/div[2]/section[2]/div/div/button/div");
    By reportPhotoBtn = By.xpath("//*[@id=\"checklist-editor\"]/div[2]/section[3]/div[1]/div/div/header");

    By savedMsgChk = By.xpath("//*[contains(text(), 'TestWelcomeMsg')]");
    By editedMsgChk = By.xpath("//*[contains(text(), 'TestWelcomeMsgM')]");
    By deleteMsg = By.xpath("//*[@id=\"checklist-editor\"]/div[2]/section[3]/div[2]/div/div/section/div[2]/div/div/div/div[2]/div[1]");
    By editMsg = By.xpath("//*[@id=\"checklist-editor\"]/div[2]/section[3]/div[2]/div/div/section/div[2]/div/div/div/div[2]/div[2]");
    By welcomeMsgBtn = By.xpath("//*[@id=\"checklist-editor\"]/div[2]/section[3]/div[3]/div/div/header");
    By welcomeMsgBody = By.xpath("//*[@id=\"checklist-editor\"]/div[2]/section[3]/div[3]/div/div/section/div/div[2]");
    By welcomeMsgSaveBtn = By.xpath("//*[@id=\"checklist-editor\"]/div[2]/section[3]/div[3]/div/div/section/button");

    public CheckListWcMsg(WebDriver driver) {
        super(driver);
    }

    public CheckListWcMsg checkListWcMsg() throws Exception {
        RunCases.test = RunCases.extent.createTest("Welcome message Test");
        waitForVisibilityOf(checkListTab);

        driver.findElement(checkListTab).click();
        waitForVisibilityOf(createCategoryBtn);
        System.out.println("On the checklist page");
        RunCases.test.log(Status.PASS, MarkupHelper.createLabel("On the checklist page", ExtentColor.GREEN));

        waitForVisibilityOf(enterCategory);
        driver.findElement(enterCategory).click();
        waitForVisibilityOf(enterChecklist);
        Thread.sleep(1000);
        driver.findElement(enterChecklist).click();
        waitForVisibilityOf(checkLogo);
        Thread.sleep(2000);

        //for scrolling the page, can move up or down by changing the element as per your needs
        WebElement element = driver.findElement(addGroupBtn);
        ((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView();", element);

        boolean check = true;
        while( check == true){
            Thread.sleep(3000);
            if (driver.findElements(deleteQuestionGroup).size() > 0){
                driver.findElement(deleteQuestionGroup).click();
                waitForVisibilityOf(yesConfirmationDialog);
                driver.findElement(yesConfirmationDialog).click();
                System.out.println("Existing question group deleted");
                RunCases.test.log(Status.PASS, MarkupHelper.createLabel("Existing question group deleted", ExtentColor.GREEN));

            }else {
                check = false;
            }
        }

        Thread.sleep(2000);
        WebElement element4 = driver.findElement(showAdvanceOptionBtn);
        ((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView();", element4);
        Thread.sleep(2000);

        driver.findElement(showAdvanceOptionBtn).click();

        WebElement element5 = driver.findElement(reportPhotoBtn);
        ((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView();", element5);
        Thread.sleep(2000);

        waitForVisibilityOf(welcomeMsgBtn);
        driver.findElement(welcomeMsgBtn).click();

        waitForClickabilityOf(welcomeMsgBody);
        WebElement element7 = driver.findElement(welcomeMsgBody);
        Actions actions = new Actions(driver);
        actions.moveToElement(element7);
        actions.click();
        actions.sendKeys("TestWelcomeMsg");
        actions.build().perform();
        Thread.sleep(2000);

        WebElement element9 = driver.findElement(welcomeMsgSaveBtn);
        ((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView();", element9);
        Thread.sleep(1000);
        driver.findElement(welcomeMsgSaveBtn).click();
        Thread.sleep(1000);

        driver.findElement(savedMsgChk);
        Thread.sleep(1000);
        System.out.println("Welcome message saved successfully");
        RunCases.test.log(Status.PASS, MarkupHelper.createLabel("Welcome message saved successfully", ExtentColor.GREEN));

        actions.moveToElement(element7);
        actions.click();
        actions.sendKeys("M");
        actions.build().perform();
        Thread.sleep(1000);
        driver.findElement(welcomeMsgSaveBtn).click();
        Thread.sleep(1000);
        driver.findElement(editedMsgChk);
        System.out.println("Welcome message edited successfully");
        RunCases.test.log(Status.PASS, MarkupHelper.createLabel("Welcome message edited successfully", ExtentColor.GREEN));
        
        return new CheckListWcMsg(driver);
    }
}
