package cases;

import com.aventstack.extentreports.Status;
import com.aventstack.extentreports.markuputils.ExtentColor;
import com.aventstack.extentreports.markuputils.MarkupHelper;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.testng.Assert;
import settings.HelperClass;
import settings.RunCases;

import java.awt.*;
import java.awt.datatransfer.StringSelection;
import java.awt.event.KeyEvent;
import java.io.File;

public class CheckListAdvanced extends HelperClass {
    By createCategoryBtn =By.xpath("//*[@id=\"sub-body\"]/section/div/div/div/header/div[1]/button");
    By checkListTab = By.xpath("//*[@id=\"sub-body\"]/aside/menu/ul/li[3]");
    By checkLogo = By.xpath("//*[@id=\"checklist-editor\"]/div[2]/div[1]/div/section/form/div/div[1]/div/div/a/img");

    By enterCategory = By.xpath("//*[@id=\"sub-body\"]/section/div/div/div/ul/li[1]/div/div/div[1]/a/h3");
    By enterChecklist = By.xpath("/html/body/div[1]/section/div/div/div/ul/li/div/div/a");
    By addGroupBtn = By.xpath("/html/body/div[1]/section/div/div/div[2]/div[3]/div[2]/div[1]/div[1]");

    By deleteQuestionGroup = By.xpath("//*[@id=\"checklist-editor\"]/div[2]/div[3]/section/div/div/div[2]/header/aside/div[1]");
    By yesConfirmationDialog = By.xpath("//*[@id=\"confirm-modal\"]/section/menu/button[2]");

    By showAdvanceOptionText = By.xpath("//*[contains(text(), 'Show Advanced Options')]");
    By scoringTab = By.xpath("//*[@id=\"checklist-editor\"]/div[2]/div[3]/section/div/div/div[3]/ul/li/section[2]/div/header/div[5]");
    By showAdvanceOptionBtn = By.xpath("//*[@id=\"checklist-editor\"]/div[2]/section[2]/div/div/button/div");

    By reportPhotoBtn = By.xpath("//*[@id=\"checklist-editor\"]/div[2]/section[3]/div[1]/div/div/header");

    By conditionalMessagesBtn = By.xpath("/html/body/div[1]/section/div/div/div[2]/section[3]/div[2]/div/div/header");
    By addMessageBtn = By.xpath("/html/body/div[1]/section/div/div/div[2]/section[3]/div[2]/div/div/section/div[1]/div[1]/div");
    By saveMessageBtn = By.xpath("/html/body/div[1]/section/div/div/div[2]/section[3]/div[2]/div/div/section/div[2]/div/div/div/div[2]/div[3]");
    By messageBody = By.xpath("//*[@id=\"checklist-editor\"]/div[2]/section[3]/div[2]/div/div/section/div[2]/div/div/div/div[1]/section[2]/div[1]/div[2]");
    By textFunction = By.xpath("//*[@id=\"checklist-editor\"]/div[2]/section[3]/div[2]/div/div/section/div[2]/div/div/div/div[1]/section[2]/div[1]/div[1]");

    By savedMsgChk = By.xpath("//*[contains(text(), 'TestMessage')]");
    By editedMsgChk = By.xpath("//*[contains(text(), 'TestMessageM')]");
    By deleteMsg = By.xpath("//*[@id=\"checklist-editor\"]/div[2]/section[3]/div[2]/div/div/section/div[2]/div/div/div/div[2]/div[1]");
    By editMsg = By.xpath("//*[@id=\"checklist-editor\"]/div[2]/section[3]/div[2]/div/div/section/div[2]/div/div/div/div[2]/div[2]");

    public CheckListAdvanced(WebDriver driver) {
        super(driver);
    }

    public CheckListAdvanced checkListAdvanced() throws Exception {
        RunCases.test = RunCases.extent.createTest("Checklist advanced Test");
        waitForVisibilityOf(checkListTab);

        driver.findElement(checkListTab).click();
        waitForVisibilityOf(createCategoryBtn);
        System.out.println("On the checklist page");
        RunCases.test.log(Status.PASS, MarkupHelper.createLabel("On the checklist page", ExtentColor.GREEN));

        waitForVisibilityOf(enterCategory);
        driver.findElement(enterCategory).click();
        waitForVisibilityOf(enterChecklist);
        Thread.sleep(1000);
        driver.findElement(enterChecklist).click();
        waitForVisibilityOf(checkLogo);
        Thread.sleep(2000);

        //for scrolling the page, can move up or down by changing the element as per your needs
        WebElement element = driver.findElement(addGroupBtn);
        ((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView();", element);

        boolean check = true;
        while( check == true){
            Thread.sleep(3000);
            if (driver.findElements(deleteQuestionGroup).size() > 0){
                driver.findElement(deleteQuestionGroup).click();
                waitForVisibilityOf(yesConfirmationDialog);
                driver.findElement(yesConfirmationDialog).click();
                System.out.println("Existing question group deleted");
                RunCases.test.log(Status.PASS, MarkupHelper.createLabel("Existing question group deleted", ExtentColor.GREEN));

            }else {
                check = false;
            }
        }

        Thread.sleep(2000);
        WebElement element4 = driver.findElement(showAdvanceOptionBtn);
        ((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView();", element4);
        Thread.sleep(2000);

        driver.findElement(showAdvanceOptionBtn).click();

        WebElement element5 = driver.findElement(reportPhotoBtn);
        ((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView();", element5);
        Thread.sleep(2000);

        waitForVisibilityOf(reportPhotoBtn);

        driver.findElement(showAdvanceOptionBtn).click();
        Thread.sleep(2000);

        if (driver.findElements(showAdvanceOptionText).size() > 0){
            System.out.println("Show/Hide advance option working");
            RunCases.test.log(Status.PASS, MarkupHelper.createLabel("Show/Hide advance option working", ExtentColor.GREEN));
        }else {
            System.out.println("Show/Hide advance option not working");
            RunCases.test.log(Status.FAIL, MarkupHelper.createLabel("Show/Hide advance option not working", ExtentColor.RED));
            driver.findElement(makeFail);
        }

        driver.findElement(showAdvanceOptionBtn).click();
        waitForClickabilityOf(reportPhotoBtn);
        WebElement elem = driver.findElement(By.xpath("//input[@type='file']"));
        File file = new File(logoPath);
        elem.sendKeys(file.getAbsolutePath());

        waitForVisibilityOf(conditionalMessagesBtn);
        driver.findElement(conditionalMessagesBtn).click();
        waitForClickabilityOf(addMessageBtn);
        driver.findElement(addMessageBtn).click();

        WebElement element6 = driver.findElement(textFunction);
        ((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView();", element6);

        WebElement element7 = driver.findElement(messageBody);
        Actions actions = new Actions(driver);
        actions.moveToElement(element7);
        actions.click();
        actions.sendKeys("TestMessage");
        actions.build().perform();
        Thread.sleep(2000);

        driver.findElement(saveMessageBtn).click();
        waitForVisibilityOf(savedMsgChk);
        System.out.println("Conditional message saved successfully");
        RunCases.test.log(Status.PASS, MarkupHelper.createLabel("Conditional message saved successfully", ExtentColor.GREEN));

        Thread.sleep(1000);
        driver.findElement(editMsg).click();
        Thread.sleep(1000);
        actions.moveToElement(element7);
        actions.click();
        actions.sendKeys("M");
        actions.build().perform();
        Thread.sleep(1000);
        driver.findElement(saveMessageBtn).click();
        Thread.sleep(1000);
        driver.findElement(editedMsgChk);
        System.out.println("Conditional message edited successfully");
        RunCases.test.log(Status.PASS, MarkupHelper.createLabel("Conditional message edited successfully", ExtentColor.GREEN));

        waitForClickabilityOf(deleteMsg);
        driver.findElement(deleteMsg).click();
        Thread.sleep(2000);
        if (driver.findElements(editedMsgChk).size() > 0){
            System.out.println("Conditional message delete not working");
            RunCases.test.log(Status.FAIL, MarkupHelper.createLabel("Conditional message delete not working", ExtentColor.RED));
            driver.findElement(makeFail);
        }else {
            System.out.println("Conditional message delete working");
            RunCases.test.log(Status.FAIL, MarkupHelper.createLabel("Conditional message delete working", ExtentColor.RED));
        }

        
        return new CheckListAdvanced(driver);
    }
}
