package cases;

import com.aventstack.extentreports.Status;
import com.aventstack.extentreports.markuputils.ExtentColor;
import com.aventstack.extentreports.markuputils.MarkupHelper;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import settings.HelperClass;
import settings.RunCases;

public class CheckListNotification extends HelperClass {
    By createCategoryBtn =By.xpath("//*[@id=\"sub-body\"]/section/div/div/div/header/div[1]/button");
    By checkListTab = By.xpath("//*[@id=\"sub-body\"]/aside/menu/ul/li[3]");
    By checkLogo = By.xpath("//*[@id=\"checklist-editor\"]/div[2]/div[1]/div/section/form/div/div[1]/div/div/a/img");

    By enterCategory = By.xpath("//*[@id=\"sub-body\"]/section/div/div/div/ul/li[1]/div/div/div[1]/a/h3");
    By enterChecklist = By.xpath("/html/body/div[1]/section/div/div/div/ul/li/div/div/a");
    By addGroupBtn = By.xpath("/html/body/div[1]/section/div/div/div[2]/div[3]/div[2]/div[1]/div[1]");
    By deleteQuestionGroup = By.xpath("//*[@id=\"checklist-editor\"]/div[2]/div[3]/section/div/div/div[2]/header/aside/div[1]");
    By yesConfirmationDialog = By.xpath("//*[@id=\"confirm-modal\"]/section/menu/button[2]");

    By showAdvanceOptionBtn = By.xpath("//*[@id=\"checklist-editor\"]/div[2]/section[2]/div/div/button/div");
    By reportPhotoBtn = By.xpath("//*[@id=\"checklist-editor\"]/div[2]/section[3]/div[1]/div/div/header");

    By chkModSub = By.xpath("//*[contains(text(), 'ModSub')]");
    By testSubChk = By.xpath("//*[contains(text(), 'TestSubject')]");
    By testEmailBodyChk = By.xpath("//*[contains(text(), 'TestMessageBody')]");

    By notificationBtn = By.xpath("//*[@id=\"checklist-editor\"]/div[2]/section[3]/div[4]/div/div/header");
    By addNotificationBtn = By.xpath("//*[@id=\"checklist-editor\"]/div[2]/section[3]/div[4]/div/div/section/section/div[1]/div[1]/div/span");

    By toField = By.xpath("//*[@id=\"checklist-editor\"]/div[2]/section[3]/div[4]/div/div/section/section/div[2]/div/div/div/div[1]/section[2]/div[2]/div[1]/div[1]/input");
    By toFieldAddBtn = By.xpath("//*[@id=\"checklist-editor\"]/div[2]/section[3]/div[4]/div/div/section/section/div[2]/div/div/div/div[1]/section[2]/div[2]/div[1]/div[2]/div");
    By deletedAddedContact = By.xpath("//*[@id=\"checklist-editor\"]/div[2]/section[3]/div[4]/div/div/section/section/div[2]/div/div/div/div[1]/section[2]/div[2]/div[2]/div/ul/li/div/div[2]/div");

    By toFieldContactBtn = By.xpath("//*[@id=\"checklist-editor\"]/div[2]/section[3]/div[4]/div/div/section/section/div[2]/div/div/div/div[1]/section[2]/div[2]/div[1]/div[3]/div/div");
    By toFieldSelectUser = By.xpath("//*[@id=\"usergroup-user-search\"]");
    By selectUser = By.xpath("//*[@id=\"usergroup-user-search-overlay\"]/div[2]/div[1]");
    By addUser = By.xpath("//*[@id=\"checklist-editor\"]/div[2]/section[3]/div[4]/div/div/section/section/div[2]/div/div/div/div[1]/section[2]/div[2]/div[1]/div[3]/div/section/div[2]/div[2]/div");

    By ccField = By.xpath("//*[@id=\"checklist-editor\"]/div[2]/section[3]/div[4]/div/div/section/section/div[2]/div/div/div/div[1]/section[2]/div[3]/div[1]/div[1]/input");
    By ccFieldAddBtn = By.xpath("//*[@id=\"checklist-editor\"]/div[2]/section[3]/div[4]/div/div/section/section/div[2]/div/div/div/div[1]/section[2]/div[3]/div[1]/div[2]/div");
    By deleteCCBtn = By.xpath("//*[@id=\"checklist-editor\"]/div[2]/section[3]/div[4]/div/div/section/section/div[2]/div/div/div/div[1]/section[2]/div[2]/div[2]/div/ul/li/div/div[2]/div");
    By subjectField = By.xpath("//*[@id=\"checklist-editor\"]/div[2]/section[3]/div[4]/div/div/section/section/div[2]/div/div/div/div[1]/section[2]/div[4]/input");

    By addRequirementSet = By.xpath("//*[@id=\"checklist-editor\"]/div[2]/section[3]/div[4]/div/div/section/section/div[2]/div/div/div/div[1]/section[2]/div[5]/div/div");
    By deleteRequirementSet = By.xpath("//*[@id=\"checklist-editor\"]/div[2]/section[3]/div[4]/div/div/section/section/div[2]/div/div/div/div[1]/section[2]/div[6]/div/div[1]/div[2]/div");

    By emailBody = By.xpath("//*[@id=\"checklist-editor\"]/div[2]/section[3]/div[4]/div/div/section/section/div[2]/div/div/div/div[1]/section[2]/div[6]/div[2]/div[2]");
    By saveEmailBodyBtn = By.xpath("//*[@id=\"checklist-editor\"]/div[2]/section[3]/div[4]/div/div/section/section/div[2]/div/div/div/div[1]/section[2]/div[7]/div[1]/button[1]");

    By saveEmailNotificationBtn = By.xpath("//*[@id=\"checklist-editor\"]/div[2]/section[3]/div[4]/div/div/section/section/div[2]/div/div/div/div[2]/div[3]");
    By editEmailNotification = By.xpath("//*[@id=\"checklist-editor\"]/div[2]/section[3]/div[4]/div/div/section/section/div[2]/div/div/div/div[2]/div[2]");
    By deleteEmailNotification = By.xpath("//*[@id=\"checklist-editor\"]/div[2]/section[3]/div[4]/div/div/section/section/div[2]/div/div/div/div[2]/div[1]");

    public CheckListNotification(WebDriver driver) {
        super(driver);
    }

    public CheckListNotification checkListNotification() throws Exception {
        RunCases.test = RunCases.extent.createTest("Notification Test");
        waitForVisibilityOf(checkListTab);

        driver.findElement(checkListTab).click();
        waitForVisibilityOf(createCategoryBtn);
        System.out.println("On the checklist page");
        RunCases.test.log(Status.PASS, MarkupHelper.createLabel("On the checklist page", ExtentColor.GREEN));

        waitForVisibilityOf(enterCategory);
        driver.findElement(enterCategory).click();
        waitForVisibilityOf(enterChecklist);
        Thread.sleep(1000);
        driver.findElement(enterChecklist).click();
        waitForVisibilityOf(checkLogo);
        Thread.sleep(2000);

        //for scrolling the page, can move up or down by changing the element as per your needs
        WebElement element = driver.findElement(addGroupBtn);
        ((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView();", element);

        boolean check = true;
        while( check == true){
            Thread.sleep(3000);
            if (driver.findElements(deleteQuestionGroup).size() > 0){
                driver.findElement(deleteQuestionGroup).click();
                waitForVisibilityOf(yesConfirmationDialog);
                driver.findElement(yesConfirmationDialog).click();
                System.out.println("Existing question group deleted");
                RunCases.test.log(Status.PASS, MarkupHelper.createLabel("Existing question group deleted", ExtentColor.GREEN));

            }else {
                check = false;
            }
        }

        driver.findElement(showAdvanceOptionBtn).click();
        WebElement element5 = driver.findElement(reportPhotoBtn);
        ((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView();", element5);
        Thread.sleep(2000);

        waitForVisibilityOf(notificationBtn);
        driver.findElement(notificationBtn).click();

        boolean check1 = true;
        while( check1 == true){
            Thread.sleep(3000);
            if (driver.findElements(deleteEmailNotification).size() > 0){
                driver.findElement(deleteEmailNotification).click();
                Thread.sleep(2000);
                System.out.println("Existing Email notification deleted");
                RunCases.test.log(Status.PASS, MarkupHelper.createLabel("Existing Email notification deleted", ExtentColor.GREEN));

            }else {
                check1 = false;
            }
        }

        waitForClickabilityOf(addNotificationBtn);
        Thread.sleep(3000);
        driver.findElement(addNotificationBtn).click();

        /*driver.findElement(typeField).click();
        Thread.sleep(1000);
        driver.findElement(emailOption).click();
        System.out.println("Email selected");
        RunCases.test.log(Status.PASS, MarkupHelper.createLabel("Email selected", ExtentColor.GREEN));*/

        driver.findElement(toField);
        driver.findElement(toField).click();
        driver.findElement(toField).sendKeys("test@email.com");
        Thread.sleep(1000);
        driver.findElement(toFieldAddBtn).click();
        driver.findElement(deletedAddedContact);
        System.out.println("Contact added in to field");
        RunCases.test.log(Status.PASS, MarkupHelper.createLabel("Contact added in to field", ExtentColor.GREEN));

        driver.findElement(deletedAddedContact).click();
        Thread.sleep(2000);

        if (driver.findElements(deletedAddedContact).size() > 0){
            System.out.println("Contact deleted");
            RunCases.test.log(Status.FAIL, MarkupHelper.createLabel("Contact deleted", ExtentColor.RED));
            driver.findElement(makeFail);
        }else {
            System.out.println("Contact deleted");
            RunCases.test.log(Status.PASS, MarkupHelper.createLabel("Contact deleted", ExtentColor.GREEN));
        }

        waitForClickabilityOf(toFieldContactBtn);
        driver.findElement(toFieldContactBtn).click();
        waitForVisibilityOf(toFieldSelectUser);
        driver.findElement(toFieldSelectUser).click();
        Thread.sleep(3000);
        driver.findElement(selectUser).click();
        Thread.sleep(2000);
        driver.findElement(toFieldSelectUser).click();
        waitForClickabilityOf(addUser);
        driver.findElement(addUser).click();
        driver.findElement(deletedAddedContact);

        System.out.println("Email added from contact list");
        RunCases.test.log(Status.PASS, MarkupHelper.createLabel("Email added from contact list", ExtentColor.GREEN));

        waitForClickabilityOf(ccField);
        driver.findElement(ccField).sendKeys("testcc@email.com");
        Thread.sleep(1000);
        driver.findElement(ccFieldAddBtn).click();
        Thread.sleep(1000);
        driver.findElement(deleteCCBtn);

        System.out.println("Email added at CC field");
        RunCases.test.log(Status.PASS, MarkupHelper.createLabel("Email added at CC field", ExtentColor.GREEN));

        Thread.sleep(1000);
        driver.findElement(subjectField).click();
        driver.findElement(subjectField).sendKeys("TestSubject");
        driver.findElement(testSubChk);
        System.out.println("Subject entered");
        RunCases.test.log(Status.PASS, MarkupHelper.createLabel("Subject entered", ExtentColor.GREEN));

        Thread.sleep(1000);
        driver.findElement(addRequirementSet).click();
        Thread.sleep(1000);
        driver.findElement(deleteRequirementSet);
        System.out.println("Requirement set created");
        RunCases.test.log(Status.PASS, MarkupHelper.createLabel("Requirement set created", ExtentColor.GREEN));

        Thread.sleep(1000);
        driver.findElement(deleteRequirementSet).click();
        System.out.println("Requirement set Deleted");
        RunCases.test.log(Status.PASS, MarkupHelper.createLabel("Requirement set Deleted", ExtentColor.GREEN));

        WebElement element7 = driver.findElement(emailBody);
        Actions actions = new Actions(driver);
        actions.moveToElement(element7);
        actions.click();
        actions.sendKeys("TestMessageBody");
        actions.build().perform();
        Thread.sleep(2000);

        driver.findElement(saveEmailBodyBtn).click();
        Thread.sleep(1000);
        driver.findElement(testEmailBodyChk);
        System.out.println("Text inserted in the email body and saved");
        RunCases.test.log(Status.PASS, MarkupHelper.createLabel("Text inserted in the email body and saved", ExtentColor.GREEN));

        driver.navigate().refresh();
        waitForVisibilityOf(checkLogo);
        WebElement element6 = driver.findElement(showAdvanceOptionBtn);
        ((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView();", element6);
        Thread.sleep(2000);

        driver.findElement(showAdvanceOptionBtn).click();

        WebElement element8 = driver.findElement(notificationBtn);
        ((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView();", element8);
        Thread.sleep(2000);

        driver.findElement(notificationBtn).click();
        waitForVisibilityOf(testSubChk);

        System.out.println("Notification created successfully");
        RunCases.test.log(Status.PASS, MarkupHelper.createLabel("Notification created successfully", ExtentColor.GREEN));

        Thread.sleep(1000);
        driver.findElement(editEmailNotification).click();

        waitForClickabilityOf(subjectField);
        driver.findElement(subjectField).clear();
        Thread.sleep(1000);
        driver.findElement(subjectField).sendKeys("ModSub");
        Thread.sleep(1000);

        driver.findElement(saveEmailNotificationBtn).click();
        waitForVisibilityOf(chkModSub);
        System.out.println("Notification modified");
        RunCases.test.log(Status.PASS, MarkupHelper.createLabel("Notification modified", ExtentColor.GREEN));

        waitForClickabilityOf(deleteEmailNotification);
        driver.findElement(deleteEmailNotification).click();
        Thread.sleep(2000);

        if (driver.findElements(deleteEmailNotification).size() > 0){
            System.out.println("Email notification delete Not working");
            RunCases.test.log(Status.FAIL, MarkupHelper.createLabel("Email notification delete Not working", ExtentColor.RED));
            driver.findElement(makeFail);
        }else {
            System.out.println("Email notification deleted");
            RunCases.test.log(Status.PASS, MarkupHelper.createLabel("Email notification deleted", ExtentColor.GREEN));
        }


        return new CheckListNotification(driver);
    }
}
