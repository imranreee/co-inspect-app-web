package cases;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import settings.HelperClass;
import settings.RunCases;

public class SettingsAuth extends HelperClass {
    By settingsTab = By.xpath("//*[@id=\"sub-body\"]/aside/menu/ul/li[7]");
    By authTab = By.xpath("//*[@id=\"sub-body\"]/section/div/div/div[1]/header/div[5]");
    By adminOne = By.xpath("//*[@id=\"saml-editor\"]/div[1]/div/div[1]/div[1]/input");
    By agentOne = By.xpath("//*[@id=\"saml-editor\"]/div[1]/div/div[1]/div[2]/input");
    By saveBtnRole = By.xpath("//*[@id=\"saml-editor\"]/div[1]/div/div[2]/div[2]/div");
    By enableSamlIntegration = By.xpath("//*[@id=\"enable-saml\"]");
    By entrypointFiled = By.xpath("//*[@id=\"saml-editor\"]/div[3]/div/div[1]/div/input");

    By adminTwo = By.xpath("//*[@id=\"saml-editor\"]/div[4]/div[1]/div/textarea");
    By agentTwo = By.xpath("//*[@id=\"saml-editor\"]/div[4]/div[2]/div/textarea");
    By saveBtnUpdateTimeZone = By.xpath("//*[@id=\"saml-editor\"]/div[5]/div[2]/div");

    public SettingsAuth(WebDriver driver) {
        super(driver);
    }

    public SettingsAuth settingsAuth() throws InterruptedException {
        RunCases.test = RunCases.extent.createTest("Settings Auth Test");
        waitForVisibilityOf(settingsTab);
        driver.findElement(settingsTab).click();

        waitForVisibilityOf(authTab);
        driver.findElement(authTab).click();
        Thread.sleep(2000);

        driver.findElement(adminOne).clear();
        driver.findElement(adminOne).sendKeys("TestAdminOne");

        driver.findElement(agentOne).clear();
        driver.findElement(agentOne).sendKeys("TestAgentOne");

        driver.findElement(saveBtnRole).click();
        Thread.sleep(2000);
        System.out.println("Role Level saved");

        driver.findElement(enableSamlIntegration).click();
        driver.findElement(entrypointFiled).clear();
        driver.findElement(entrypointFiled).sendKeys("TestString");

        driver.findElement(adminTwo).clear();
        driver.findElement(adminTwo).sendKeys("TestAdminTwo");

        driver.findElement(agentTwo).clear();
        driver.findElement(agentTwo).sendKeys("TestAgentTwo");

        driver.findElement(saveBtnUpdateTimeZone).click();
        Thread.sleep(2000);
        System.out.println("Default time zone updated");


        return new SettingsAuth(driver);
    }
}
