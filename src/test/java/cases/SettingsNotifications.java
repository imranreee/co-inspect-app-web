package cases;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import settings.HelperClass;
import settings.RunCases;

public class SettingsNotifications extends HelperClass {
    By settingsTab = By.xpath("//*[@id=\"sub-body\"]/aside/menu/ul/li[7]");
    By notificationTab = By.xpath("//*[@id=\"sub-body\"]/section/div/div/div[1]/header/div[2]");


    By createdEmailNoti = By.xpath("//*[contains(text(), 'TestSubject')]");
    By modifiedSubject = By.xpath("//*[contains(text(), 'ModifiedSubject')]");
    By modifiedEmail = By.xpath("//*[contains(text(), 'mod@email.com')]");
    By modifiedEmail2 = By.xpath("//*[contains(text(), 'mod2@email.com')]");
    By selectEmailType = By.xpath("//*[contains(text(), 'Email')]");

    By createNew = By.xpath("//*[@id=\"sub-body\"]/section/div/div/div[1]/section[2]/div/div[1]/header/div/div/a/span");
    By typeFiled = By.xpath("//*[@id=\"sub-body\"]/section/div/div/div[1]/section[2]/div/div[2]/div/form/div/div[1]/div/select");
    By toFiled = By.xpath("//*[@id=\"sub-body\"]/section/div/div/div[1]/section[2]/div/div[2]/div/form/div/div[2]/div[1]/div[1]/input");
    By plusBtnTo = By.xpath("//*[@id=\"sub-body\"]/section/div/div/div[1]/section[2]/div/div[2]/div/form/div/div[2]/div[1]/div[2]");
    By contactBtnTo = By.xpath("//*[@id=\"sub-body\"]/section/div/div/div[1]/section[2]/div/div[2]/div/form/div/div[2]/div[1]/div[3]/div/div");
    By ccFiled = By.xpath("//*[@id=\"sub-body\"]/section/div/div/div[1]/section[2]/div/div[2]/div/form/div/div[3]/div[1]/div[1]/input");
    By saveBtn = By.xpath("//*[@id=\"sub-body\"]/section/div/div/div[1]/section[2]/div/div[2]/div/form/div/div[7]/div/button[1]");
    By cancelBtn = By.xpath("//*[@id=\"sub-body\"]/section/div/div/div[1]/section[2]/div/div[2]/div/form/div/div[7]/div/button[3]");


    By subjectFiled = By.xpath("//*[@id=\"sub-body\"]/section/div/div/div[1]/section[2]/div/div[2]/div/form/div/div[4]/input");
    By emailBody = By.id("//*[@id=\"sub-body\"]/section/div/div/div[1]/section[2]/div/div[2]/div/form/div/div[6]/div[2]/div[2]");
    By plusBtnCc = By.xpath("//*[@id=\"sub-body\"]/section/div/div/div[1]/section[2]/div/div[2]/div/form/div/div[3]/div[1]/div[2]/div");
    By deleteTo = By.xpath("//*[@id=\"sub-body\"]/section/div/div/div[1]/section[2]/div/div[2]/div/form/div/div[2]/div[2]/div[1]/ul/li[1]/div/div[2]/div");

    By deleteEmailNoti = By.xpath("//*[@id=\"sub-body\"]/section/div/div/div[1]/section[2]/div/div[1]/section/ul/li/div/div[2]/div");
    By yesBtnDialog = By.xpath("//*[@id=\"confirm-modal\"]/section/menu/button[2]");

    public SettingsNotifications(WebDriver driver) {
        super(driver);
    }

    public SettingsNotifications settingsNotifications() throws InterruptedException {
        RunCases.test = RunCases.extent.createTest("Settings Notifications Test");
        waitForVisibilityOf(settingsTab);
        driver.findElement(settingsTab).click();

        waitForVisibilityOf(notificationTab);
        driver.findElement(notificationTab).click();
        waitForVisibilityOf(createNew);
        driver.findElement(createNew).click();
        Thread.sleep(1000);
        driver.findElement(typeFiled).click();
        driver.findElement(selectEmailType).click();

        driver.findElement(toFiled).sendKeys("test@selenium.com");
        driver.findElement(plusBtnTo).click();
        Thread.sleep(1000);
        driver.findElement(toFiled).sendKeys("test2@selenium.com");
        driver.findElement(plusBtnTo).click();

        driver.findElement(ccFiled).sendKeys("test3@selenium.com");
        driver.findElement(plusBtnCc).click();
        Thread.sleep(1000);

        WebElement element = driver.findElement(subjectFiled); //for scrolling the page, can move up or down by changing the element as pre your needs
        ((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView();", element);

        driver.findElement(subjectFiled).sendKeys("TestSubject");

        WebElement element2 = driver.findElement(saveBtn); //for scrolling the page, can move up or down by changing the element as pre your needs
        ((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView();", element2);

        /*driver.findElement(emailBody).click();
        driver.findElement(emailBody).sendKeys("This is test email from automation please ignore it");
        Thread.sleep(1000);*/

        driver.findElement(saveBtn).click();
        Thread.sleep(2000);
        driver.findElement(cancelBtn).click();
        Thread.sleep(1000);

        driver.findElement(createdEmailNoti);
        System.out.println("Email notification created");

        System.out.println("Current subject: "+driver.findElement(createdEmailNoti).getText());

        driver.findElement(createdEmailNoti).click();
        Thread.sleep(2000);
        driver.findElement(deleteTo).click();
        Thread.sleep(1000);
        driver.findElement(deleteTo).click();

        driver.findElement(toFiled).sendKeys("mod@email.com");
        driver.findElement(plusBtnTo).click();
        Thread.sleep(1000);
        driver.findElement(toFiled).sendKeys("mod2@email.com");
        driver.findElement(plusBtnTo).click();
        Thread.sleep(1000);

        driver.findElement(subjectFiled).clear();
        driver.findElement(subjectFiled).sendKeys("ModifiedSubject");
        Thread.sleep(1000);

        WebElement element3 = driver.findElement(saveBtn);
        ((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView();", element3);

        driver.findElement(saveBtn).click();
        Thread.sleep(1000);
        driver.findElement(modifiedSubject);
        driver.findElement(modifiedEmail);
        driver.findElement(modifiedEmail2);
        System.out.println("Email and subject modified");

        driver.findElement(deleteEmailNoti).click();
        waitForVisibilityOf(yesBtnDialog);
        driver.findElement(yesBtnDialog).click();
        waitForVisibilityOf(createNew);

        driver.navigate().refresh();
        waitForVisibilityOf(notificationTab);
        driver.findElement(notificationTab).click();
        driver.findElement(createNew);

        if (driver.findElements(modifiedSubject).size() > 0){
            System.out.println("Delete email not working");
            driver.findElement(makeFail);
        }else {
            System.out.println("Delete email worked");
        }
        return new SettingsNotifications(driver);
    }
}
