package cases;

import com.aventstack.extentreports.Status;
import com.aventstack.extentreports.markuputils.ExtentColor;
import com.aventstack.extentreports.markuputils.MarkupHelper;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import settings.HelperClass;
import settings.RunCases;

public class UsersInvalidEntries extends HelperClass {
    By usersTab = By.xpath("/html/body/div[1]/aside/menu/ul/li[4]");
    By createNewUserBtn = By.xpath("//*[@id=\"sub-body\"]/section/div/div/div[1]/header[2]/div/div[1]/a");

    By changeImage = By.xpath("/html/body/div[1]/section/div/div/div[2]/form/div[1]/div[1]/a/img");
    By selectImage = By.xpath("/html/body/div[1]/section/div/div/div[1]/section/div/img[8]");
    By selectImageBtn = By.xpath("/html/body/div[1]/section/div/div/div[1]/section/menu/button[2]");
    By firstNameFiled = By.xpath("/html/body/div[1]/section/div/div/div[2]/form/div[1]/div[2]/div[1]/input");
    By lastNameFiled = By.xpath("/html/body/div[1]/section/div/div/div[2]/form/div[1]/div[2]/div[2]/input");
    By accountRoleAdmin = By.xpath("/html/body/div[1]/section/div/div/div[2]/form/div[2]/div/div/label[3]");

    By usernameField = By.xpath("/html/body/div[1]/section/div/div/div[2]/form/div[3]/div[1]/div[1]/input");
    By usernameOkBtn = By.xpath("/html/body/div[1]/section/div/div/div[2]/form/div[3]/div[1]/div[1]/div[1]");
    By usernameEditBtn = By.xpath("/html/body/div[1]/section/div/div/div[2]/form/div[3]/div[1]/div[1]/div[2]");

    By contactEmailField = By.xpath("/html/body/div[1]/section/div/div/div[2]/form/div[3]/div[2]/div[1]/input");
    By emailOkBtn = By.xpath("/html/body/div[1]/section/div/div/div[2]/form/div[3]/div[2]/div[1]/div[1]");
    By emailEditBtn = By.xpath("/html/body/div[1]/section/div/div/div[2]/form/div[3]/div[2]/div[1]/div[2]");

    By loginPasswordField = By.xpath("/html/body/div[1]/section/div/div/div[2]/form/div[3]/div[3]/div[1]/input");
    By loginPasswordOkBtn = By.xpath("/html/body/div[1]/section/div/div/div[2]/form/div[3]/div[3]/div[1]/div[1]");
    By loginPasswordEditBtn = By.xpath("/html/body/div[1]/section/div/div/div[2]/form/div[3]/div[3]/div[1]/div[2]");

    By loginPasswordVerifyField = By.xpath("/html/body/div[1]/section/div/div/div[2]/form/div[3]/div[4]/div[1]/input");
    By loginPasswordVerifyOkBtn = By.xpath("/html/body/div[1]/section/div/div/div[2]/form/div[3]/div[4]/div[1]/div[1]");
    By loginPasswordVerifyEditBtn = By.xpath("/html/body/div[1]/section/div/div/div[2]/form/div[3]/div[4]/div[1]/div[2]");

    By saveBtn = By.xpath("/html/body/div[1]/section/div/div/div[2]/form/div[4]/div[1]");
    By deleteBtn = By.xpath("/html/body/div[1]/section/div/div/div[2]/form/div[4]/div[2]");
    By deleteConfirmationBtn = By.xpath("/html/body/div[1]/section/menu/button[2]");
    By checkCreatedUser = By.xpath("//*[text()[contains(.,'ATestUser')]]");

    By firstNameHintText = By.xpath("//*[text()[contains(.,'First name')]]");
    By provideEmailAlert = By.xpath("//*[text()[contains(.,'Please enter a valid email address.')]]");
    By usernameAlreadyExistAlert = By.xpath("//*[text()[contains(.,'This username is already taken, please use another.')]]");
    By emailAlreadyExistAlert = By.xpath("//*[text()[contains(.,'This email address is already taken, please use another.')]]");

    By usersListView = By.xpath("/html/body/div[1]/section/div/div/div[2]/section/ul[2]/li[1]");
    By firstCheckBox = By.xpath("/html/body/div[1]/section/div/div/div[2]/section/ul[2]/li[1]/div[1]/input");
    By secondCheckBox = By.xpath("/html/body/div[1]/section/div/div/div[2]/section/ul[2]/li[2]/div[1]/input");

    By deleteAllSelected = By.xpath("/html/body/div[1]/section/div/div/div[1]/header[1]/div/div[1]/a");
    By yesConfirmationDialog = By.xpath("//*[@id=\"confirm-modal\"]/section/menu/button[2]");
    By searchUserField = By.xpath("/html/body/div[1]/section/div/div/div[1]/header[2]/div/div[2]/input");

    public UsersInvalidEntries(WebDriver driver) {
        super(driver);
    }

    public UsersInvalidEntries usersInvalidEntries() throws Exception {
        RunCases.test = RunCases.extent.createTest("Users invalid entries Test");

        waitForVisibilityOf(usersTab);
        driver.findElement(usersTab).click();
        Thread.sleep(2000);
        if (driver.findElements(checkCreatedUser).size() > 0){
            driver.findElement(checkCreatedUser).click();
            Thread.sleep(1000);
            driver.findElement(deleteBtn).click();
            waitForVisibilityOf(deleteConfirmationBtn);
            driver.findElement(deleteConfirmationBtn).click();
            System.out.println("Existing user deleted");
        }

        waitForVisibilityOf(createNewUserBtn);
        System.out.println("App on the users page");
        RunCases.test.log(Status.PASS, MarkupHelper.createLabel("App on the users page", ExtentColor.GREEN));

        driver.findElement(createNewUserBtn).click();
        waitForVisibilityOf(firstNameFiled);
        driver.findElement(saveBtn).click();
        Thread.sleep(1000);
        driver.findElements(firstNameHintText);
        System.out.println("Fast and last name filed empty checking passed");
        RunCases.test.log(Status.PASS, MarkupHelper.createLabel("Fast and last name filed empty checking passed", ExtentColor.GREEN));

        driver.findElement(firstNameFiled).sendKeys("ATestUser");
        Thread.sleep(1000);
        driver.findElement(lastNameFiled).sendKeys("FromAutomation");
        Thread.sleep(1000);
        driver.findElement(accountRoleAdmin).click();

        driver.findElement(usernameField).sendKeys("testusername");
        Thread.sleep(2000);

        driver.findElement(usernameAlreadyExistAlert);
        driver.findElement(provideEmailAlert);
        System.out.println("Username exist and provide Email alert working fine");
        RunCases.test.log(Status.PASS, MarkupHelper.createLabel("Username exist and provide Email alert working fine", ExtentColor.GREEN));


        driver.findElement(contactEmailField).sendKeys("testtest");
        Thread.sleep(1000);
        driver.findElement(provideEmailAlert);
        System.out.println("Enter valid email alert showing");
        RunCases.test.log(Status.PASS, MarkupHelper.createLabel("Enter valid email alert showing", ExtentColor.GREEN));

        driver.findElement(contactEmailField).clear();
        driver.findElement(contactEmailField).sendKeys("admin@selenium3.coinspectapp.com");
        Thread.sleep(3000);
        driver.findElement(emailAlreadyExistAlert);
        System.out.println("Email already exist alert showing");
        RunCases.test.log(Status.PASS, MarkupHelper.createLabel("Email already exist alert showing", ExtentColor.GREEN));

        driver.findElement(usersTab).click();
        waitForVisibilityOf(usersListView);
        System.out.println("User list found");
        RunCases.test.log(Status.PASS, MarkupHelper.createLabel("User list found", ExtentColor.GREEN));

        for (int i = 0; i < 2; i++){
            driver.findElement(createNewUserBtn).click();
            driver.findElement(changeImage).click();
            Thread.sleep(2000);
            driver.findElement(selectImage).click();
            Thread.sleep(1000);
            driver.findElement(selectImageBtn).click();
            driver.findElement(firstNameFiled);
            System.out.println("Avatar selected");
            RunCases.test.log(Status.PASS, MarkupHelper.createLabel("Avatar selected, as profile photo", ExtentColor.GREEN));

            driver.findElement(firstNameFiled).sendKeys("ATestUser");
            Thread.sleep(1000);
            driver.findElement(lastNameFiled).sendKeys("FromAutomation");
            Thread.sleep(1000);
            driver.findElement(accountRoleAdmin).click();

            //For generating random numeric value
            String randomValue = ""+((int)(Math.random()*9000)+1000);

            driver.findElement(usernameField).sendKeys("testusername"+randomValue);
            Thread.sleep(1000);
            driver.findElement(usernameOkBtn).click();
            driver.findElement(usernameEditBtn);
            System.out.println("Username successfully entered");
            RunCases.test.log(Status.PASS, MarkupHelper.createLabel("Username successfully entered", ExtentColor.GREEN));

            driver.findElement(contactEmailField).sendKeys("test"+randomValue+"@automation.com");
            Thread.sleep(1000);
            driver.findElement(emailOkBtn).click();
            driver.findElement(emailEditBtn);
            System.out.println("Contact email successfully entered");
            RunCases.test.log(Status.PASS, MarkupHelper.createLabel("Contact email successfully entered", ExtentColor.GREEN));

            driver.findElement(loginPasswordField).sendKeys("testpassword");
            Thread.sleep(1000);
            driver.findElement(loginPasswordOkBtn).click();
            driver.findElement(loginPasswordEditBtn);
            Thread.sleep(1000);

            driver.findElement(loginPasswordVerifyEditBtn).click();
            driver.findElement(loginPasswordVerifyField).sendKeys("testpassword");
            Thread.sleep(1000);
            driver.findElement(loginPasswordVerifyOkBtn).click();
            driver.findElement(loginPasswordVerifyEditBtn);

            System.out.println("Password entered successfully");
            RunCases.test.log(Status.PASS, MarkupHelper.createLabel("Password entered successfully", ExtentColor.GREEN));

            Thread.sleep(1000);
            driver.findElement(saveBtn).click();
            Thread.sleep(2000);
            driver.findElement(usersTab).click();
            waitForVisibilityOf(checkCreatedUser);
            System.out.println("User successfully created");
            RunCases.test.log(Status.PASS, MarkupHelper.createLabel("User successfully created", ExtentColor.GREEN));
            Thread.sleep(2000);
        }

        driver.findElement(searchUserField).sendKeys("ATestUser");
        Thread.sleep(3000);
        driver.findElement(checkCreatedUser);

        driver.findElement(searchUserField).clear();
        Thread.sleep(1000);
        driver.findElement(searchUserField).sendKeys("FromAutomation");
        Thread.sleep(3000);
        driver.findElement(checkCreatedUser);
        System.out.println("Search by first name and last name working fine");
        RunCases.test.log(Status.PASS, MarkupHelper.createLabel("Search by first name and last name working fine", ExtentColor.GREEN));

        driver.navigate().refresh();
        waitForVisibilityOf(usersListView);
        driver.findElement(firstCheckBox).click();
        driver.findElement(secondCheckBox).click();
        Thread.sleep(1000);

        driver.findElement(deleteAllSelected).click();
        waitForVisibilityOf(yesConfirmationDialog);
        driver.findElement(yesConfirmationDialog).click();
        waitForVisibilityOf(createNewUserBtn);
        driver.navigate().refresh();
        waitForVisibilityOf(createNewUserBtn);
        Thread.sleep(3000);

        if (driver.findElements(checkCreatedUser).size() > 0){
            System.out.println("Delete all selected not working");
            RunCases.test.log(Status.FAIL, MarkupHelper.createLabel("Delete all selected not working", ExtentColor.RED));
            driver.findElement(makeFail);
        }else {
            System.out.println("Delete all selected worked successfully");
            RunCases.test.log(Status.PASS, MarkupHelper.createLabel("Delete all selected worked successfully", ExtentColor.GREEN));
        }

        return new UsersInvalidEntries(driver);
    }
}
