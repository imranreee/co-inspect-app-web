package cases;

import com.aventstack.extentreports.Status;
import com.aventstack.extentreports.markuputils.ExtentColor;
import com.aventstack.extentreports.markuputils.MarkupHelper;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import settings.HelperClass;
import settings.RunCases;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

public class Assignments extends HelperClass {
    By assignmentsTab = By.xpath("//*[@id=\"sub-body\"]/aside/menu/ul/li[6]");
    By createAssignmentsBtn = By.xpath("//*[@id=\"sub-body\"]/section/div/div/div[1]/header/div/div/a/span");

    By choosePropertyTab = By.xpath("//*[@id=\"assignment-wizard-container\"]/section/header/div[1]");
    By choosePropertySelectProperty = By.xpath("//*[@id=\"assignment-wizard-container\"]/section/div[1]/ul/li[1]");
    By continueBtnChooseProperty = By.xpath("//*[@id=\"assignment-wizard-container\"]/section/div[2]/div[2]/div[3]/button");

    By chooseChecklistTab = By.xpath("//*[@id=\"assignment-wizard-container\"]/section/header/div[2]");
    By selectChecklist = By.xpath("//*[@id=\"assignment-wizard-container\"]/section/div[1]/ul/li");
    By continueBtnChooseChecklist = By.xpath("//*[@id=\"assignment-wizard-container\"]/section/div[2]/div[2]/div[3]/button");

    By chooseAgentTab = By.xpath("//*[@id=\"assignment-wizard-container\"]/section/header/div[3]");
    By selectAgent = By.xpath("//*[@id=\"assignment-wizard-container\"]/section/div[1]/ul/li[1]");
    By saveBtn = By.xpath("//*[@id=\"assignment-wizard-container\"]/section/div[2]/div[2]/div[3]/button");

    By inspectionsTab = By.xpath("//*[@id=\"sub-body\"]/section/div/div/div[2]/header/div[1]");

    By statusFilter = By.xpath("//*[@id=\"assignment-filters\"]/div[1]/select");
    By all = By.xpath("//*[text()[contains(.,'ALL')]]");
    By newF = By.xpath("//*[text()[contains(.,'NEW')]]");
    By open = By.xpath("//*[text()[contains(.,'OPEN')]]");
    By done = By.xpath("//*[text()[contains(.,'DONE')]]");

    By fromDateFilter = By.xpath("//*[@id=\"assignment-filters\"]/div[2]/input");
    By toDateFilter = By.xpath("//*[@id=\"assignment-filters\"]/div[3]/input");
    By propertyFilter = By.xpath("//*[@id=\"property-filter\"]/span/button");
    By propertySearchResult = By.xpath("//*[@id=\"-overlay\"]/div[2]/div[1]");

    By checkListFilter = By.xpath("//*[@id=\"assignment-filters\"]/div[5]/div/span/button");
    //By checkList = By.xpath("//*[@id=\"-overlay\"]/div[2]/div");
    By checkList = By.xpath("//*[text()[contains(.,'Sample Checklist')]]");

    By userFilter = By.xpath("//*[@id=\"assignment-filters\"]/div[6]/div/span/button");
    By user = By.xpath("//*[@id=\"-overlay\"]/div[2]/div[1]/div/label/span");

    By deleteBtn = By.xpath("//*[@id=\"assignment-list\"]/section/ul/li[1]/div[2]");
    By yesConfirmation = By.xpath("//*[@id=\"confirm-modal\"]/section/menu/button[2]");

    By calenderTab = By.xpath("//*[@id=\"sub-body\"]/section/div/div/div[2]/header/div[2]");
    By monthBtn = By.xpath("//*[@id=\"calendar-ui\"]/div[1]/div[1]/button[1]");
    By weekBtn = By.xpath("//*[@id=\"calendar-ui\"]/div[1]/div[1]/button[2]");
    By dayBtn = By.xpath("//*[@id=\"calendar-ui\"]/div[1]/div[1]/button[3]");
    By todayBtn = By.xpath("//*[@id=\"calendar-ui\"]/div[1]/div[2]/button");
    By previousBtn = By.xpath("//*[@id=\"calendar-ui\"]/div[1]/div[2]/div/button[1]");
    By nextBtn = By.xpath("//*[@id=\"calendar-ui\"]/div[1]/div[2]/div/button[2]");
    By propertyOnCalender = By.xpath("//*[@id=\"calendar-ui\"]/div[2]/div/table/tbody/tr/td/div/div/div[2]/div[2]/table/tbody/tr/td[5]/a/div/span[2]");
    By selectAllCheckMarkBox = By.xpath("//*[@id=\"assignment-list\"]/section/div[1]/input");
    By deleteAllSelected = By.xpath("//*[@id=\"assignment-list\"]/header/div/div/div");


    DateFormat dateFormatNew = new SimpleDateFormat("MM/dd/yyyy");
    Date dateNew = new Date();
    String currentDateNew = dateFormatNew.format(dateNew);

    Boolean flag = true;

    public Assignments(WebDriver driver) {
        super(driver);
    }

    public Assignments assignments() throws InterruptedException {
        RunCases.test = RunCases.extent.createTest("Assignments Test");

        waitForVisibilityOf(assignmentsTab);
        driver.findElement(assignmentsTab).click();
        waitForVisibilityOf(inspectionsTab);
        System.out.println("On the Assignments page");
        RunCases.test.log(Status.PASS, MarkupHelper.createLabel("On the Assignments page", ExtentColor.GREEN));

        /*Thread.sleep(3000);
        while (flag == true){
            if (driver.findElements(deleteBtn).size() > 0){
                driver.findElement(deleteBtn).click();
                waitForVisibilityOf(yesConfirmation);
                driver.findElement(yesConfirmation).click();
                Thread.sleep(2000);
            }else {
                flag = false;
                System.out.println("All existing assignment deleted");
                RunCases.test.log(Status.PASS, MarkupHelper.createLabel("All existing assignment deleted", ExtentColor.GREEN));

            }
        }*/


        Thread.sleep(3000);
        if (driver.findElements(deleteBtn).size() > 0){
            driver.findElement(selectAllCheckMarkBox).click();
            waitForVisibilityOf(deleteAllSelected);
            driver.findElement(deleteAllSelected).click();
            waitForVisibilityOf(yesConfirmation);
            driver.findElement(yesConfirmation).click();
            Thread.sleep(2000);
        }


        driver.findElement(createAssignmentsBtn).click();
        waitForVisibilityOf(choosePropertyTab);
        driver.findElement(choosePropertyTab).click();
        waitForVisibilityOf(choosePropertySelectProperty);
        driver.findElement(choosePropertySelectProperty).click();
        Thread.sleep(1000);
        driver.findElement(continueBtnChooseProperty).click();
        System.out.println("Property selected from choose property");
        RunCases.test.log(Status.PASS, MarkupHelper.createLabel("Property selected from choose property", ExtentColor.GREEN));

        waitForVisibilityOf(selectChecklist);
        driver.findElement(selectChecklist).click();
        Thread.sleep(1000);
        driver.findElement(continueBtnChooseChecklist).click();
        System.out.println("Checklist selected");
        RunCases.test.log(Status.PASS, MarkupHelper.createLabel("Checklist selected", ExtentColor.GREEN));

        waitForVisibilityOf(selectAgent);
        driver.findElement(selectAgent).click();
        Thread.sleep(1000);
        driver.findElement(saveBtn).click();
        waitForVisibilityOf(deleteBtn);

        System.out.println("Assignment created");
        RunCases.test.log(Status.PASS, MarkupHelper.createLabel("Assignment created", ExtentColor.GREEN));
        Thread.sleep(2000);

        driver.findElement(inspectionsTab).click();
        driver.findElement(statusFilter).click();
        Thread.sleep(1000);
        driver.findElement(newF).click();

        driver.findElement(statusFilter).click();
        Thread.sleep(1000);
        driver.findElement(open).click();

        driver.findElement(statusFilter).click();
        Thread.sleep(1000);
        driver.findElement(done).click();

        driver.findElement(statusFilter).click();
        Thread.sleep(1000);
        driver.findElement(all).click();
        System.out.println("Status filter selected as All");
        RunCases.test.log(Status.PASS, MarkupHelper.createLabel("", ExtentColor.GREEN));

        driver.findElement(fromDateFilter).click();
        Thread.sleep(1000);
        driver.findElement(fromDateFilter).click();
        Thread.sleep(1000);
        driver.findElement(fromDateFilter).sendKeys("04/01/2018");

        System.out.println("From dated selected");
        RunCases.test.log(Status.PASS, MarkupHelper.createLabel("From dated selected ", ExtentColor.GREEN));

        driver.findElement(toDateFilter).click();
        Thread.sleep(1000);
        driver.findElement(toDateFilter).click();
        Thread.sleep(1000);
        driver.findElement(toDateFilter).sendKeys(currentDateNew);

        System.out.println("To dated selected");
        RunCases.test.log(Status.PASS, MarkupHelper.createLabel("To dated selected ", ExtentColor.GREEN));

        driver.findElement(propertyFilter).click();
        waitForVisibilityOf(propertySearchResult);
        driver.findElement(propertySearchResult).click();
        System.out.println("Property search result selected");
        RunCases.test.log(Status.PASS, MarkupHelper.createLabel("Property search result selected", ExtentColor.GREEN));

        Thread.sleep(1000);
        driver.findElement(checkListFilter).click();
        waitForVisibilityOf(checkList);
        driver.findElement(checkList).click();
        System.out.println("Checklist selected");
        RunCases.test.log(Status.PASS, MarkupHelper.createLabel("Checklist selected", ExtentColor.GREEN));

        driver.findElement(userFilter).click();;
        driver.findElement(userFilter).click();
        System.out.println("User selected");
        RunCases.test.log(Status.PASS, MarkupHelper.createLabel("User selected", ExtentColor.GREEN));

        if (driver.findElements(deleteBtn).size() > 0){
            System.out.println("Search result found as per the filters");
        }else {
            System.out.println("No search result found as per the filters");
        }

        driver.navigate().refresh();
        waitForVisibilityOf(deleteBtn);
        driver.findElement(deleteBtn).click();
        waitForVisibilityOf(yesConfirmation);
        driver.findElement(yesConfirmation).click();
        Thread.sleep(2000);

        if (driver.findElements(yesConfirmation).size() > 0){
            System.out.println("Delete Assignment not working");
            RunCases.test.log(Status.FAIL, MarkupHelper.createLabel("Delete Assignment not working", ExtentColor.RED));
        }else {
            System.out.println("Delete assignment worked");
            RunCases.test.log(Status.PASS, MarkupHelper.createLabel("Delete assignment worked", ExtentColor.GREEN));
        }

        return new Assignments(driver);
    }
}
