package cases;

import com.aventstack.extentreports.Status;
import com.aventstack.extentreports.markuputils.ExtentColor;
import com.aventstack.extentreports.markuputils.MarkupHelper;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import settings.HelperClass;
import settings.RunCases;

public class CheckListSingleMultiSelect extends HelperClass {
    By createCategoryBtn =By.xpath("//*[@id=\"sub-body\"]/section/div/div/div/header/div[1]/button");
    By checkListTab = By.xpath("//*[@id=\"sub-body\"]/aside/menu/ul/li[3]");
    By checkLogo = By.xpath("//*[@id=\"checklist-editor\"]/div[2]/div[1]/div/section/form/div/div[1]/div/div/a/img");

    By enterCategory = By.xpath("//*[@id=\"sub-body\"]/section/div/div/div/ul/li[1]/div/div/div[1]/a/h3");
    By enterChecklist = By.xpath("/html/body/div[1]/section/div/div/div/ul/li/div/div/a");
    By addGroupBtn = By.xpath("/html/body/div[1]/section/div/div/div[2]/div[3]/div[2]/div[1]/div[1]");

    By questionGroupField = By.xpath("/html/body/div[1]/section/div/div/div[2]/div[3]/section/div/div/div[2]/header/section/div/div[2]/input");
    By questionGroupField2 = By.xpath("//*[@id=\"checklist-editor\"]/div[2]/div[3]/section/div/div/div[3]/header/section/div/div[2]/input");
    By plusBtn2 = By.xpath("/html/body/div[1]/section/div/div/div[2]/div[3]/section/div/div/div[3]/header/aside/div[2]");

    By detailsTab = By.xpath("/html/body/div[1]/section/div/div/div[2]/div[3]/section/div/div/div[3]/ul/li/section[2]/div/header/div[1]");
    By answerTab = By.xpath("/html/body/div[1]/section/div/div/div[2]/div[3]/section/div/div/div[3]/ul/li/section[2]/div/header/div[4]");
    By addAnswerOptions = By.xpath("/html/body/div[1]/section/div/div/div[2]/div[3]/section/div/div/div[3]/ul/li/section[2]/div/div/div/div/div[3]/button");

    By newAnswerField = By.xpath("/html/body/div[1]/section/div/div/div[2]/div[3]/section/div/div/div[3]/ul/li/section[2]/div/div/div/div/div[4]/div/div/table/tbody/tr/th[1]/input");
    By deleteQuestionGroup = By.xpath("//*[@id=\"checklist-editor\"]/div[2]/div[3]/section/div/div/div[2]/header/aside/div[1]");
    By yesConfirmationDialog = By.xpath("//*[@id=\"confirm-modal\"]/section/menu/button[2]");
    By multipleChoiceOption = By.xpath("//*[@id=\"criteria\"]/div/div[4]/div[1]/div/div[2]/label");
    By dropDownOption = By.xpath("//*[@id=\"criteria\"]/div/div[4]/div[1]/div/div[3]/label");

    By multipleChoiceAnsText = By.xpath("//*[contains(text(), 'Multiple Choice Answers')]");
    By dropDownText = By.xpath("//*[contains(text(), 'Dropdown Answers')]");
    By scoringTab = By.xpath("/html/body/div[1]/section/div/div/div[2]/div[3]/section/div/div/div[3]/ul/li/section[2]/div/header/div[5]");
    By singleAnswerOption = By.xpath("//*[@id=\"criteria\"]/div/div[4]/div[1]/div/div[7]/label");
    By multiAnswerOption = By.xpath("//*[@id=\"criteria\"]/div/div[4]/div[2]/div/div[8]/label");
    By saveConfirmationDialog = By.xpath("/html/body/div[1]/section/menu/button[2]");

    public CheckListSingleMultiSelect(WebDriver driver) {
        super(driver);
    }

    public CheckListSingleMultiSelect checkListSingleMultiSelect() throws Exception {
        RunCases.test = RunCases.extent.createTest("Checklist Single or Multi answer Test");
        waitForVisibilityOf(checkListTab);

        driver.findElement(checkListTab).click();
        Thread.sleep(3000);
        if (driver.findElements(saveConfirmationDialog).size() > 0){
            driver.findElement(saveConfirmationDialog).click();
            driver.findElement(checkListTab).click();
        }

        waitForVisibilityOf(createCategoryBtn);
        System.out.println("On the checklist page");
        RunCases.test.log(Status.PASS, MarkupHelper.createLabel("On the checklist page", ExtentColor.GREEN));

        waitForVisibilityOf(enterCategory);
        driver.findElement(enterCategory).click();
        waitForVisibilityOf(enterChecklist);
        driver.findElement(enterChecklist).click();
        waitForVisibilityOf(checkLogo);
        Thread.sleep(2000);

        //for scrolling the page, can move up or down by changing the element as pre your needs
        WebElement element = driver.findElement(addGroupBtn);
        ((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView();", element);

        boolean check = true;
        while( check == true){
            Thread.sleep(3000);
            if (driver.findElements(deleteQuestionGroup).size() > 0){
                driver.findElement(deleteQuestionGroup).click();
                waitForVisibilityOf(yesConfirmationDialog);
                driver.findElement(yesConfirmationDialog).click();
                System.out.println("Existing question group deleted");
                RunCases.test.log(Status.PASS, MarkupHelper.createLabel("Existing question group deleted", ExtentColor.GREEN));

            }else {
                check = false;
            }
        }

        driver.findElement(addGroupBtn).click();
        waitForVisibilityOf(questionGroupField);
        System.out.println("App on the add group name page");
        RunCases.test.log(Status.PASS, MarkupHelper.createLabel("App on the add group name page", ExtentColor.GREEN));

        driver.findElement(questionGroupField).sendKeys("FirstGroup");
        Thread.sleep(2000);
        driver.navigate().refresh();
        waitForVisibilityOf(checkLogo);
        Thread.sleep(3000);

        WebElement element2 = driver.findElement(addGroupBtn);
        ((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView();", element2);

        Thread.sleep(1000);
        driver.findElement(addGroupBtn).click();
        waitForVisibilityOf(questionGroupField2);
        driver.findElement(questionGroupField2).sendKeys("secondGroup");
        Thread.sleep(2000);
        driver.navigate().refresh();
        waitForVisibilityOf(checkLogo);

        System.out.println("App on the add group name page");
        RunCases.test.log(Status.PASS, MarkupHelper.createLabel("App on the add group name page", ExtentColor.GREEN));

        WebElement element3 = driver.findElement(addGroupBtn);
        ((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView();", element3);

        driver.findElement(plusBtn2).click();
        waitForVisibilityOf(detailsTab);
        driver.findElement(detailsTab).click();
        Thread.sleep(1000);

        driver.findElement(answerTab).click();
        waitForVisibilityOf(addAnswerOptions);
        Thread.sleep(1000);
        driver.findElement(addAnswerOptions).click();
        waitForVisibilityOf(newAnswerField);
        Thread.sleep(2000);
        driver.findElement(addAnswerOptions).click();
        waitForVisibilityOf(newAnswerField);
        Thread.sleep(1000);

        driver.findElement(dropDownOption).click();
        Thread.sleep(1000);
        driver.findElement(singleAnswerOption).click();
        Thread.sleep(1000);
        driver.findElement(scoringTab).click();
        waitForVisibilityOf(dropDownText);
        System.out.println("Dropdown option with single answer selected and worked");
        RunCases.test.log(Status.PASS, MarkupHelper.createLabel("Dropdown option with single answer selected and worked", ExtentColor.GREEN));
        Thread.sleep(2000);

        driver.findElement(answerTab).click();
        Thread.sleep(1000);

        driver.findElement(dropDownOption).click();
        Thread.sleep(1000);
        driver.findElement(multiAnswerOption).click();
        Thread.sleep(1000);
        driver.findElement(scoringTab).click();
        waitForVisibilityOf(dropDownText);
        System.out.println("Dropdown option with multi answer selected and worked");
        RunCases.test.log(Status.PASS, MarkupHelper.createLabel("Dropdown option with multi answer selected and worked", ExtentColor.GREEN));
        Thread.sleep(2000);

        driver.findElement(answerTab).click();
        waitForVisibilityOf(multipleChoiceOption);
        driver.findElement(multipleChoiceOption).click();
        Thread.sleep(1000);
        driver.findElement(singleAnswerOption).click();
        Thread.sleep(1000);
        driver.findElement(scoringTab).click();
        waitForVisibilityOf(multipleChoiceAnsText);
        System.out.println("Multiple option with single answer selected and worked");
        RunCases.test.log(Status.PASS, MarkupHelper.createLabel("Multiple option with single answer selected and worked", ExtentColor.GREEN));
        Thread.sleep(2000);

        driver.findElement(answerTab).click();
        waitForVisibilityOf(multipleChoiceOption);
        driver.findElement(multipleChoiceOption).click();
        Thread.sleep(1000);
        driver.findElement(multiAnswerOption).click();
        Thread.sleep(1000);
        driver.findElement(scoringTab).click();
        waitForVisibilityOf(multipleChoiceAnsText);
        System.out.println("Multiple option with multiple answer selected and worked");
        RunCases.test.log(Status.PASS, MarkupHelper.createLabel("Multiple option with multiple answer selected and worked", ExtentColor.GREEN));
        Thread.sleep(2000);

        return new CheckListSingleMultiSelect(driver);
    }
}
