package cases;

import com.aventstack.extentreports.Status;
import com.aventstack.extentreports.markuputils.ExtentColor;
import com.aventstack.extentreports.markuputils.MarkupHelper;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import settings.HelperClass;
import settings.RunCases;

public class CheckListReportConfigure extends HelperClass {
    By createCategoryBtn =By.xpath("//*[@id=\"sub-body\"]/section/div/div/div/header/div[1]/button");
    By checkListTab = By.xpath("//*[@id=\"sub-body\"]/aside/menu/ul/li[3]");
    By checkLogo = By.xpath("//*[@id=\"checklist-editor\"]/div[2]/div[1]/div/section/form/div/div[1]/div/div/a/img");

    By enterCategory = By.xpath("//*[@id=\"sub-body\"]/section/div/div/div/ul/li[1]/div/div/div[1]/a/h3");
    By enterChecklist = By.xpath("/html/body/div[1]/section/div/div/div/ul/li/div/div/a");
    By addGroupBtn = By.xpath("/html/body/div[1]/section/div/div/div[2]/div[3]/div[2]/div[1]/div[1]");
    By deleteQuestionGroup = By.xpath("//*[@id=\"checklist-editor\"]/div[2]/div[3]/section/div/div/div[2]/header/aside/div[1]");
    By yesConfirmationDialog = By.xpath("//*[@id=\"confirm-modal\"]/section/menu/button[2]");

    By showAdvanceOptionBtn = By.xpath("//*[@id=\"checklist-editor\"]/div[2]/section[2]/div/div/button/div");
    By reportPhotoBtn = By.xpath("//*[@id=\"checklist-editor\"]/div[2]/section[3]/div[1]/div/div/header");

    By highToLow = By.xpath("//*[contains(text(), 'Score from highest to lowest')]");

    By reportConfigureBtn = By.xpath("//*[@id=\"checklist-editor\"]/div[2]/section[3]/div[5]/div/div/header");
    By hideQuestionBtnOn = By.xpath("//*[@id=\"checklist-editor\"]/div[2]/section[3]/div[5]/div/div/section/section/div[1]/div/div[2]/div/label");
    By hidePhotoBtnOn = By.xpath("//*[@id=\"checklist-editor\"]/div[2]/section[3]/div[5]/div/div/section/section/div[1]/div/div[4]/div/label");
    By hidePassFailBtnOn = By.xpath("//*[@id=\"checklist-editor\"]/div[2]/section[3]/div[5]/div/div/section/section/div[1]/div/div[6]/div/label");
    By hideCommentBtnOn = By.xpath("//*[@id=\"checklist-editor\"]/div[2]/section[3]/div[5]/div/div/section/section/div[2]/div/div[2]/div/label");
    By scoringField = By.xpath("//*[@id=\"checklist-editor\"]/div[2]/section[3]/div[5]/div/div/section/section/div[2]/div/div[3]/select");

    public CheckListReportConfigure(WebDriver driver) {
        super(driver);
    }

    public CheckListReportConfigure checkListReportConfigure() throws Exception {
        RunCases.test = RunCases.extent.createTest("Report configuration Test");
        waitForVisibilityOf(checkListTab);

        driver.findElement(checkListTab).click();
        waitForVisibilityOf(createCategoryBtn);
        System.out.println("On the checklist page");
        RunCases.test.log(Status.PASS, MarkupHelper.createLabel("On the checklist page", ExtentColor.GREEN));

        waitForVisibilityOf(enterCategory);
        driver.findElement(enterCategory).click();
        waitForVisibilityOf(enterChecklist);
        Thread.sleep(1000);
        driver.findElement(enterChecklist).click();
        waitForVisibilityOf(checkLogo);
        Thread.sleep(2000);

        //for scrolling the page, can move up or down by changing the element as per your needs
        WebElement element = driver.findElement(addGroupBtn);
        ((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView();", element);

        boolean check = true;
        while( check == true){
            Thread.sleep(3000);
            if (driver.findElements(deleteQuestionGroup).size() > 0){
                driver.findElement(deleteQuestionGroup).click();
                waitForVisibilityOf(yesConfirmationDialog);
                driver.findElement(yesConfirmationDialog).click();
                System.out.println("Existing question group deleted");
                RunCases.test.log(Status.PASS, MarkupHelper.createLabel("Existing question group deleted", ExtentColor.GREEN));

            }else {
                check = false;
            }
        }

        driver.findElement(showAdvanceOptionBtn).click();
        WebElement element5 = driver.findElement(reportPhotoBtn);
        ((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView();", element5);
        Thread.sleep(2000);

        waitForVisibilityOf(reportConfigureBtn);
        driver.findElement(reportConfigureBtn).click();

        waitForClickabilityOf(hideQuestionBtnOn);
        driver.findElement(hideQuestionBtnOn).click();
        Thread.sleep(1000);

        waitForClickabilityOf(hidePhotoBtnOn);
        driver.findElement(hidePhotoBtnOn).click();
        Thread.sleep(1000);

        waitForClickabilityOf(hidePassFailBtnOn);
        driver.findElement(hidePassFailBtnOn).click();
        Thread.sleep(1000);

        waitForClickabilityOf(hideCommentBtnOn);
        driver.findElement(hideCommentBtnOn).click();
        Thread.sleep(1000);

        driver.findElement(scoringField).click();
        waitForClickabilityOf(highToLow);
        driver.findElement(highToLow).click();

        return new CheckListReportConfigure(driver);
    }
}
