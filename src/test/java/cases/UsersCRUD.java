package cases;

import com.aventstack.extentreports.Status;
import com.aventstack.extentreports.markuputils.ExtentColor;
import com.aventstack.extentreports.markuputils.MarkupHelper;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import settings.HelperClass;
import settings.RunCases;

public class UsersCRUD extends HelperClass {
    By usersTab = By.xpath("/html/body/div[1]/aside/menu/ul/li[4]");
    By createNewUserBtn = By.xpath("//*[@id=\"sub-body\"]/section/div/div/div[1]/header[2]/div/div[1]/a");

    By changeImage = By.xpath("/html/body/div[1]/section/div/div/div[2]/form/div[1]/div[1]/a/img");
    By selectImage = By.xpath("/html/body/div[1]/section/div/div/div[1]/section/div/img[8]");
    By selectImageBtn = By.xpath("/html/body/div[1]/section/div/div/div[1]/section/menu/button[2]");
    By firstNameFiled = By.xpath("/html/body/div[1]/section/div/div/div[2]/form/div[1]/div[2]/div[1]/input");
    By lastNameFiled = By.xpath("/html/body/div[1]/section/div/div/div[2]/form/div[1]/div[2]/div[2]/input");
    By accountRoleAdmin = By.xpath("/html/body/div[1]/section/div/div/div[2]/form/div[2]/div/div/label[3]");


    By usernameField = By.xpath("/html/body/div[1]/section/div/div/div[2]/form/div[3]/div[1]/div[1]/input");
    By usernameOkBtn = By.xpath("/html/body/div[1]/section/div/div/div[2]/form/div[3]/div[1]/div[1]/div[1]");
    By usernameEditBtn = By.xpath("/html/body/div[1]/section/div/div/div[2]/form/div[3]/div[1]/div[1]/div[2]");

    By contactEmailField = By.xpath("/html/body/div[1]/section/div/div/div[2]/form/div[3]/div[2]/div[1]/input");
    By emailOkBtn = By.xpath("/html/body/div[1]/section/div/div/div[2]/form/div[3]/div[2]/div[1]/div[1]");
    By emailEditBtn = By.xpath("/html/body/div[1]/section/div/div/div[2]/form/div[3]/div[2]/div[1]/div[2]");

    By loginPasswordField = By.xpath("/html/body/div[1]/section/div/div/div[2]/form/div[3]/div[3]/div[1]/input");
    By loginPasswordOkBtn = By.xpath("/html/body/div[1]/section/div/div/div[2]/form/div[3]/div[3]/div[1]/div[1]");
    By loginPasswordEditBtn = By.xpath("/html/body/div[1]/section/div/div/div[2]/form/div[3]/div[3]/div[1]/div[2]");

    By loginPasswordVerifyField = By.xpath("/html/body/div[1]/section/div/div/div[2]/form/div[3]/div[4]/div[1]/input");
    By loginPasswordVerifyOkBtn = By.xpath("/html/body/div[1]/section/div/div/div[2]/form/div[3]/div[4]/div[1]/div[1]");
    By loginPasswordVerifyEditBtn = By.xpath("/html/body/div[1]/section/div/div/div[2]/form/div[3]/div[4]/div[1]/div[2]");

    By saveBtn = By.xpath("/html/body/div[1]/section/div/div/div[2]/form/div[4]/div[1]");
    By deleteBtn = By.xpath("/html/body/div[1]/section/div/div/div[2]/form/div[4]/div[2]");
    By deleteConfirmationBtn = By.xpath("/html/body/div[1]/section/menu/button[2]");
    By checkCreatedUser = By.xpath("//*[text()[contains(.,'ATestUser')]]");
    By checkModdedUser = By.xpath("//*[text()[contains(.,'AAATestUser')]]");

    By logoutTab = By.xpath("//*[@id=\"sub-body\"]/aside/menu/ul/li[10]");
    By logoutTabForNewUser = By.xpath("//*[@id=\"sub-body\"]/aside/menu/ul/li[8]");

    By loginUsernameField = By.xpath("/html/body/form/div[2]/div/input[1]");
    By passwordField = By.xpath("/html/body/form/div[2]/div/input[2]");
    By logInBtn = By.xpath("/html/body/form/div[2]/div/input[3]");
    By adminRadioBtn = By.xpath("/html/body/form/div[2]/div/div[1]/div[2]/label");
    By deleteUserBtn = By.xpath("/html/body/div[1]/section/div/div/div[2]/section/ul[2]/li[1]/div[5]/div");

    public UsersCRUD(WebDriver driver) {
        super(driver);
    }

    public UsersCRUD usersCRUD() throws Exception {
        RunCases.test = RunCases.extent.createTest("UsersCRUD Test");

        waitForVisibilityOf(usersTab);
        driver.findElement(usersTab).click();
        Thread.sleep(2000);
        if (driver.findElements(checkCreatedUser).size() > 0){
            driver.findElement(checkCreatedUser).click();
            Thread.sleep(1000);
            driver.findElement(deleteBtn).click();
            waitForVisibilityOf(deleteConfirmationBtn);
            driver.findElement(deleteConfirmationBtn).click();
            System.out.println("Existing user deleted");
        }

        waitForVisibilityOf(createNewUserBtn);
        System.out.println("App on the users page");
        RunCases.test.log(Status.PASS, MarkupHelper.createLabel("App on the users page", ExtentColor.GREEN));

        driver.findElement(createNewUserBtn).click();
        driver.findElement(changeImage).click();
        Thread.sleep(2000);
        driver.findElement(selectImage).click();
        Thread.sleep(1000);
        driver.findElement(selectImageBtn).click();
        driver.findElement(firstNameFiled);
        System.out.println("Avatar selected");
        RunCases.test.log(Status.PASS, MarkupHelper.createLabel("Avatar selected, as profile photo", ExtentColor.GREEN));

        driver.findElement(firstNameFiled).sendKeys("ATestUser");
        Thread.sleep(1000);
        driver.findElement(lastNameFiled).sendKeys("FromAutomation");
        Thread.sleep(1000);
        driver.findElement(accountRoleAdmin).click();

        //For generating random numeric value
        String randomValue = ""+((int)(Math.random()*9000)+1000);

        driver.findElement(usernameField).sendKeys("testusername"+randomValue);
        Thread.sleep(1000);
        driver.findElement(usernameOkBtn).click();
        driver.findElement(usernameEditBtn);
        System.out.println("Username successfully entered");
        RunCases.test.log(Status.PASS, MarkupHelper.createLabel("Username successfully entered", ExtentColor.GREEN));

        driver.findElement(contactEmailField).sendKeys("test"+randomValue+"@automation.com");
        Thread.sleep(1000);
        driver.findElement(emailOkBtn).click();
        driver.findElement(emailEditBtn);
        System.out.println("Contact email successfully entered");
        RunCases.test.log(Status.PASS, MarkupHelper.createLabel("Contact email successfully entered", ExtentColor.GREEN));

        driver.findElement(loginPasswordField).sendKeys("testpassword");
        Thread.sleep(1000);
        driver.findElement(loginPasswordOkBtn).click();
        driver.findElement(loginPasswordEditBtn);
        Thread.sleep(1000);

        driver.findElement(loginPasswordVerifyEditBtn).click();
        driver.findElement(loginPasswordVerifyField).sendKeys("testpassword");
        Thread.sleep(1000);
        driver.findElement(loginPasswordVerifyOkBtn).click();
        driver.findElement(loginPasswordVerifyEditBtn);

        System.out.println("Password entered successfully");
        RunCases.test.log(Status.PASS, MarkupHelper.createLabel("Password entered successfully", ExtentColor.GREEN));

        Thread.sleep(1000);
        driver.findElement(saveBtn).click();
        Thread.sleep(2000);
        driver.findElement(usersTab).click();
        waitForVisibilityOf(checkCreatedUser);
        System.out.println("User successfully created");
        RunCases.test.log(Status.PASS, MarkupHelper.createLabel("User successfully created", ExtentColor.GREEN));
        Thread.sleep(2000);

        driver.findElement(logoutTab).click();
        waitForVisibilityOf(adminRadioBtn);
        System.out.println("Successfully logged out");
        RunCases.test.log(Status.PASS, MarkupHelper.createLabel("Successfully logged out", ExtentColor.GREEN));

        if (driver.findElements(adminRadioBtn).size() > 0){
            driver.findElement(adminRadioBtn).click();
            System.out.println("Login with admin radio button");
            RunCases.test.log(Status.INFO, MarkupHelper.createLabel("Login with admin radio button", ExtentColor.CYAN));
        }else {
            System.out.println("Login without admin radio button");
        }

        waitForVisibilityOf(loginUsernameField);
        driver.findElement(loginUsernameField).sendKeys("test"+randomValue+"@automation.com");
        driver.findElement(passwordField).sendKeys("testpassword");
        driver.findElement(logInBtn).click();
        waitForVisibilityOf(usersTab);
        System.out.println("Logged in successfully with new credentials");
        RunCases.test.log(Status.PASS, MarkupHelper.createLabel("Logged in successfully with new credentials", ExtentColor.GREEN));

        waitForVisibilityOf(logoutTabForNewUser);
        driver.findElement(logoutTabForNewUser).click();
        waitForVisibilityOf(loginUsernameField);
        System.out.println("Logged out successfully from new user");
        RunCases.test.log(Status.PASS, MarkupHelper.createLabel("Logged out successfully from new user", ExtentColor.GREEN));

        driver.findElement(loginUsernameField).sendKeys("admin@selenium3.coinspectapp.com");
        driver.findElement(passwordField).sendKeys("newtestpass");
        if (driver.findElements(adminRadioBtn).size() > 0){
            driver.findElement(adminRadioBtn).click();
            System.out.println("Login with admin radio button");
            RunCases.test.log(Status.INFO, MarkupHelper.createLabel("Login with admin radio button", ExtentColor.CYAN));
        }else {
            System.out.println("Login without admin radio button");
        }
        driver.findElement(logInBtn).click();
        waitForVisibilityOf(usersTab);

        driver.findElement(usersTab).click();
        Thread.sleep(1000);

        waitForVisibilityOf(checkCreatedUser);
        driver.findElement(checkCreatedUser).click();
        Thread.sleep(1000);

        driver.findElement(firstNameFiled).clear();
        driver.findElement(firstNameFiled).sendKeys("AAATestUser");
        driver.findElement(saveBtn).click();
        Thread.sleep(2000);
        driver.findElement(usersTab).click();
        waitForVisibilityOf(checkModdedUser);
        System.out.println("User Modified successfully");
        RunCases.test.log(Status.PASS, MarkupHelper.createLabel("User Modified successfully", ExtentColor.GREEN));
        Thread.sleep(2000);

        driver.findElement(deleteUserBtn).click();
        waitForVisibilityOf(deleteConfirmationBtn);
        driver.findElement(deleteConfirmationBtn).click();

        waitForVisibilityOf(createNewUserBtn);
        driver.navigate().refresh();
        waitForVisibilityOf(createNewUserBtn);

        if (driver.findElements(checkModdedUser).size() > 0){
            System.out.println("User delete not working");
            RunCases.test.log(Status.FAIL, MarkupHelper.createLabel("User delete not working", ExtentColor.RED));
            driver.findElements(makeFail);
        }else {
            System.out.println("User deleted successfully");
            RunCases.test.log(Status.PASS, MarkupHelper.createLabel("User deleted successfully", ExtentColor.GREEN));
        }
        return new UsersCRUD(driver);
    }
}
