package cases;

import com.aventstack.extentreports.Status;
import com.aventstack.extentreports.markuputils.ExtentColor;
import com.aventstack.extentreports.markuputils.MarkupHelper;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import settings.HelperClass;
import settings.RunCases;

import java.awt.*;
import java.awt.event.KeyEvent;

public class CheckListExportImport extends HelperClass {
    By createCategoryBtn =By.xpath("//*[@id=\"sub-body\"]/section/div/div/div/header/div[1]/button");
    By checkListTab = By.xpath("//*[@id=\"sub-body\"]/aside/menu/ul/li[3]");
    By checkLogo = By.xpath("//*[@id=\"checklist-editor\"]/div[2]/div[1]/div/section/form/div/div[1]/div/div/a/img");

    By importedCheckListName = By.xpath("//*[contains(text(), 'TestName')]");
    By copyChecklist = By.xpath("//*[contains(text(), 'Copy')]");
    By exportZipBtn = By.xpath("/html/body/div[1]/section/div/div/div[2]/div[3]/div[2]/div[1]/a/div");

    By enterCategory = By.xpath("//*[@id=\"sub-body\"]/section/div/div/div/ul/li[1]/div/div/div[1]/a/h3");
    By enterChecklist = By.xpath("/html/body/div[1]/section/div/div/div/ul/li/div/div/a");
    By checklistNameField = By.xpath("//*[@id=\"chk_name\"]");
    By cancelBtnDialog = By.xpath("/html/body/div[1]/section/menu/button[1]");
    By saveBtnDialog = By.xpath("//*[@id=\"confirm-modal\"]/section/menu/button[2]");
    By importZipBtn = By.xpath("/html/body/div[1]/section/div/div/div[2]/div[3]/div[2]/div[1]/div[2]/div[1]/input");

    By deleteCheckList = By.xpath("/html/body/div[1]/section/div/div/div[2]/div[3]/section/div/div/div[2]/header/aside/div[1]");
    By yesConfirmationDialog = By.xpath("/html/body/div[1]/section/menu/button[2]");
    By duplicateBtn = By.xpath("//*[@id=\"checklist-editor\"]/div[2]/section[1]/div/div/div/div/button[1]");
    By backToPrevious = By.xpath("//*[@id=\"mewe-crumbs\"]/li[3]/a/div");
    By deleteCheckBtn = By.xpath("//*[@id=\"checklist-editor\"]/div[2]/section[1]/div/div/div/div/button[2]");

    public CheckListExportImport(WebDriver driver) {
        super(driver);
    }

    public CheckListExportImport checkListExportImport() throws Exception {
        RunCases.test = RunCases.extent.createTest("Checklist Export and Import Test");
        waitForVisibilityOf(checkListTab);

        driver.findElement(checkListTab).click();

        waitForVisibilityOf(createCategoryBtn);
        System.out.println("On the checklist page");
        RunCases.test.log(Status.PASS, MarkupHelper.createLabel("On the checklist page", ExtentColor.GREEN));

        waitForVisibilityOf(enterCategory);
        driver.findElement(enterCategory).click();
        waitForVisibilityOf(enterChecklist);
        driver.findElement(enterChecklist).click();
        waitForVisibilityOf(checkLogo);
        Thread.sleep(2000);

        //for scrolling the page, can move up or down by changing the element as pre your needs
        WebElement element = driver.findElement(exportZipBtn);
        ((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView();", element);

        driver.findElement(exportZipBtn).click();

        Robot object = new Robot();
        object.keyPress(KeyEvent.VK_ENTER);
        object.keyRelease(KeyEvent.VK_ENTER);

        System.out.println("Zip exported");
        RunCases.test.log(Status.PASS, MarkupHelper.createLabel("Zip exported", ExtentColor.GREEN));

        driver.findElement(checklistNameField).clear();
        Thread.sleep(1000);
        driver.findElement(checklistNameField).sendKeys("NewName");

        ((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView();", element);

        Thread.sleep(2000);
        if (driver.findElements(deleteCheckList).size() > 0){
            driver.findElement(deleteCheckList).click();
            waitForVisibilityOf(yesConfirmationDialog);
            driver.findElement(yesConfirmationDialog).click();
            Thread.sleep(2000);
        }
        Thread.sleep(2000);

        driver.findElement(importZipBtn).sendKeys(zipFilePath);

        System.out.println("Zip file imported");
        RunCases.test.log(Status.PASS, MarkupHelper.createLabel("Zip file imported", ExtentColor.GREEN));

        driver.findElement(duplicateBtn).click();
        Thread.sleep(3000);
        waitForVisibilityOf(saveBtnDialog);
        driver.findElement(saveBtnDialog).click();
        Thread.sleep(3000);

        driver.findElement(backToPrevious).click();
        Thread.sleep(2000);
        driver.findElement(copyChecklist);

        System.out.println("Checklist copied successfully");
        RunCases.test.log(Status.PASS, MarkupHelper.createLabel("Checklist copied successfully", ExtentColor.GREEN));

        driver.findElement(copyChecklist).click();
        Thread.sleep(3000);

        WebElement element2 = driver.findElement(deleteCheckBtn);
        ((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView();", element2);
        driver.findElement(deleteCheckBtn).click();
        waitForVisibilityOf(yesConfirmationDialog);
        driver.findElement(yesConfirmationDialog).click();
        System.out.println("Copy deleted");
        RunCases.test.log(Status.PASS, MarkupHelper.createLabel("Copy deleted", ExtentColor.GREEN));

        return new CheckListExportImport(driver);
    }
}
