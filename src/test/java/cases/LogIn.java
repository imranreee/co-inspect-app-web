package cases;

import com.aventstack.extentreports.Status;
import com.aventstack.extentreports.markuputils.ExtentColor;
import com.aventstack.extentreports.markuputils.MarkupHelper;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import settings.HelperClass;
import settings.RunCases;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;

public class LogIn extends HelperClass{
    By appLogo = By.xpath("/html/body/form/figure/img");
    By usernameField = By.xpath("/html/body/form/div[2]/div/input[1]");
    By passwordField = By.xpath("/html/body/form/div[2]/div/input[2]");
    By logInBtn = By.xpath("/html/body/form/div[2]/div/input[3]");
    By sevenDaysTab = By.xpath("//*[@id=\"dashboard\"]/div[1]/div[1]/div[1]/div/div[1]");

    By adminRadioBtn = By.xpath("/html/body/form/div[2]/div/div[1]/div[2]/label");;
    By appVersion = By.xpath("/html/body/form/div[2]/div/div[2]/label");

    public LogIn(WebDriver driver) {
        super(driver);
    }

    public LogIn logIn() throws Exception {
        RunCases.test = RunCases.extent.createTest("Login test");

        waitForVisibilityOf(appLogo);
        RunCases.test.log(Status.INFO, MarkupHelper.createLabel("Admin login page loaded", ExtentColor.CYAN));
        System.out.println("Admin login page loaded");

        String appV = driver.findElement(appVersion).getText();
        System.out.println("CoInspectApp(Admin) "+appV);
        RunCases.test.log(Status.INFO, MarkupHelper.createLabel("CoInspectApp(Admin) "+appV, ExtentColor.CYAN));

        if (driver.findElements(adminRadioBtn).size() > 0){
            driver.findElement(adminRadioBtn).click();
            System.out.println("Login with admin radio button");
            RunCases.test.log(Status.INFO, MarkupHelper.createLabel("Login with admin radio button", ExtentColor.CYAN));
        }else {
            System.out.println("Login without admin radio button");
        }

        driver.findElement(usernameField).sendKeys("admin@selenium3.coinspectapp.com");
        driver.findElement(passwordField).sendKeys("newtestpass");
        driver.findElement(logInBtn).click();
        driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);

        if (driver.findElements(logInBtn).size() > 0){
            if (driver.findElements(adminRadioBtn).size() > 0){
                driver.findElement(adminRadioBtn).click();
                System.out.println("Login with admin radio button");
                RunCases.test.log(Status.INFO, MarkupHelper.createLabel("Login with admin radio button", ExtentColor.CYAN));
            }else {
                System.out.println("Login without admin radio button");
            }

            driver.findElement(usernameField).sendKeys("admin@selenium3.coinspectapp.com");
            driver.findElement(passwordField).sendKeys("newtestpass");
            driver.findElement(logInBtn).click();
            System.out.println("First login failed, trying to login second time");
            RunCases.test.log(Status.INFO, MarkupHelper.createLabel("First login failed, trying to login second time", ExtentColor.CYAN));
        }

        waitForVisibilityOf(sevenDaysTab);
        System.out.println("Admin logged-in successfully");
        RunCases.test.log(Status.PASS, MarkupHelper.createLabel("Admin logged-in successfully", ExtentColor.GREEN));

        return new LogIn(driver);
    }

}
