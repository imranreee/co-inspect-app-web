package cases;

import com.aventstack.extentreports.Status;
import com.aventstack.extentreports.markuputils.ExtentColor;
import com.aventstack.extentreports.markuputils.MarkupHelper;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import settings.HelperClass;
import settings.RunCases;

public class LogoutAndInvalidLogin extends HelperClass {
    By logoutTab = By.xpath("//*[@id=\"sub-body\"]/aside/menu/ul/li[10]");
    By coInspectLogo = By.xpath("/html/body/form/figure/img");

    By usernameField = By.xpath("/html/body/form/div[2]/div/input[1]");
    By passwordField = By.xpath("/html/body/form/div[2]/div/input[2]");
    By logInBtn = By.xpath("/html/body/form/div[2]/div/input[3]");
    By sevenDaysTab = By.xpath("//*[@id=\"dashboard\"]/div[1]/div[1]/div[1]/div/div[1]");

    By adminRadioBtn = By.xpath("/html/body/form/div[2]/div/div[1]/div[2]/label");;

    public LogoutAndInvalidLogin(WebDriver driver) {
        super(driver);
    }

    public LogoutAndInvalidLogin logoutAndInvalidLogin(){
        RunCases.test = RunCases.extent.createTest("Logout and invalid login Test");

        waitForVisibilityOf(logoutTab);
        driver.findElement(logoutTab).click();
        waitForVisibilityOf(coInspectLogo);
        System.out.println("Successfully logged out");
        RunCases.test.log(Status.PASS, MarkupHelper.createLabel("Successfully logged out", ExtentColor.GREEN));


        if (driver.findElements(adminRadioBtn).size() > 0){
            driver.findElement(adminRadioBtn).click();
            System.out.println("Login with admin radio button");
            RunCases.test.log(Status.INFO, MarkupHelper.createLabel("Login with admin radio button", ExtentColor.CYAN));
        }else {
            System.out.println("Login without admin radio button");
        }

        driver.findElement(usernameField).sendKeys("wrong@username.com");
        driver.findElement(passwordField).sendKeys("wrongpassword");
        driver.findElement(logInBtn).click();
        waitForVisibilityOf(coInspectLogo);
        System.out.println("Invalid id and password test passed");
        RunCases.test.log(Status.PASS, MarkupHelper.createLabel("Invalid id and password test passed", ExtentColor.GREEN));

        return new LogoutAndInvalidLogin(driver);
    }
}
