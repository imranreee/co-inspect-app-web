package cases;

import com.aventstack.extentreports.Status;
import com.aventstack.extentreports.markuputils.ExtentColor;
import com.aventstack.extentreports.markuputils.MarkupHelper;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import settings.HelperClass;
import settings.RunCases;

public class MyAccount extends HelperClass {
    By myAccountTab = By.xpath("//*[@id=\"sub-body\"]/aside/menu/ul/li[8]");
    By loginEmailTab = By.xpath("//*[@id=\"sub-body\"]/section/div/div/div/header/div[1]");
    By newEmailField = By.xpath("//*[@id=\"sub-body\"]/section/div/div/div/section/div[1]/div/div/div/input[1]");
    By changeEmailBtn = By.xpath("//*[@id=\"sub-body\"]/section/div/div/div/section/div[1]/div/div/div/div");
    By reTypeEmailFiled = By.xpath("//*[@id=\"sub-body\"]/section/div/div/div/section/div[1]/div/div/div/input[2]");
    By currentPwEmail = By.xpath("//*[@id=\"sub-body\"]/section/div/div/div/section/div[1]/div/div/div/input[3]");
    By pwValidation = By.xpath("//*[@id=\"sub-body\"]/section/div/div/div/section/div[3]");

    By usernameTab = By.xpath("//*[@id=\"sub-body\"]/section/div/div/div/header/div[2]");
    By newUsernameFile = By.xpath("//*[@id=\"sub-body\"]/section/div/div/div/section/div[1]/div/div/div/input[1]");
    By reTypNewUsernameFile = By.xpath("//*[@id=\"sub-body\"]/section/div/div/div/section/div[1]/div/div/div/input[2]");
    By changeUsername = By.xpath("//*[@id=\"sub-body\"]/section/div/div/div/section/div[1]/div/div/div/div");
    By currentPwUsername = By.xpath("//*[@id=\"sub-body\"]/section/div/div/div/section/div[1]/div/div/div/input[3]");

    By pwTab = By.xpath("//*[@id=\"sub-body\"]/section/div/div/div/header/div[3]");
    By currentPw = By.xpath("//*[@id=\"sub-body\"]/section/div/div/div/section/div[1]/div/div/div/form/input[1]");
    By newPw = By.xpath("//*[@id=\"sub-body\"]/section/div/div/div/section/div[1]/div/div/div/form/input[2]");
    By changePwBtn = By.xpath("//*[@id=\"sub-body\"]/section/div/div/div/section/div[1]/div/div/div/form/div[2]");
    By minimumCharValidation = By.xpath("//*[@id=\"passwordCallout\"]");
    By reTypeNewPw = By.xpath("//*[@id=\"sub-body\"]/section/div/div/div/section/div[1]/div/div/div/form/input[3]");

    //By invalidPwText = By.xpath("//*[contains(text(), 'invalid password!')]");
    By invalidPwText = By.xpath("//*[text()[contains(.,'invalid password!')]]");
    //driver.findElement(By.xpath("//*[text() = 'foobar']"));

    public MyAccount(WebDriver driver) {
        super(driver);
    }

    public MyAccount myAccount() throws InterruptedException {
        RunCases.test = RunCases.extent.createTest("My Account Test");
        waitForVisibilityOf(myAccountTab);
        driver.findElement(myAccountTab).click();

        waitForVisibilityOf(loginEmailTab);
        RunCases.test.log(Status.PASS, MarkupHelper.createLabel("On the My account page", ExtentColor.GREEN));

        driver.findElement(loginEmailTab).click();
        Thread.sleep(2000);

        driver.findElement(newEmailField).sendKeys("admin@selenium3.coinspectapp.com");
        Thread.sleep(1000);
        driver.findElement(reTypeEmailFiled).sendKeys("admin@selenium3.coinspectapp.com");
        Thread.sleep(1000);
        driver.findElement(currentPwEmail).sendKeys("adsfasdfasdfasdfasdf");
        Thread.sleep(1000);
        driver.findElement(changeEmailBtn).click();
        waitForVisibilityOf(pwValidation);
        System.out.println("Password validation working fine");
        RunCases.test.log(Status.PASS, MarkupHelper.createLabel("Password validation working fine", ExtentColor.GREEN));

        Thread.sleep(2000);
        driver.findElement(newEmailField).sendKeys("admin@selenium3.coinspectapp.com");
        Thread.sleep(1000);
        driver.findElement(reTypeEmailFiled).sendKeys("admin@selenium3.coinspectapp.com");
        Thread.sleep(1000);
        driver.findElement(currentPwEmail).sendKeys("newtestpass");
        Thread.sleep(1000);
        driver.findElement(changeEmailBtn).click();
        waitForVisibilityOf(changeEmailBtn);
        Thread.sleep(3000);
        if (driver.findElements(invalidPwText).size() > 0){
            System.out.println("Email not change, invalid current password");
            RunCases.test.log(Status.FAIL, MarkupHelper.createLabel("Email not change, invalid current password", ExtentColor.RED));
            driver.findElement(makeFail);
        }
        System.out.println("Change email worked find");
        RunCases.test.log(Status.PASS, MarkupHelper.createLabel("Change email worked find", ExtentColor.GREEN));

        Thread.sleep(2000);
        driver.findElement(usernameTab).click();
        driver.findElement(newUsernameFile).sendKeys("testUsername");
        Thread.sleep(1000);
        driver.findElement(reTypNewUsernameFile).sendKeys("testUsername");
        Thread.sleep(1000);
        driver.findElement(currentPwUsername).sendKeys("newtestpass");
        Thread.sleep(1000);
        driver.findElement(changeUsername).click();
        Thread.sleep(2000);
        waitForVisibilityOf(changeUsername);
        Thread.sleep(3000);
        if (driver.findElements(invalidPwText).size() > 0){
            System.out.println("Username not change, invalid current password");
            RunCases.test.log(Status.FAIL, MarkupHelper.createLabel("Username not change, invalid current password", ExtentColor.RED));
            driver.findElement(makeFail);
        }
        System.out.println("Change username worked");
        RunCases.test.log(Status.PASS, MarkupHelper.createLabel("Change username worked", ExtentColor.GREEN));

        driver.findElement(pwTab).click();
        driver.findElement(currentPw).sendKeys("newtestpass");
        Thread.sleep(1000);
        driver.findElement(newPw).sendKeys("new");
        waitForVisibilityOf(minimumCharValidation);
        System.out.println("Password minimum character validation working fine");

        driver.findElement(newPw).clear();
        driver.findElement(newPw).sendKeys("newtestpass");
        Thread.sleep(1000);
        driver.findElement(reTypeNewPw).sendKeys("newtestpass");
        Thread.sleep(1000);
        driver.findElement(changePwBtn).click();
        waitForVisibilityOf(changePwBtn);
        Thread.sleep(3000);
        if (driver.findElements(invalidPwText).size() > 0){
            System.out.println("Password not change, invalid current password");
            RunCases.test.log(Status.FAIL, MarkupHelper.createLabel("Password not change, invalid current password", ExtentColor.RED));
            driver.findElement(makeFail);
        }

        System.out.println("Password changed");
        RunCases.test.log(Status.PASS, MarkupHelper.createLabel("Password changed", ExtentColor.GREEN));

        return new MyAccount(driver);
    }
}
