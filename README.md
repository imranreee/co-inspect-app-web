# Why this project?
This automation project of CoInspectApp (https://coinspectapp.com/) web application admin part

# Prerequisites to use the project
* Download and install the JDK and set the environment veritable path (http://www.oracle.com/technetwork/java/javase/downloads/index.html)
* Download and install the intelliJ IDEA (https://www.jetbrains.com/idea/download/#section=windows)
* Download and keep the Chrome web driver in any driver your PC(http://chromedriver.chromium.org/downloads) No need install

# Project structure and instruction

 ![] (readmeimage/01.png)
 
 ![] (readmeimage/02.png)
 
 ![] (readmeimage/003.png)